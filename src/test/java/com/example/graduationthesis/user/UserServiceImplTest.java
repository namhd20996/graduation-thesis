package com.example.graduationthesis.user;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Nested;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceImplTest {

    @BeforeAll
    public void setUpAll() {
        System.out.println("setup All");
    }

    @BeforeEach
    public void setUp() {
        System.out.println("setup Each");
    }

    @Test
    @DisplayName("Dùng để hiển thị thông báo cho error")
//    @EnabledOnOs(value = OS.MAC, disabledReason = "Hello MacOs")
//    @DisabledOnOs(value = OS.WINDOWS, disabledReason = "Hello window")
    void registration() {
        System.out.println("Tét");
        Assumptions.assumeTrue("DEV1".equals(System.getProperty("ENV")));
    }

    @Test
    @Nested
    @DisplayName("Parameterized test")
    @ParameterizedTest
    @ValueSource(strings = {"123", "234"})
    public void parameterTest(String string) {
        System.out.println(string);
    }

    @AfterEach
    public void tearDownEach() {
        System.out.println("tearDown each");
    }

    @AfterAll
    public void tearDownAll() {
        System.out.println("tearDown all");
    }
}