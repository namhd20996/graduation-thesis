package com.example.graduationthesis.review.entity;

import com.example.graduationthesis.feedback.entity.Feedback;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.reviewcategory.entity.ReviewCategory;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "_review")
public class Review extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column
    private Double rating;
    @Column
    @CreatedDate
    private Date reviewDate;
    @Column(length = 512)
    @Nationalized
    private String reviewComment;
    @Column
    private String roomName;
    @Column
    private String bedName;
    @Column
    private String roomTypeName;
    @Column
    private Integer totalDay;
    @Column
    private String fullName;
    @Column
    private LocalDate bookingDate;
    @OneToMany(mappedBy = "review")
    private List<ReviewCategory> reviewCategories;
    @OneToMany(mappedBy = "review")
    private List<Feedback> feedbacks;

}
