package com.example.graduationthesis.review.dto;

import com.example.graduationthesis.feedback.dto.FeedbackDTO;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public record ReviewDTO(
        UUID reviewId,
        Double rating,
        String reviewContent,
        Date reviewDate,
        String roomName,
        String bedName,
        String roomTypeName,
        String fullName,
        LocalDate bookingDate,
        List<FeedbackDTO> feedbacks
) {
}
