package com.example.graduationthesis.review.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ReviewUpdateRequest {

    @NotNull
    private UUID reviewId;
    @NotNull
    private Double rating;
    @NotBlank
    private String reviewComment;
}
