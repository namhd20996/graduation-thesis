package com.example.graduationthesis.review.controller.member;

import com.example.graduationthesis.constant.ReviewConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.review.dto.ReviewDTO;
import com.example.graduationthesis.review.dto.response.ReviewResponse;
import com.example.graduationthesis.review.service.ReviewService;
import com.example.graduationthesis.review.dto.request.ReviewUpdateRequest;
import com.example.graduationthesis.review.dto.request.ReviewAddRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + ReviewConstant.API_REVIEW)
@RequiredArgsConstructor
public class ReviewAuthController {

    private final ReviewService reviewService;

    @PostMapping
    public ResponseEntity<ReviewResponse> addReview(
            @Validated @RequestBody ReviewAddRequest request
    ) {
        return new ResponseEntity<>(reviewService.addReview(request), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ReviewResponse> updateReview(
            @Validated @RequestBody ReviewUpdateRequest request
    ) {
        return new ResponseEntity<>(reviewService.updateReview(request), HttpStatus.OK);
    }

}
