package com.example.graduationthesis.review.mapper;

import com.example.graduationthesis.feedback.dto.FeedbackDTO;
import com.example.graduationthesis.review.dto.ReviewDTO;
import com.example.graduationthesis.review.entity.Review;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class ReviewDTOMapper implements Function<Review, ReviewDTO> {
    @Override
    public ReviewDTO apply(Review review) {
        return new ReviewDTO(
                review.getId(),
                review.getRating(),
                review.getReviewComment(),
                review.getReviewDate(),
                review.getRoomName(),
                review.getBedName(),
                review.getRoomTypeName(),
                review.getFullName(),
                review.getBookingDate(),
                review.getFeedbacks() != null
                        ? review.getFeedbacks().stream()
                        .map(item -> new FeedbackDTO(
                                item.getId(),
                                item.getReview().getId(),
                                item.getFeedbackDate(),
                                item.getFeedbackContent()
                        )).toList()
                        : null
        );
    }
}
