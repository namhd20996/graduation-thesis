package com.example.graduationthesis.review.service.impl;

import com.example.graduationthesis.category.entity.Category;
import com.example.graduationthesis.category.mapper.CategoryDTOMapper;
import com.example.graduationthesis.category.repo.CategoryRepo;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.review.dto.ReviewDTO;
import com.example.graduationthesis.review.dto.request.ReviewAddRequest;
import com.example.graduationthesis.review.dto.request.ReviewUpdateRequest;
import com.example.graduationthesis.review.dto.response.ReviewResponse;
import com.example.graduationthesis.review.dto.response.ReviewSyntheticResponse;
import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.review.mapper.ReviewDTOMapper;
import com.example.graduationthesis.review.repo.ReviewRepo;
import com.example.graduationthesis.review.service.ReviewService;
import com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName;
import com.example.graduationthesis.reviewcategory.entity.ReviewCategory;
import com.example.graduationthesis.reviewcategory.repo.ReviewCategoryRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class ReviewServiceImpl extends AbsUserServiceUtil implements ReviewService {

    private final ReviewRepo reviewRepo;

    private final ReviewDTOMapper reviewDTOMapper;

    private final ReviewCategoryRepo reviewCategoryRepo;

    private final HotelRepo hotelRepo;

    private final CategoryRepo categoryRepo;

    private final CategoryDTOMapper categoryDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public long countReviewByHotelId(UUID hid) {
        return reviewRepo.countReviewByHotelId(hid);
    }

    @Override
    public ReviewResponse addReview(ReviewAddRequest request) {
        Category category = categoryRepo.findById(request.getCategoryId())
                .orElseThrow(() -> new ApiRequestException("", "find category by id " + request.getCategoryId() + " not found!."));

        Hotel hotel = hotelRepo.findById(request.getHotelId())
                .orElseThrow(() -> new ApiRequestException("", "find hotel by id " + request.getHotelId() + " not found!"));

        User user = getUser();

        Review review = reviewRepo.save(
                Review.builder()
                        .hotel(hotel)
                        .user(user)
                        .rating(request.getRating())
                        .reviewComment(request.getReviewComment())
                        .roomName(request.getRoomName())
                        .bedName(request.getBedName())
                        .roomTypeName(request.getRoomTypeName())
                        .totalDay(request.getTotalDay())
                        .fullName(request.getFullName())
                        .bookingDate(request.getBookingDate())
                        .reviewDate(new Date(System.currentTimeMillis()))
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        reviewCategoryRepo.save(
                ReviewCategory.builder()
                        .review(review)
                        .category(category)
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        return ReviewResponse.builder()
                .code(ResourceConstant.RV_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(reviewDTOMapper.apply(review))
                .message(getMessageBundle(ResourceConstant.RV_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }

    @Override
    public ReviewResponse updateReview(ReviewUpdateRequest request) {
        Review review = reviewRepo.findById(request.getReviewId())
                .orElseThrow(() -> new ApiRequestException("", "find review by id " + request.getReviewId() + " not found!."));

        if (!Objects.equals(review.getRating(), request.getRating()))
            review.setRating(request.getRating());

        if (!review.getReviewComment().equals(request.getReviewComment()))
            review.setReviewComment(request.getReviewComment());

        Review save = reviewRepo.save(review);
        return ReviewResponse.builder()
                .code(ResourceConstant.RV_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(reviewDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.RV_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();


    }

    @Override
    public ReviewSyntheticResponse findReviewsByHotel(UUID hid, Integer page, Integer limit) {
        Pageable pageable = baseAmenityUtil.pageable(page, limit);
        List<Review> reviews = reviewRepo.findReviewsByHotelId(hid, pageable)
                .orElseThrow(() -> new ApiRequestException("", "find reviews by hotel id " + hid + " not found!."));
        List<ReviewDTO> reviewDTOS = reviews.stream()
                .map(reviewDTOMapper)
                .toList();

        List<StatisticalRatingGroupByName> statisticalRatingGroupByNames =
                reviewCategoryRepo.statisticalRatingGroupNameByHotelId(hid)
                        .orElseThrow(() -> new ApiRequestException("", "statistic by hotel not found!."));

        double totalRatingReview = statisticalRatingGroupByNames.stream()
                .mapToDouble(StatisticalRatingGroupByName::averageRating)
                .sum();

        List<Category> categories = categoryRepo.findCategoriesByStatus(SystemConstant.STATUS_ACTIVE)
                .orElseThrow(() -> new ApiRequestException("", "find category by status active not found!"));

        return ReviewSyntheticResponse.builder()
                .totalRatingReview(totalRatingReview / statisticalRatingGroupByNames.size())
                .totalNumberOfReviews(reviews.size())
                .statisticalRatingGroupByNames(statisticalRatingGroupByNames)
                .categories(
                        categories.stream()
                                .map(categoryDTOMapper)
                                .toList()
                )
                .reviewResponse(reviewDTOS)
                .build();
    }
}
