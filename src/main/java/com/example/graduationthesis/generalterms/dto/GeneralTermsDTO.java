package com.example.graduationthesis.generalterms.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class GeneralTermsDTO extends BaseDTOUtil {

    private String name;

    private String content;

    private String status;
}
