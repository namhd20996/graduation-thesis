package com.example.graduationthesis.generalterms.service;

import com.example.graduationthesis.generalterms.dto.request.GeneralTermsRequest;
import com.example.graduationthesis.generalterms.dto.response.GeneralTermsResponse;
import com.example.graduationthesis.generalterms.dto.response.GeneralTermsResponses;

import java.util.UUID;

public interface GeneralTermsService {

    GeneralTermsResponse addGeneralTerms(GeneralTermsRequest request);

    GeneralTermsResponse updateGeneralTerms(GeneralTermsRequest request, UUID gtid);

    GeneralTermsResponse deleteGeneralTerms(UUID gtid);

    GeneralTermsResponses findAll(int currentPage,int limitPage);
}
