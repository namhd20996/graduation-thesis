package com.example.graduationthesis.roomtype.service;

import com.example.graduationthesis.roomtype.dto.request.RoomTypeRequest;
import com.example.graduationthesis.roomtype.dto.response.RoomTypeResponse;
import com.example.graduationthesis.roomtype.dto.response.RoomTypeResponses;

import java.util.UUID;

public interface RoomTypeService {

    RoomTypeResponse addRoomType(RoomTypeRequest request);

    RoomTypeResponse updateRoomType(RoomTypeRequest request, UUID rotid);

    RoomTypeResponse deleteRoomType(UUID rotid);

    RoomTypeResponses findAllRoomType(int currentPage, int limitPage);
}
