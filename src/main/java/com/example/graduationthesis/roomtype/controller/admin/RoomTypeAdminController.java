package com.example.graduationthesis.roomtype.controller.admin;

import com.example.graduationthesis.constant.RoomTypeConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.roomtype.dto.request.RoomTypeRequest;
import com.example.graduationthesis.roomtype.dto.response.RoomTypeResponse;
import com.example.graduationthesis.roomtype.dto.response.RoomTypeResponses;
import com.example.graduationthesis.roomtype.service.RoomTypeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + RoomTypeConstant.API_ROOM_TYPE)
@RequiredArgsConstructor
public class RoomTypeAdminController {

    private final RoomTypeService roomTypeService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping
    public ResponseEntity<RoomTypeResponse> addRoomType(
            @Valid @RequestBody RoomTypeRequest request
    ) {
        return new ResponseEntity<>(roomTypeService.addRoomType(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping("/{rotid}")
    public ResponseEntity<RoomTypeResponse> updateRoomType(
            @Valid @RequestBody RoomTypeRequest request,
            @PathVariable("rotid") UUID rotid
    ) {
        return new ResponseEntity<>(roomTypeService.updateRoomType(request, rotid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping("/{rotid}")
    public ResponseEntity<RoomTypeResponse> deleteRoomType(@PathVariable("rotid") UUID rotid) {
        return new ResponseEntity<>(roomTypeService.deleteRoomType(rotid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<RoomTypeResponses> selectAllRoomType(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(roomTypeService.findAllRoomType(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

}
