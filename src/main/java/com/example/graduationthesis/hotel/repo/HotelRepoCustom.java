package com.example.graduationthesis.hotel.repo;

import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.response.HotelDTOResponse;
import org.springframework.data.domain.Page;

public interface HotelRepoCustom {

    Page<HotelDTOResponse> findHotelsByCondition(HotelSearchRequest request, int currentPage, int limitPage);

}
