package com.example.graduationthesis.hotel.repo.impl;

import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.response.HotelDTOResponse;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepoCustom;
import com.example.graduationthesis.utils.AbstractDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class HotelRepoCustomImpl extends AbstractDao<Hotel> implements HotelRepoCustom {

    private final Logger logger = LoggerFactory.getLogger(HotelRepoCustomImpl.class);

    String query = """
            SELECT
                DISTINCT
                new com.example.graduationthesis.hotel.dto.response.HotelDTOResponse(
                    h.id,
                    h.name,
                    'room name demo',
                    'bed name demo',
                    h.rating,
                    h.streetAddress,
                    h.districtAddress,
                    h.city,
                    h.country,
                    h.countView,
                    r.pricePerNight,
                    h.description
                )
            FROM
                Hotel h
            LEFT JOIN
                Room r ON r.hotel.id = h.id
            LEFT JOIN
                ServiceAndAmenityRoom sar ON sar.room.id = r.id
            LEFT JOIN
                BookingDetail bkd ON bkd.room.id = r.id
            WHERE
                h.status = 'ACTIVE' AND r.status = 'ACTIVE'
            AND
                r.quantityRoom >= :quantityRoom
            AND
                r.maxOccupancy >= :maxOccupancy
            AND
                (bkd.checkOutDate <= :checkInDate OR r.saleDate <= :checkInDate)
            """;

    @Override
    public Page<HotelDTOResponse> findHotelsByCondition(HotelSearchRequest request, int currentPage, int limitPage) {
        try {
            Map<String, Object> params = new HashMap<>();
            StringBuilder hql = new StringBuilder();
            hql.append(query);
            params.put("quantityRoom", request.getQuantityRoom());
            params.put("maxOccupancy", request.getAdults() + request.getChildren());
            params.put("checkInDate", request.getCheckInDate());

            if (request.getCity() != null) {
                hql.append(" AND h.city ILIKE :city");
                params.put("city", request.getCity());
            }

            if (request.getCountry() != null) {
                hql.append(" AND h.country ILIKE :country");
                params.put("country", request.getCountry());
            }

            if (request.getHotelId() != null) {
                hql.append(" AND h.id = :hotelId");
                params.put("hotelId", request.getHotelId());
            }

            return findMany(hql.toString(), params, currentPage, limitPage);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

}
