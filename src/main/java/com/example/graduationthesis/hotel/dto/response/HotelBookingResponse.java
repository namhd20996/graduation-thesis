package com.example.graduationthesis.hotel.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class HotelBookingResponse {

    private UUID hotelId;

    private String hotelName;

    private String hotelImage;

    private String city;

    private String hotelAddress;
}
