package com.example.graduationthesis.hotel.dto.response;

import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteDTOResp;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeIndexResp;
import com.example.graduationthesis.review.dto.response.ReviewHotelIndexResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class HotelIndexResponse {

    private List<HotelTypeIndexResp> hotelTypeIndexResp;

    private List<ReviewHotelIndexResponse> reviewHotelIndexResp;

    private List<HotelFavouriteDTOResp> hotelFavouriteResp;

    private List<HotelGroupResp> hotelGroupPostalCodeResp;

    private List<HotelGroupResp> hotelGroupCityResp;
}
