package com.example.graduationthesis.hotel.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class HotelSearchResultBase {

    private UUID hotelId;

    private String name;

    private String roomName;

    private String bedName;

    private Integer adults;

    private Integer children;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private Integer quantityRoom;

    private Double rating;

    private String streetAddress;

    private String districtAddress;

    private String country;

    private String city;

    private Long totalDay;

    private Integer countView;

    private Double discountPercent;

    private String description;
}
