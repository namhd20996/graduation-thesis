package com.example.graduationthesis.hotel.dto.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelUpdateStatusRequest {

    @NotNull(message = "hotel id not null")
    private UUID hotelId;
    @NotNull
    @Min(value = 0, message = "min status value = 0")
    @Max(value = 1, message = "max status value = 1")
    private String status;
}
