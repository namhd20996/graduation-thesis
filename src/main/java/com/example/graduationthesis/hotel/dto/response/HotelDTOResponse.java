package com.example.graduationthesis.hotel.dto.response;

import java.math.BigDecimal;
import java.util.UUID;

public record HotelDTOResponse(
        UUID hotelId,

        String name,

        String roomName,

        String bedName,

        Double rating,

        String streetAddress,

        String districtAddress,

        String city,

        String country,

        Integer countView,

        BigDecimal priceRoom,

        String description
) {
}


