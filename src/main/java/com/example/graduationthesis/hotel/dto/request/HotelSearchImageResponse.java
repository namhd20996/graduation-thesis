package com.example.graduationthesis.hotel.dto.request;

public record HotelSearchImageResponse(String urlImage) {
}
