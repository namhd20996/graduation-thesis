package com.example.graduationthesis.hotel.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HotelAddTypeRequest {

    private String name;

    private Double rating;

    private String contactPerson;

    private String phoneNumber;

    private String phoneNumberTwo;

    private String streetAddress;

    private String districtAddress;

    private String country;

    private String city;

    private String postalCode;

}
