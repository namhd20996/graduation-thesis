package com.example.graduationthesis.hotel.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelSearchRequest {

    private UUID hotelId;

    private String city;

    private String country;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private Integer adults;

    private Integer children;

    private Integer quantityRoom;

    private BigDecimal priceFindStart;

    private BigDecimal priceFindEnd;

}
