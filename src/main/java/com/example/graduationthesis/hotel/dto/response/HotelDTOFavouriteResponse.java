package com.example.graduationthesis.hotel.dto.response;

import java.util.UUID;

public record HotelDTOFavouriteResponse(
        UUID hotelId,

        String name,

        Double rating,

        String streetAddress,

        String districtAddress,

        String city,

        String country,

        Integer countView,

        String hotelImage,

        String description
) {
}


