package com.example.graduationthesis.hotel.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@Builder
public class HotelSearchResult {

    private UUID hotelId;

    private Long countReview;

    private Double reviewRating;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private Integer quantityRoom;

    private String name;

    private String roomName;

    private String bedName;

    private Double rating;

    private String streetAddress;

    private String districtAddress;

    private String country;

    private String city;

    private Integer adults;

    private Integer children;

    private Long totalDay;

    private BigDecimal totalMoneyOriginal;

    private BigDecimal totalMoneyPromotion;

    private Integer countView;

    private Double discountPercent;

    private String urlImage;
}
