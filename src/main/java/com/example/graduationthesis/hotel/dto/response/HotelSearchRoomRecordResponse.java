package com.example.graduationthesis.hotel.dto.response;

import com.example.graduationthesis.room.dto.response.RoomRecordResp;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public record HotelSearchRoomRecordResponse(

        UUID roomId,
        String roomName,
        String roomNameCustom,
        String bedName,
        Integer maxOccupancy,
        Integer quantityRoom,
        BigDecimal totalMoneyOriginal,
        BigDecimal totalMoneyPromotion,
        List<RoomRecordResp> roomSearchToNameServiceAndAmenities
) {
}
