package com.example.graduationthesis.hotel.mapper;

import com.example.graduationthesis.hotel.dto.HotelDTO;
import com.example.graduationthesis.hotel.entity.Hotel;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class HotelDTOMapper implements Function<Hotel, HotelDTO> {

    private final ModelMapper mapper;

    @Override
    public HotelDTO apply(Hotel hotel) {
        return mapper.map(hotel, HotelDTO.class);
    }

}
