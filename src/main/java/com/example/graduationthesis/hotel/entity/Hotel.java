package com.example.graduationthesis.hotel.entity;

import com.example.graduationthesis.hotelfavourite.entity.HotelFavourite;
import com.example.graduationthesis.hotelimage.entity.HotelImage;
import com.example.graduationthesis.hotelpaymentmethod.entity.HotelPaymentMethod;
import com.example.graduationthesis.hotelpolicy.entity.HotelPolicy;
import com.example.graduationthesis.hoteltype.entity.HotelType;
import com.example.graduationthesis.policyconfirm.entity.PolicyConfirmation;
import com.example.graduationthesis.promotion.entity.Promotion;
import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "_hotel")
public class Hotel extends BaseEntityUtil {
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "hotel_type_id")
    private HotelType hotelType;
    @ManyToOne
    @JoinColumn(name = "policy_confirmation_id")
    private PolicyConfirmation policyConfirmation;
    @ManyToOne
    @JoinColumn(name = "hotel_payment_method_id")
    private HotelPaymentMethod hotelPaymentMethod;
    @Column
    private String name;
    @Column
    private Double rating;
    @Column
    private String contactPerson;
    @Column
    private String phoneNumber;
    @Column
    private String phoneNumberTwo;
    @Column
    private String streetAddress;
    @Column
    private String districtAddress;
    @Column
    private String country;
    @Column
    private String city;
    @Column
    private Integer countView;
    @Column
    @Temporal(TemporalType.DATE)
    private Date activationDate;
    @Column
    private String postalCode;
    @Column
    private String description;
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<HotelImage> images;
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<HotelPolicy> policies;
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<Promotion> promotions;
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<Room> rooms;
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<Review> reviews;
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<HotelFavourite> hotelFavourites;

}
