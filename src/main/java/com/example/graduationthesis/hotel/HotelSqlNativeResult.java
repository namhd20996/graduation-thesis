package com.example.graduationthesis.hotel;

import java.math.BigDecimal;
import java.util.UUID;

public interface HotelSqlNativeResult {

    UUID getHotelId();

    String getName();

    String getRoomName();

    String getBedName();

    Double getRating();

    String getStreetAddress();

    String getDistrictAddress();

    String getCity();

    String getCountry();

    Integer getTotalDay();

    BigDecimal getTotalMoneyOriginal();

    Integer getCountView();
}
