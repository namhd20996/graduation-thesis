package com.example.graduationthesis.hotel.controller.member;

import com.example.graduationthesis.constant.HotelConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotel.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + HotelConstant.API_HOTEL)
@RequiredArgsConstructor
public class HotelMemberController {

    private final HotelService hotelService;

    @GetMapping("/favourite")
    public ResponseEntity<?> getHotelByHotelFavouriteByUser() {
        return ResponseEntity.ok(hotelService.getHotelByHotelFavouriteByUser());
    }
}
