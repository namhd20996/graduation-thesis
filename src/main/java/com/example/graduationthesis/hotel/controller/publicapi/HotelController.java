package com.example.graduationthesis.hotel.controller.publicapi;

import com.example.graduationthesis.constant.HotelConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotel.dto.request.HotelSearchRequest;
import com.example.graduationthesis.hotel.dto.response.HotelSearchResultResponse;
import com.example.graduationthesis.hotel.service.HotelService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + HotelConstant.API_HOTEL)
@RequiredArgsConstructor
public class HotelController {

    private final HotelService hotelService;

    @GetMapping(HotelConstant.HOTEL_GET_INDEX)
    public ResponseEntity<?> findAllHotel() {
        return new ResponseEntity<>(hotelService.findHotelsIndex(), HttpStatus.OK);
    }

    @GetMapping(HotelConstant.HOTEL_GET_CONDITION)
    public ResponseEntity<HotelSearchResultResponse> findAllByCondition(
            @Valid HotelSearchRequest request,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(hotelService.findAllHotelByCondition(
                request,
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK
        );
    }

    @GetMapping(HotelConstant.HOTEL_GET_CONDITION_ID)
    public ResponseEntity<?> findHotelById(
            @Valid HotelSearchRequest request
    ) {
        return new ResponseEntity<>(hotelService.findHotelByConditionAndId(request), HttpStatus.OK);
    }

}
