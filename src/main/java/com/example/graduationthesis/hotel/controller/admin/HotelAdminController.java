package com.example.graduationthesis.hotel.controller.admin;

import com.example.graduationthesis.constant.HotelConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotel.dto.request.HotelAddTypeRequest;
import com.example.graduationthesis.hotel.dto.request.HotelStarSessionOne;
import com.example.graduationthesis.hotel.dto.request.HotelUpdateByOwnerRequest;
import com.example.graduationthesis.hotel.dto.response.HotelResponse;
import com.example.graduationthesis.hotel.dto.response.HotelResponses;
import com.example.graduationthesis.hotel.service.HotelService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + HotelConstant.API_HOTEL)
@RequiredArgsConstructor
public class HotelAdminController {

    private final HotelService hotelService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping("/{hid}/{status}")
    public ResponseEntity<HotelResponse> updateStatusHotelWithRoleAdminMaster(
            @PathVariable("hid") UUID hid,
            @PathVariable("status") Optional<String> status

    ) {
        return new ResponseEntity<>(hotelService.updateStatusHotelWithRoleAdminMaster(
                hid,
                status.orElse(SystemConstant.STATUS_ACTIVE)),
                HttpStatus.OK
        );
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @PostMapping(HotelConstant.HOTEL_ADD_SESSION_ONE)
    public ResponseEntity<HotelResponse> addHotelSessionOne(
            @Valid @RequestBody HotelStarSessionOne request
    ) {
        return new ResponseEntity<>(hotelService.addHotelSessionOne(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @PostMapping(HotelConstant.HOTEL_ADD_SESSION_TWO)
    public ResponseEntity<HotelResponse> addHotel(
            @RequestHeader("jwtToken") String jwtToken,
            @Valid @RequestBody HotelAddTypeRequest request
    ) {
        return new ResponseEntity<>(hotelService.addHotelSessionTwo(jwtToken, request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @GetMapping
    public ResponseEntity<HotelResponses> findAllHotel(
            @RequestParam(SystemConstant.PARAM_STATUS) Optional<String> status,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> page,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limit
    ) {
        return new ResponseEntity<>(hotelService.findAll(
                status.orElse(SystemConstant.STATUS_ACTIVE),
                page.orElse(1),
                limit.orElse(8)),
                HttpStatus.OK
        );
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @PutMapping(HotelConstant.HOTEL_UPDATE_OWNER)
    public ResponseEntity<HotelResponse> updateHotelWithRoleOwner(
            @Valid @RequestBody HotelUpdateByOwnerRequest request
    ) {
        return new ResponseEntity<>(hotelService.updateHotelWithRoleOwner(request), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @GetMapping("/{jwtToken}/{status}")
    public ResponseEntity<HotelResponse> getDateHotelRegister(
            @PathVariable("jwtToken") String jwtToken,
            @PathVariable("status") String status
    ) {
        return new ResponseEntity<>(hotelService.getProfileHotelRegister(jwtToken, status), HttpStatus.OK);
    }

}
