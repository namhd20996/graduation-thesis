package com.example.graduationthesis.permissionrole.controller.admin;

import com.example.graduationthesis.constant.PermissionRoleConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.permissionrole.dto.request.PermissionRoleAddRequest;
import com.example.graduationthesis.permissionrole.dto.request.PermissionRoleUpdateRequest;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleResponse;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleResponses;
import com.example.graduationthesis.permissionrole.service.PermissionRoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + PermissionRoleConstant.API_PERMISSION_ROLE)
@RequiredArgsConstructor
@PreAuthorize("hasAnyAuthority('admin_master')")
public class PermissionRoleAdminController {

    private final PermissionRoleService permissionRoleService;

    @GetMapping
    public ResponseEntity<PermissionRoleResponses> findAllPermissionRole(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(permissionRoleService.findAllPermissionRole(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PermissionRoleResponse> addPermissionRole(
            @Valid @RequestBody PermissionRoleAddRequest request
    ) {
        return new ResponseEntity<>(permissionRoleService.addPermissionRole(request), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<PermissionRoleResponse> updatePermissionRole(
            @Valid @RequestBody PermissionRoleUpdateRequest request
    ) {
        return new ResponseEntity<>(permissionRoleService.updatePermissionRole(request), HttpStatus.OK);
    }

    @DeleteMapping(PermissionRoleConstant.API_PERMISSION_ROLE_ID)
    public ResponseEntity<PermissionRoleResponse> deletePermissionRole(
            @PathVariable(PermissionRoleConstant.PATH_PERMISSION_ROLE_ID) UUID id
    ) {
        return new ResponseEntity<>(permissionRoleService.deletePermissionRole(id), HttpStatus.OK);
    }
}
