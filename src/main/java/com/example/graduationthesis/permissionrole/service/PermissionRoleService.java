package com.example.graduationthesis.permissionrole.service;

import com.example.graduationthesis.permissionrole.dto.request.PermissionRoleAddRequest;
import com.example.graduationthesis.permissionrole.dto.request.PermissionRoleUpdateRequest;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleResponse;
import com.example.graduationthesis.permissionrole.dto.response.PermissionRoleResponses;

import java.util.UUID;

public interface PermissionRoleService {

    PermissionRoleResponse addPermissionRole(PermissionRoleAddRequest request);

    PermissionRoleResponse updatePermissionRole(PermissionRoleUpdateRequest request);

    PermissionRoleResponse deletePermissionRole(UUID permissionRoleId);

    PermissionRoleResponses findAllPermissionRole(int currentPage, int limitPage);
}
