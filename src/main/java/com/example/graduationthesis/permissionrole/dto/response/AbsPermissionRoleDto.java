package com.example.graduationthesis.permissionrole.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public abstract class AbsPermissionRoleDto {
    private UUID permissionRoleId;

    private UUID permissionId;

    private String permissionCode;

    private UUID roleId;

    private String roleName;

    private Boolean isDeleted;

}
