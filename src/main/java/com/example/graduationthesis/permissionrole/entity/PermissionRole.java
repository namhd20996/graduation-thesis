package com.example.graduationthesis.permissionrole.entity;

import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.role.entity.Role;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_permission_role")
@Entity
public class PermissionRole extends BaseEntityUtil {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "permission_id")
    private Permission permission;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    public PermissionRole(Permission permission, Role role, Boolean isDeleted, String status) {
        this.permission = permission;
        this.role = role;
        super.setIsDeleted(isDeleted);
        super.setStatus(status);
    }
}
