package com.example.graduationthesis.historysearch.entity;

import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_history_search")
public class HistorySearch extends BaseIDUtil {

    @Column
    private String locationSearch;
    @Column
    private Date checkInSearch;
    @Column
    private Date checkOutSearch;
    @Column
    private Integer adults;
    @Column
    private Integer children;
    @Column
    private Integer room;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
