package com.example.graduationthesis.historysearch.dto;

import com.example.graduationthesis.utils.BaseDTOIDUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class HistorySearchDTO extends BaseDTOIDUtil {

    private String locationSearch;

    private Date checkInSearch;

    private Date checkOutSearch;

    private Integer adults;

    private Integer children;

    private Integer room;
}
