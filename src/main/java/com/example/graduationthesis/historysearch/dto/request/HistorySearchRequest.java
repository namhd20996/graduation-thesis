package com.example.graduationthesis.historysearch.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class HistorySearchRequest {

    private String locationSearch;

    private Date checkInSearch;

    private Date checkOutSearch;

    private Integer adults;

    private Integer children;

    private Integer room;
}
