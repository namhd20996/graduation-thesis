package com.example.graduationthesis.historysearch.service.impl;

import com.example.graduationthesis.historysearch.mapper.HistorySearchDTOMapper;
import com.example.graduationthesis.historysearch.repo.HistorySearchRepo;
import com.example.graduationthesis.historysearch.service.HistorySearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class HistorySearchServiceImpl implements HistorySearchService {

    private final HistorySearchRepo historySearchRepo;

    private final HistorySearchDTOMapper historySearchDTOMapper;
}
