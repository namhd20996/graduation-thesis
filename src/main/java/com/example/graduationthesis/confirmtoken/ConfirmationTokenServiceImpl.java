package com.example.graduationthesis.confirmtoken;

import com.example.graduationthesis.exception.ApiRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private final ConfirmationTokenRepo confirmationTokenRepo;

    private final ConfirmationTokenDTOMapper confirmationTokenDTOMapper;

    @Override
    public ConfirmationTokenDTO findByToken(String token) {
        return confirmationTokenRepo.findConfirmationTokenByToken(token)
                .map(confirmationTokenDTOMapper)
                .orElseThrow(() -> new ApiRequestException("", "find confirmation by token " + token + " not found!."));
    }

    @Override
    public void setConfirmAt(String token) {
        confirmationTokenRepo.updateConfirmAtByToken(token, LocalDateTime.now());
    }

    @Override
    public void saveConfirmationToken(ConfirmationToken token) {
        confirmationTokenRepo.save(token);
    }
}
