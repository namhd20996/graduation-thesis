package com.example.graduationthesis.confirmtoken;

public interface ConfirmationTokenService {

    ConfirmationTokenDTO findByToken(String token);

    void setConfirmAt(String token);

    void saveConfirmationToken(ConfirmationToken token);
}
