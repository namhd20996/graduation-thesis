package com.example.graduationthesis.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@RequiredArgsConstructor
@PropertySource("classpath:application.yml")
public class PermissionConfig {

    private final Environment environment;

    @Bean
    public String adminMaster() {
        return environment.getProperty("admin.admin-master");
    }

    @Bean
    public String adminAdmin() {
        return environment.getProperty("admin.admin-admin");
    }

    @Bean
    public String adminRead() {
        return environment.getProperty("admin.admin-read");
    }

    @Bean
    public String adminUpdate() {
        return environment.getProperty("admin.admin-update");
    }

    @Bean
    public String adminWrite() {
        return environment.getProperty("admin.admin-write");
    }

    @Bean
    public String adminDelete() {
        return environment.getProperty("admin.admin-delete");
    }

    @Bean
    public String guestRead() {
        return environment.getProperty("guest.guest-read");
    }

    @Bean
    public String guestWrite() {
        return environment.getProperty("guest.guest-write");
    }

    @Bean
    public String guestUpdate() {
        return environment.getProperty("guest.guest-update");
    }

    @Bean
    public String guestDelete() {
        return environment.getProperty("guest.guest-delete");
    }

    @Bean
    public String contentRead() {
        return environment.getProperty("content.content-read");
    }

    @Bean
    public String contentWrite() {
        return environment.getProperty("content.content-write");
    }

    @Bean
    public String contentUpdate() {
        return environment.getProperty("content.content-update");
    }

    @Bean
    public String contentDelete() {
        return environment.getProperty("content.content-delete");
    }

    @Bean
    public String managerImage() {
        return environment.getProperty("image.manager-image");
    }

    @Bean
    public String hotelUpdateStatus() {
        return environment.getProperty("hotel.hotel-update-status");
    }

    @Bean
    public String hotelCreateInfo() {
        return environment.getProperty("hotel.hotel-create-info");
    }

    @Bean
    public String hotelViewInfo() {
        return environment.getProperty("hotel.hotel-view-info");
    }

    @Bean
    public String hotelUpdateInfo() {
        return environment.getProperty("hotel.hotel-update-info");
    }

    @Bean
    public String hotelCreateContact() {
        return environment.getProperty("hotel.hotel-create-contact");
    }

    @Bean
    public String hotelViewContact() {
        return environment.getProperty("hotel.hotel-view-contact");
    }

    @Bean
    public String hotelUpdateContact() {
        return environment.getProperty("hotel.hotel-update-contact");
    }

    @Bean
    public String roomCreateInfo() {
        return environment.getProperty("room.room-create-info");
    }

    @Bean
    public String roomViewInfo() {
        return environment.getProperty("room.room-view-info");
    }

    @Bean
    public String roomUpdateInfo() {
        return environment.getProperty("room.room-update-info");
    }

    @Bean
    public String roomDeleteInfo() {
        return environment.getProperty("room.room-delete");
    }

    @Bean
    public String roomUpdateStatus() {
        return environment.getProperty("room.room-update-status");
    }

    @Bean
    public String roomCreatePolicy() {
        return environment.getProperty("room.room-create-policy");
    }

    @Bean
    public String roomViewPolicy() {
        return environment.getProperty("room.room-view-policy");
    }

    @Bean
    public String roomUpdatePolicy() {
        return environment.getProperty("room.room-update-policy");
    }

    @Bean
    public String userRead() {
        return environment.getProperty("user.user-read");
    }

    @Bean
    public String hotelOwner() {
        return environment.getProperty("hotel.hotel-owner");
    }
}
