package com.example.graduationthesis.revenue.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public record RevenueDTO(
        UUID revenueId,
        BigDecimal revenueAmount,
        BigDecimal profit,
        LocalDateTime statisticDay,
        UUID hotelId
) {
}
