package com.example.graduationthesis.revenue.controller.admin;

import com.example.graduationthesis.constant.RevenueConstant;
import com.example.graduationthesis.constant.SystemConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + RevenueConstant.API_REVENUE)
@RequiredArgsConstructor
public class RevenueAdminController {
}
