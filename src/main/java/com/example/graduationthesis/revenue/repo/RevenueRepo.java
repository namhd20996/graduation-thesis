package com.example.graduationthesis.revenue.repo;

import com.example.graduationthesis.revenue.entity.Revenue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RevenueRepo extends JpaRepository<Revenue, UUID> {
}
