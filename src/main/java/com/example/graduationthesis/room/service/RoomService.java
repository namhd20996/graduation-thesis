package com.example.graduationthesis.room.service;

import com.example.graduationthesis.room.dto.request.RoomAddRequest;
import com.example.graduationthesis.room.dto.request.RoomUpdateRequest;
import com.example.graduationthesis.room.dto.response.RoomResponse;
import com.example.graduationthesis.room.dto.response.RoomResponses;

import java.util.UUID;

public interface RoomService {

    RoomResponse addRoom(String jwtToken, RoomAddRequest request);

    RoomResponse updateRoom(UUID rid, RoomUpdateRequest request);

    RoomResponse deleteRoom(UUID rid);

    RoomResponses findAllRoomByStatusAndHotelId(UUID hid, String status, int currentPage, int limitPage);
}
