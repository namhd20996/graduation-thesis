package com.example.graduationthesis.room.service.impl;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.room.dto.RoomDTO;
import com.example.graduationthesis.room.dto.request.RoomAddRequest;
import com.example.graduationthesis.room.dto.request.RoomUpdateRequest;
import com.example.graduationthesis.room.dto.response.RoomResponse;
import com.example.graduationthesis.room.dto.response.RoomResponses;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.room.mapper.RoomDTOMapper;
import com.example.graduationthesis.room.repo.RoomRepo;
import com.example.graduationthesis.room.service.RoomService;
import com.example.graduationthesis.roomtype.entity.RoomType;
import com.example.graduationthesis.roomtype.repo.RoomTypeRepo;
import com.example.graduationthesis.session.common.SessionResponseUtil;
import com.example.graduationthesis.session.room.RoomSession;
import com.example.graduationthesis.session.room.RoomSessionRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepo roomRepo;

    private final RoomTypeRepo roomTypeRepo;

    private final RoomDTOMapper roomDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final RoomSessionRepo roomSessionRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public RoomResponse addRoom(String jwtToken, RoomAddRequest request) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        roomSessionRepo.findRoomSessionByJwtToken(jwtToken).ifPresent(roomSessionRepo::delete);
        RoomType roomType = getRoomType(request.getRoomTypeId());

        RoomSession roomSession = roomSessionRepo.save(
                RoomSession.builder()
                        .roomTypeId(roomType.getId())
                        .pricePerNight(request.getPricePerNight())
                        .roomName(request.getRoomName())
                        .roomNameCustom(request.getRoomNameCustom())
                        .quantityRoom(request.getQuantityRoom())
                        .roomArea(request.getRoomArea())
                        .maxOccupancy(request.getMaxOccupancy())
                        .bedName(request.getBedName())
                        .jwtToken(jwtToken)
                        .build()
        );

        return RoomResponse.builder()
                .code("R_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("ROOM", roomSession))
                .message("Add room session success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomResponse updateRoom(UUID rid, RoomUpdateRequest request) {
        Room room = getRoom(rid);

        RoomType roomType = roomTypeRepo.findById(request.getRoomTypeId())
                .orElseThrow(() ->
                        new ApiRequestException("find hotel type by id " + request.getRoomTypeId() + " not found!.", ""));

        if (roomType != null && !roomType.equals(room.getRoomType()))
            room.setRoomType(roomType);

        if (!Objects.equals(room.getPricePerNight(), request.getPricePerNight()))
            room.setPricePerNight(request.getPricePerNight());

        if (!room.getRoomName().equals(request.getRoomName()))
            room.setRoomName(request.getRoomName());

        if (room.getRoomNameCustom() == null || !room.getRoomNameCustom().equals(request.getRoomNameCustom()))
            room.setRoomNameCustom(request.getRoomNameCustom());

        if (!Objects.equals(room.getQuantityRoom(), request.getQuantityRoom()))
            room.setQuantityRoom(room.getQuantityRoom());

        if (!Objects.equals(room.getRoomArea(), request.getRoomArea()))
            room.setRoomArea(request.getRoomArea());

        if (!Objects.equals(room.getMaxOccupancy(), request.getMaxOccupancy()))
            room.setMaxOccupancy(request.getMaxOccupancy());

        if (!room.getBedName().equals(request.getBedName()))
            room.setBedName(request.getBedName());

        Room save = getSave(room);

        return RoomResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private Room getSave(Room room) {
        return roomRepo.save(room);
    }

    @Override
    public RoomResponse deleteRoom(UUID rid) {
        Room room = getRoom(rid);
        room.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        Room save = getSave(room);

        return RoomResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomResponses findAllRoomByStatusAndHotelId(UUID hid, String status, int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Room> promotions = roomRepo.findRoomsByStatusAndHotelId(status, hid, pageable)
                .orElseThrow(() -> new ApiRequestException("find rooms by hotel id " + hid + " not found!", ""));

        List<RoomDTO> roomDTOS = promotions.stream()
                .map(roomDTOMapper)
                .toList();
        return RoomResponses.builder()
                .code("RO_002")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomDTOS)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, promotions.getTotalPages()))
                .message("Get rooms by hotel id success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private Room getRoom(UUID rid) {
        return roomRepo.findById(rid)
                .orElseThrow(() ->
                        new ApiRequestException("find room by id " + rid + " not found!.", ""));
    }

    private RoomType getRoomType(UUID rtid) {
        return roomTypeRepo.findById(rtid)
                .orElseThrow(() -> new ApiRequestException("find room type by id " + rtid + " not found!.", ""));
    }
}
