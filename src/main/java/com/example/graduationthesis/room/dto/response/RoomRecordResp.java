package com.example.graduationthesis.room.dto.response;

public record RoomRecordResp(String name) {
}
