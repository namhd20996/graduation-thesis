package com.example.graduationthesis.room.mapper;

import com.example.graduationthesis.room.dto.RoomDTO;
import com.example.graduationthesis.room.entity.Room;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RoomDTOMapper implements Function<Room, RoomDTO> {
    @Override
    public RoomDTO apply(Room room) {
        return new RoomDTO(
                room.getId(),
                room.getRoomType().getName(),
                room.getRoomNumber(),
                room.getStatus(),
                room.getRoomNameCustom(),
                room.getQuantityRoom(),
                room.getRoomArea(),
                room.getMaxOccupancy(),
                room.getRoomName(),
                room.getBedName()
        );
    }
}
