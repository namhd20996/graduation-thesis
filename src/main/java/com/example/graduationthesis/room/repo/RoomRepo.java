package com.example.graduationthesis.room.repo;

import com.example.graduationthesis.room.dto.response.RoomRecordResp;
import com.example.graduationthesis.room.entity.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoomRepo extends JpaRepository<Room, UUID> {

    @Modifying(clearAutomatically = true)
    @Query("""
               UPDATE Room r SET r.quantityRoom = ?1 WHERE r.id = ?2
            """)
    void updateQuantityRoom(Integer quantityRoom, UUID roomId);

    @Query("""
                SELECT DISTINCT
                    new com.example.graduationthesis.room.dto.response.RoomRecordResp(saa.name)
                FROM
                    Room r
                INNER JOIN
                    ServiceAndAmenityRoom saar ON saar.room.id = r.id
                INNER JOIN
                    ServiceAndAmenity saa ON saa.id = saar.serviceAndAmenity.id
                WHERE
                    r.id = ?1
            """)
    Optional<List<RoomRecordResp>> findAllNameServiceAndAmenityByRoomId(UUID roomId);

    Optional<Page<Room>> findRoomsByStatusAndHotelId(String status, UUID hotelId, Pageable pageable);
}
