package com.example.graduationthesis.room.controller.admin;

import com.example.graduationthesis.constant.RoomConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.room.dto.request.RoomAddRequest;
import com.example.graduationthesis.room.dto.request.RoomUpdateRequest;
import com.example.graduationthesis.room.dto.response.RoomResponse;
import com.example.graduationthesis.room.dto.response.RoomResponses;
import com.example.graduationthesis.room.service.RoomService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + RoomConstant.API_ROOM)
@RequiredArgsConstructor
public class RoomAdminController {

    private final RoomService roomService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<RoomResponse> addRoom(
            @RequestHeader("jwtToken") String jwtToken,
            @Valid @RequestBody RoomAddRequest request
    ) {
        return new ResponseEntity<>(roomService.addRoom(jwtToken, request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping("/{rid}")
    public ResponseEntity<RoomResponse> updateRoom(
            @Valid @RequestBody RoomUpdateRequest request,
            @PathVariable("rid") UUID rid
    ) {
        return new ResponseEntity<>(roomService.updateRoom(rid, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @DeleteMapping("/{rid}")
    public ResponseEntity<RoomResponse> deleteRoom(@PathVariable("rid") UUID rid) {
        return new ResponseEntity<>(roomService.deleteRoom(rid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<RoomResponses> findAllRoomByStatusAndHotelId(
            @RequestParam("hid") UUID hid,
            @RequestParam(SystemConstant.PARAM_STATUS) Optional<String> status,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(roomService.findAllRoomByStatusAndHotelId(
                hid,
                status.orElse(SystemConstant.STATUS_ACTIVE),
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK
        );
    }

}
