package com.example.graduationthesis.promotion.repo;

import com.example.graduationthesis.promotion.entity.Promotion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PromotionRepo extends JpaRepository<Promotion, UUID> {

    @Query("""
            SELECT p FROM Promotion p WHERE p.status = 'ACTIVE' AND p.hotel.id = ?1 ORDER BY p.startDate DESC
            """)
    List<Promotion> findPromotionByStatusAndHotelId(UUID hotelId);

    Optional<Page<Promotion>> findPromotionsByStatusAndHotelId(String status, UUID hotelId, Pageable pageable);

    Optional<List<Promotion>> findPromotionsByStatusAndHotelId(String status, UUID hotelId);

}
