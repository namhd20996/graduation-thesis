package com.example.graduationthesis.promotion.dto.request;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class PromotionAddRequest {

    @NotNull
    private UUID hotelId;
    @NotBlank
    private String name;
    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    private Double discountPercent;
    @FutureOrPresent
    private LocalDate startDate;
    @FutureOrPresent
    private LocalDate endDate;

    private String description;
}
