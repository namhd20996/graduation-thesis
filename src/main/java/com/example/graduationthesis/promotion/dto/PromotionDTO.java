package com.example.graduationthesis.promotion.dto;

import com.example.graduationthesis.utils.BaseDTOIDUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class PromotionDTO extends BaseDTOIDUtil {

    private String name;

    private String description;

    private Double discountPercent;

    private LocalDate startDate;

    private LocalDate endDate;
}
