package com.example.graduationthesis.constant;

public class ResourceConstant {

    // Validation
    public static final String MSG_USR_CP_001 = "MSG_USR_CP_001";
    public static final String MSG_USR_CP_002 = "MSG_USR_CP_002";
    // Email
    public static final String MSG_E_001 = "MSG_E_001";
    public static final String MSG_E_002 = "MSG_E_002";
    public static final String MSG_E_003 = "MSG_E_003";
    public static final String MSG_PN_001 = "MSG_PN_001";
    public static final String MSG_FN_001 = "MSG_FN_001";
    public static final String MSG_ID_001 = "MSG_ID_001";

    // Common
    public static final String ADD_001 = "ADD_001";
    public static final String ADD_002 = "ADD_002";
    public static final String UD_001 = "UD_001";
    public static final String UD_002 = "UD_002";
    public static final String DL_001 = "DL_001";
    public static final String DL_002 = "DL_002";
    public static final String GET_001 = "GET_001";
    public static final String GET_002 = "GET_002";
    public static final String ERROR_SERVER = "ERROR_SERVER";
    public static final String ERROR_VALIDATION_FIELD = "ERROR_VALIDATION_FIELD";
    public static final String ERROR_FORBIDDEN = "ERROR_FORBIDDEN";
    public static final String START_SESSION_BOOKING = "START_SESSION_BOOKING";
    public static final String END_SESSION_BOOKING = "END_SESSION_BOOKING";
    public static final String JWT_TOKEN_EXPIRATION = "JWT_TOKEN_EXPIRATION";

    public static final String RG_P_001 = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=]).{8,}$";


    // User
    public static final String USR_001 = "USR_001";
    public static final String USR_002 = "USR_002";
    public static final String USR_009 = "USR_009";
    public static final String USR_011 = "USR_011";
    public static final String USR_012 = "USR_012";
    public static final String USR_013 = "USR_013";

    // Service And Amenity Room
    public static final String SAA_R001 = "SAA_R001";
    public static final String SAA_R002 = "SAA_R002";
    public static final String SAA_R003 = "SAA_R003";
    public static final String SAA_R004 = "SAA_R004";
    public static final String SAA_R005 = "SAA_R005";
    public static final String SAA_R006 = "SAA_R006";
    public static final String SAA_R007 = "SAA_R007";
    public static final String SAA_R008 = "SAA_R008";
    public static final String SAA_R009 = "SAA_R009";
    public static final String SAA_R010 = "SAA_R010";

    // Service And Amenity
    public static final String SAM_001 = "SAM_001";
    public static final String SAM_002 = "SAM_002";
    public static final String SAM_003 = "SAM_003";
    public static final String SAM_004 = "SAM_004";
    public static final String SAM_005 = "SAM_005";
    public static final String SAM_006 = "SAM_006";
    public static final String SAM_007 = "SAM_007";
    public static final String SAM_008 = "SAM_008";
    public static final String SAM_009 = "SAM_009";
    public static final String SAM_010 = "SAM_010";

    // Permission
    public static final String PMS_001 = "PMS_001";
    public static final String PMS_002 = "PMS_002";
    public static final String PMS_003 = "PMS_003";
    public static final String PMS_004 = "PMS_004";
    public static final String PMS_005 = "PMS_005";
    public static final String PMS_006 = "PMS_006";
    public static final String PMS_007 = "PMS_007";
    public static final String PMS_008 = "PMS_008";
    public static final String PMS_009 = "PMS_009";
    public static final String PMS_010 = "PMS_010";
    public static final String PMS_011 = "PMS_011";

    // Permission Role

    public static final String PMS_R_001 = "PMS_R_001";
    public static final String PMS_R_002 = "PMS_R_002";
    public static final String PMS_R_003 = "PMS_R_003";
    public static final String PMS_R_004 = "PMS_R_004";
    public static final String PMS_R_005 = "PMS_R_005";
    public static final String PMS_R_006 = "PMS_R_006";
    public static final String PMS_R_007 = "PMS_R_007";
    public static final String PMS_R_008 = "PMS_R_008";
    public static final String PMS_R_009 = "PMS_R_009";
    public static final String PMS_R_010 = "PMS_R_010";

    // Role
    public static final String RL_001 = "RL_001";
    public static final String RL_002 = "RL_002";
    public static final String RL_003 = "RL_003";
    public static final String RL_004 = "RL_004";
    public static final String RL_005 = "RL_005";
    public static final String RL_006 = "RL_006";
    public static final String RL_007 = "RL_007";
    public static final String RL_008 = "RL_008";
    public static final String RL_009 = "RL_009";
    public static final String RL_010 = "RL_010";
    public static final String RV_001 = "RV_001";
    public static final String RV_002 = "RV_002";
    public static final String RV_003 = "RV_003";
    public static final String RV_004 = "RV_004";
    public static final String RV_005 = "RV_005";
    public static final String RV_006 = "RV_006";
    public static final String RV_007 = "RV_007";
    public static final String RV_008 = "RV_008";
    public static final String RV_009 = "RV_009";
    public static final String RV_010 = "RV_010";


    public static final String PMT_001 = "PMT_001";
    public static final String PMT_002 = "PMT_002";
    public static final String PMT_003 = "PMT_003";
    public static final String PMT_004 = "PMT_004";
    public static final String PMT_005 = "PMT_005";
    public static final String PMT_006 = "PMT_006";
    public static final String PMT_007 = "PMT_007";
    public static final String PMT_008 = "PMT_008";
    public static final String PMT_009 = "PMT_009";
    public static final String PMT_010 = "PMT_010";


    public static final String PL_C_001 = "PL_C_001";
    public static final String PL_C_002 = "PL_C_002";
    public static final String PL_C_003 = "PL_C_003";
    public static final String PL_C_004 = "PL_C_004";
    public static final String PL_C_005 = "PL_C_005";
    public static final String PL_C_006 = "PL_C_006";
    public static final String PL_C_007 = "PL_C_007";
    public static final String PL_C_008 = "PL_C_008";
    public static final String PL_C_009 = "PL_C_009";
    public static final String PL_C_010 = "PL_C_010";


    public static final String PM_001 = "PM_001";
    public static final String PM_002 = "PM_002";
    public static final String PM_003 = "PM_003";
    public static final String PM_004 = "PM_004";
    public static final String PM_005 = "PM_005";
    public static final String PM_006 = "PM_006";
    public static final String PM_007 = "PM_007";
    public static final String PM_008 = "PM_008";
    public static final String PM_009 = "PM_009";
    public static final String PM_010 = "PM_010";


    public static final String HT_T_001 = "HT_T_001";
    public static final String HT_T_002 = "HT_T_002";
    public static final String HT_T_003 = "HT_T_003";
    public static final String HT_T_004 = "HT_T_004";
    public static final String HT_T_005 = "HT_T_005";
    public static final String HT_T_006 = "HT_T_006";
    public static final String HT_T_007 = "HT_T_007";
    public static final String HT_T_008 = "HT_T_008";
    public static final String HT_T_009 = "HT_T_009";
    public static final String HT_T_010 = "HT_T_010";


    public static final String HT_PLC_001 = "HT_PLC_001";
    public static final String HT_PLC_002 = "HT_PLC_002";
    public static final String HT_PLC_003 = "HT_PLC_003";
    public static final String HT_PLC_004 = "HT_PLC_004";
    public static final String HT_PLC_005 = "HT_PLC_005";
    public static final String HT_PLC_006 = "HT_PLC_006";
    public static final String HT_PLC_007 = "HT_PLC_007";
    public static final String HT_PLC_008 = "HT_PLC_008";
    public static final String HT_PLC_009 = "HT_PLC_009";
    public static final String HT_PLC_010 = "HT_PLC_010";

    public static final String HT_PM_001 = "HT_PM_001";
    public static final String HT_PM_002 = "HT_PM_002";
    public static final String HT_PM_003 = "HT_PM_003";
    public static final String HT_PM_004 = "HT_PM_004";
    public static final String HT_PM_005 = "HT_PM_005";
    public static final String HT_PM_006 = "HT_PM_006";
    public static final String HT_PM_007 = "HT_PM_007";
    public static final String HT_PM_008 = "HT_PM_008";
    public static final String HT_PM_009 = "HT_PM_009";
    public static final String HT_PM_010 = "HT_PM_010";


    public static final String HT_OWN_001 = "HT_OWN_001";
    public static final String HT_OWN_002 = "HT_OWN_002";
    public static final String HT_OWN_003 = "HT_OWN_003";
    public static final String HT_OWN_004 = "HT_OWN_004";
    public static final String HT_OWN_005 = "HT_OWN_005";
    public static final String HT_OWN_006 = "HT_OWN_006";
    public static final String HT_OWN_007 = "HT_OWN_007";
    public static final String HT_OWN_008 = "HT_OWN_008";
    public static final String HT_OWN_009 = "HT_OWN_009";
    public static final String HT_OWN_010 = "HT_OWN_010";

    public static final String HT_IM_001 = "HT_IM_001";
    public static final String HT_IM_002 = "HT_IM_002";
    public static final String HT_IM_003 = "HT_IM_003";
    public static final String HT_IM_004 = "HT_IM_004";
    public static final String HT_IM_005 = "HT_IM_005";
    public static final String HT_IM_006 = "HT_IM_006";
    public static final String HT_IM_007 = "HT_IM_007";
    public static final String HT_IM_008 = "HT_IM_008";
    public static final String HT_IM_009 = "HT_IM_009";
    public static final String HT_IM_010 = "HT_IM_010";


    public static final String HT_FA_001 = "HT_FA_001";
    public static final String HT_FA_002 = "HT_FA_002";
    public static final String HT_FA_003 = "HT_FA_003";
    public static final String HT_FA_004 = "HT_FA_004";
    public static final String HT_FA_005 = "HT_FA_005";
    public static final String HT_FA_006 = "HT_FA_006";
    public static final String HT_FA_007 = "HT_FA_007";
    public static final String HT_FA_008 = "HT_FA_008";
    public static final String HT_FA_009 = "HT_FA_009";
    public static final String HT_FA_010 = "HT_FA_010";


    public static final String HT_001 = "HT_001";
    public static final String HT_002 = "HT_002";
    public static final String HT_003 = "HT_003";
    public static final String HT_004 = "HT_004";
    public static final String HT_005 = "HT_005";
    public static final String HT_006 = "HT_006";
    public static final String HT_007 = "HT_007";
    public static final String HT_008 = "HT_008";
    public static final String HT_009 = "HT_009";
    public static final String HT_010 = "HT_010";


    public static final String GT_001 = "GT_001";
    public static final String GT_002 = "GT_002";
    public static final String GT_003 = "GT_003";
    public static final String GT_004 = "GT_004";
    public static final String GT_005 = "GT_005";
    public static final String GT_006 = "GT_006";
    public static final String GT_007 = "GT_007";
    public static final String GT_008 = "GT_008";
    public static final String GT_009 = "GT_009";
    public static final String GT_010 = "GT_010";


    public static final String FB_001 = "FB_001";
    public static final String FB_002 = "FB_002";
    public static final String FB_003 = "FB_003";
    public static final String FB_004 = "FB_004";
    public static final String FB_005 = "FB_005";
    public static final String FB_006 = "FB_006";
    public static final String FB_007 = "FB_007";
    public static final String FB_008 = "FB_008";
    public static final String FB_009 = "FB_009";
    public static final String FB_010 = "FB_010";


    public static final String CT_G_001 = "CT_G_001";
    public static final String CT_G_002 = "CT_G_002";
    public static final String CT_G_003 = "CT_G_003";
    public static final String CT_G_004 = "CT_G_004";
    public static final String CT_G_005 = "CT_G_005";
    public static final String CT_G_006 = "CT_G_006";
    public static final String CT_G_007 = "CT_G_007";
    public static final String CT_G_008 = "CT_G_008";
    public static final String CT_G_009 = "CT_G_009";
    public static final String CT_G_010 = "CT_G_010";


    public static final String BKD_001 = "BKD_001";
    public static final String BKD_002 = "BKD_002";
    public static final String BKD_003 = "BKD_003";
    public static final String BKD_004 = "BKD_004";
    public static final String BKD_005 = "BKD_005";
    public static final String BKD_006 = "BKD_006";
    public static final String BKD_007 = "BKD_007";
    public static final String BKD_008 = "BKD_008";
    public static final String BKD_009 = "BKD_009";
    public static final String BKD_010 = "BKD_010";


    public static final String BK_001 = "BK_001";
    public static final String BK_002 = "BK_002";
    public static final String BK_003 = "BK_003";
    public static final String BK_004 = "BK_004";
    public static final String BK_005 = "BK_005";
    public static final String BK_006 = "BK_006";
    public static final String BK_007 = "BK_007";
    public static final String BK_008 = "BK_008";
    public static final String BK_009 = "BK_009";
    public static final String BK_010 = "BK_010";
    public static final String RL_011 = "RL_011";

    // Permission Group
    public static final String PMS_G_001 = "PMS_G_001";
    public static final String PMS_G_002 = "PMS_G_002";
    public static final String PMS_G_003 = "PMS_G_003";
    public static final String PMS_G_004 = "PMS_G_004";
    public static final String PMS_G_005 = "PMS_G_005";
    public static final String PMS_G_006 = "PMS_G_006";
    public static final String PMS_G_007 = "PMS_G_007";
    public static final String PMS_G_008 = "PMS_G_008";
    public static final String PMS_G_009 = "PMS_G_009";
    public static final String PMS_G_010 = "PMS_G_010";

    // Group

    public static final String GR_001 = "GR_001";
    public static final String GR_002 = "GR_002";
    public static final String GR_003 = "GR_003";
    public static final String GR_004 = "GR_004";
    public static final String GR_005 = "GR_005";
    public static final String GR_006 = "GR_006";
    public static final String GR_007 = "GR_007";
    public static final String GR_008 = "GR_008";
    public static final String GR_009 = "GR_009";
    public static final String GR_010 = "GR_010";
    public static final String GR_011 = "GR_011";

    // User Role

    public static final String USR_R_001 = "USR_R_001";
    public static final String USR_R_002 = "USR_R_002";
    public static final String USR_R_003 = "USR_R_003";
    public static final String USR_R_004 = "USR_R_004";
    public static final String USR_R_005 = "USR_R_005";
    public static final String USR_R_006 = "USR_R_006";
    public static final String USR_R_007 = "USR_R_007";
    public static final String USR_R_008 = "USR_R_008";
    public static final String USR_R_009 = "USR_R_009";
    public static final String USR_R_010 = "USR_R_010";

    public static final String RO_001 = "RO_001";
    public static final String RO_002 = "RO_002";
    public static final String RO_003 = "RO_003";
    public static final String RO_004 = "RO_004";
    public static final String RO_005 = "RO_005";
    public static final String RO_006 = "RO_006";
    public static final String RO_007 = "RO_007";
    public static final String RO_008 = "RO_008";
    public static final String RO_009 = "RO_009";
    public static final String RO_010 = "RO_010";
}
