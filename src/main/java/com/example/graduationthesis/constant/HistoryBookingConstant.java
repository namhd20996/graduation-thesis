package com.example.graduationthesis.constant;

public class HistoryBookingConstant {

    public static final String API_HISTORY_BOOKING = "/history-booking";
    public static final String API_FIND = "/find";
}
