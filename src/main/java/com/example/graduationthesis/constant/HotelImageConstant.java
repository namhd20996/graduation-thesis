package com.example.graduationthesis.constant;

public class HotelImageConstant {

    public static final String API_HOTEL_IMAGE = "/hotel-image";
    public static final String API_ADD_MORE = "/add-more";

}
