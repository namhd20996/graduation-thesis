package com.example.graduationthesis.constant;

public class HotelConstant {
    public static final String API_HOTEL = "/hotel";
    public static final String HOTEL_UPDATE_STATUS = "/update-status";
    public static final String HOTEL_UPDATE_OWNER = "/update-owner";
    public static final String HOTEL_GET_INDEX = "/get-index";
    public static final String HOTEL_GET_CONDITION = "/get-condition";
    public static final String HOTEL_GET_CONDITION_ID = "/get-condition/id";
    public static final String HOTEL_ADD_SESSION_ONE = "/session-one";
    public static final String HOTEL_ADD_SESSION_TWO = "/session-two";
}
