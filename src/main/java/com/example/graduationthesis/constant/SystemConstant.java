package com.example.graduationthesis.constant;

import java.util.UUID;

public class SystemConstant {

    public static final String API_PUBLIC = "/public";
    public static final String PARAM_STATUS = "status";

    public static final String API_ADMIN = "/admin";
    public static final String API_MEMBER = "/member";

    public static final String API_VERSION_ONE = "/v1";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String LIMIT_PAGE = "limitPage";
    public static final Integer STATUS_SUCCESS = 200;
    public static final Integer STATUS_BAD_REQUEST = 400;
    public static final Integer STATUS_FORBIDDEN = 403;
    public static final Integer STATUS_INTERNAL = 500;
    public static final Boolean ACTIVE = false;
    public static final Boolean NO_ACTIVE = true;

    public static final UUID GUEST_USER_ID = UUID.fromString("e464a5e6-9235-4b80-b113-6bdd5401345a");
    public static final UUID HOTEL_OWNER_ID = UUID.fromString("c81465b3-0f1c-4d1c-9ba7-f8f08f57ca08");
    public static final String API_ADD_NEW = "/add-new";
    public static final String API_GET_STATUS = "/get-status";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    public static final String HOTEL_SESSION_VNPAY = "sessionVNPAY";
    public static final String USER_SESSION_VNPAY = "sessionUser";
    public static final String BOOKING_SESSION_VNPAY = "sessionBooking";
    public static final String BOOKING_SESSIONS_VNPAY = "sessionBookings";
    public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String STATUS_NO_ACTIVE = "NO_ACTIVE";
    public static final Double COMMISSION_PERCENTAGE = 15.0;
    public static final String SESSION_REGISTER_HOTEL_OWNER = "sessionRegisterHotelOwner";
    public static final String SESSION_REGISTER_HOTEL_OWNER_MORE = "sessionRegisterHotelOwnerMore";
    public static final String USER_HOTEL_OWNER_REGISTER_CURRENT = "userHotelOwnerCurrent";
    public static final String USER_HOTEL_OWNER_REGISTER_CURRENT_MORE = "userHotelOwnerCurrentMore";
    public static final String HOTEL_TYPE_REGISTER_CURRENT = "hotelTypeRegisterCurrent";
    public static final String HOTEL_TYPE_REGISTER_CURRENT_MORE = "hotelTypeRegisterCurrentMore";
    public static final String HOTEL_REGISTER_CURRENT = "hotelRegisterCurrent";
    public static final String HOTEL_REGISTER_CURRENT_MORE = "hotelRegisterCurrentMore";
    public static final String ROOM_REGISTER_CURRENT = "roomRegisterCurrent";
    public static final String ROOM_REGISTER_CURRENT_MORE = "roomRegisterCurrentMore";
    public static final String SERVICE_AND_AMENITY_ROOM_REGISTER_CURRENT = "serviceAndAmnenityRegisterCurrent";
    public static final String SERVICE_AND_AMENITY_ROOM_REGISTER_CURRENT_MORE = "serviceAndAmnenityRegisterCurrentMore";
    public static final String HOTEL_IMAGE_REGISTER_CURRENT = "hotelImageRegisterCurrent";
    public static final String HOTEL_IMAGE_REGISTER_CURRENT_MORE = "hotelImageRegisterCurrentMore";
    public static final String HOTEL_POLICY_REGISTER_CURRENT_MORE = "hotelPolicyRegisterCurrentMore";
    public static final String HOTEL_POLICY_REGISTER_CURRENT = "hotelPolicyRegisterCurrent";
    public static final String POLICY_CONFIRMATION_REGISTER_CURRENT = "policyConfirmationRegisterCurrent";
    public static final String HOTEL_PAYMENT_MEDTHOD_REGISTER_CURRENT = "hotelPaymentMedthodRegisterCurrent";
    public static final String HOTEL_PAYMENT_MEDTHOD_REGISTER_CURRENT_MORE = "hotelPaymentMedthodRegisterCurrentMore";
    public static final String FLAG_ADDRESS_HOTEL_PAYMENT_MEDTHOD = "flageAddressHotelPaymentMedthod";
    public static final String FLAG_ADDRESS_HOTEL_PAYMENT_MEDTHOD_MORE = "flageAddressHotelPaymentMedthodMore";
    public static final Integer STATUS_ACCEPT_GENERAL_TERMS_PRIVACY_ACTIVE = 1;
    public static final Integer STATUS_CERTIFICATION_OF_LEGAL_BUSINESS_OPERATIONS_ACTIVE = 1;
    public static final String ROLE_NAME_CUSTOM = "ROLE_";
    public static final String ROLE_CODE_CUSTOM = "CODE_";
}
