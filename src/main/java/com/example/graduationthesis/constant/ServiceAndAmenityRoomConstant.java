package com.example.graduationthesis.constant;

public class ServiceAndAmenityRoomConstant {

    public static final String API_SERVICE_AND_AMENITY_ROOM = "/service-amenity-room";
    public static final String API_ADD_NEW = "/add-new";

}
