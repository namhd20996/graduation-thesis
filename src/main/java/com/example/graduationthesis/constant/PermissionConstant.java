package com.example.graduationthesis.constant;

public class PermissionConstant {
    public static final String API_PERMISSION = "/permission";
    public static final String API_PERMISSION_ID = "/{permissionId}";
    public static final String PATH_PERMISSION_ID = "permissionId";
}
