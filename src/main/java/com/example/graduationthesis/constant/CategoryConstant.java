package com.example.graduationthesis.constant;

public class CategoryConstant {

    public static final String API_CATEGORY = "/category";
    public static final String API_GET_BY_STATUS = "/get-by-status";
}
