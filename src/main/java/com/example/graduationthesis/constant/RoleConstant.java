package com.example.graduationthesis.constant;

public class RoleConstant {

    public static final String API_ROLE = "/role";

    public static final String API_ROLE_ID = "/{roleId}";
    public static final String PATH_ROLE_ID = "roleId";
}
