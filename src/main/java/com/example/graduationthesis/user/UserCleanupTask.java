package com.example.graduationthesis.user;

import com.example.graduationthesis.bookingprocess.entity.BookingProcess;
import com.example.graduationthesis.bookingprocess.repo.BookingProcessRepo;
import com.example.graduationthesis.confirmtoken.ConfirmationToken;
import com.example.graduationthesis.confirmtoken.ConfirmationTokenRepo;
import com.example.graduationthesis.historybooking.entity.HistoryBooking;
import com.example.graduationthesis.historybooking.repo.HistoryBookingRepo;
import com.example.graduationthesis.jwt.JwtService;
import com.example.graduationthesis.roominfobookingprocess.entity.RoomInfo;
import com.example.graduationthesis.roominfobookingprocess.repo.RoomInfoRepo;
import com.example.graduationthesis.session.hotel.HotelSession;
import com.example.graduationthesis.session.hotel.HotelSessionRepo;
import com.example.graduationthesis.session.hotelimage.HotelImageSession;
import com.example.graduationthesis.session.hotelimage.HotelImageSessionRepo;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSession;
import com.example.graduationthesis.session.hotelpaymentmethod.HotelPaymentMethodSessionRepo;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySession;
import com.example.graduationthesis.session.hotelpolicy.HotelPolicySessionRepo;
import com.example.graduationthesis.session.room.RoomSession;
import com.example.graduationthesis.session.room.RoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSession;
import com.example.graduationthesis.session.serviceamenityroom.ServiceAmenityRoomSessionRepo;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomId;
import com.example.graduationthesis.session.serviceamenityroomid.ServiceAmenityRoomIdRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.repo.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class UserCleanupTask {

    private final UserRepo userRepo;

    private final HistoryBookingRepo historyBookingRepo;

    private final ConfirmationTokenRepo confirmationTokenRepo;

    private final JwtService jwtService;

    private final BookingProcessRepo bookingProcessRepo;

    private final RoomInfoRepo roomInfoRepo;

    private final HotelSessionRepo hotelSessionRepo;

    private final ServiceAmenityRoomSessionRepo serviceAmenityRoomSessionRepo;

    private final ServiceAmenityRoomIdRepo serviceAmenityRoomIdRepo;

    private final HotelPolicySessionRepo hotelPolicySessionRepo;

    private final HotelPaymentMethodSessionRepo hotelPaymentMethodSessionRepo;

    private final RoomSessionRepo roomSessionRepo;

    private final HotelImageSessionRepo hotelImageSessionRepo;

    @Scheduled(fixedRate = 900_000)
    public void cleanupInactiveUsers() {
        List<ConfirmationToken> confirmationTokens = confirmationTokenRepo
                .findAllByConfirmedAtAndExpiresAtBefore(null, LocalDateTime.now().minusMinutes(15))
                .orElse(null);

        if (confirmationTokens != null) {
            List<User> users = confirmationTokens.stream()
                    .map(ConfirmationToken::getUser)
                    .toList();

            confirmationTokenRepo.deleteAll(confirmationTokens);
            userRepo.deleteAll(users);
        }
    }

    @Scheduled(cron = "0 0 0 1 */6 ?")
    public void cleanupHistoryBooking() {
        List<HistoryBooking> historyBookings =
                historyBookingRepo.findHistoryBookingsByBookingDateBefore(LocalDate.now().plusMonths(6))
                        .orElseThrow(null);

        if (!historyBookings.isEmpty())
            historyBookingRepo.deleteAll(historyBookings);
    }

    @Scheduled(fixedRate = 900_00)
    public void cleanBooking() {
        List<BookingProcess> bookingProcesses = bookingProcessRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<RoomInfo> roomInfos = roomInfoRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        bookingProcessRepo.deleteAll(bookingProcesses);
        roomInfoRepo.deleteAll(roomInfos);
    }

    @Scheduled(fixedRate = 180_000)
    public void cleanSessionAddHotel() {
        List<HotelSession> hotelSessions = hotelSessionRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<HotelImageSession> hotelImageSessions = hotelImageSessionRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<HotelPaymentMethodSession> hotelPaymentMethodSessions = hotelPaymentMethodSessionRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<HotelPolicySession> hotelPolicySessions = hotelPolicySessionRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<RoomSession> roomSessions = roomSessionRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<ServiceAmenityRoomId> serviceAmenityRoomIds = serviceAmenityRoomIdRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        List<ServiceAmenityRoomSession> serviceAmenityRoomSessions = serviceAmenityRoomSessionRepo.findAll().stream()
                .filter(item -> jwtService.isTokenExpired(item.getJwtToken()))
                .toList();

        hotelSessionRepo.deleteAll(hotelSessions);
        hotelImageSessionRepo.deleteAll(hotelImageSessions);
        hotelPolicySessionRepo.deleteAll(hotelPolicySessions);
        hotelPaymentMethodSessionRepo.deleteAll(hotelPaymentMethodSessions);
        roomSessionRepo.deleteAll(roomSessions);
        serviceAmenityRoomIdRepo.deleteAll(serviceAmenityRoomIds);
        serviceAmenityRoomSessionRepo.deleteAll(serviceAmenityRoomSessions);
    }
}
