package com.example.graduationthesis.user.repo;

import com.example.graduationthesis.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface UserRepo extends JpaRepository<User, UUID> {

    Optional<User> findUsersByIdAndIsDeleted(UUID id, Boolean isDeleted);

    Optional<User> findUserByEmail(String email);

    Optional<User> findUserByEmailAndIsDeleted(String email, Boolean isDeleted);

    Optional<User> findUserByIdAndIsDeleted(UUID userId, Boolean isDeleted);

    @Query("""
            SELECT
                u
            FROM
                User u
            LEFT JOIN
                UserRole ur ON ur.user.id = u.id
            LEFT JOIN
                Role  r ON r.id = ur.role.id
            LEFT JOIN
                PermissionRole pmr ON pmr.role.id = r.id
            LEFT JOIN
                Permission pm ON pm.id = pmr.permission.id
            LEFT JOIN
                PermissionGroup pmg ON pmg.permission.id = pm.id
            LEFT JOIN
                Group g ON g.id = pmg.group.id
            WHERE NOT
                g.groupName = 'ADMIN_SUPER'
            AND
                (:isHotelOwner = true AND pm.permissionCode = 'hotel_owner' OR :isHotelOwner = false)
            AND
                (:status IS NULL OR u.status = :status)
            """)
    Page<User> findUsersNotAdminMaster(@Param("isHotelOwner") Boolean isHotelOwner,@Param("status") String status ,Pageable pageable);

    Boolean existsUserByEmail(String email);

    Optional<User> findUserByEmailAndStatus(String email, String status);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("""
                UPDATE User u
                SET u.status = 'ACTIVE'
                WHERE u.email = ?1
            """)
    void updateStatusByEmail(String email);

    @Query("""
                SELECT
                    u
                FROM
                    User u
                INNER JOIN
                    Token t ON t.user.id = u.id
                 WHERE
                    t.expired = false
                 AND
                    t.revoked = false
                 AND
                    t.token = ?1
            """)
    Optional<User> findUserByToken(String token);

}
