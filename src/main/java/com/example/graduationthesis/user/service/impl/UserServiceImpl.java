package com.example.graduationthesis.user.service.impl;

import com.example.graduationthesis.confirmtoken.ConfirmationToken;
import com.example.graduationthesis.confirmtoken.ConfirmationTokenDTO;
import com.example.graduationthesis.confirmtoken.ConfirmationTokenService;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.constant.UserConstant;
import com.example.graduationthesis.emai.EmailService;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.image.ImageService;
import com.example.graduationthesis.jwt.JwtService;
import com.example.graduationthesis.permission.dto.response.PermissionCodeResp;
import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.permission.repo.PermissionRepo;
import com.example.graduationthesis.role.entity.Role;
import com.example.graduationthesis.role.repo.RoleRepo;
import com.example.graduationthesis.token.Token;
import com.example.graduationthesis.token.TokenRepo;
import com.example.graduationthesis.token.TokenType;
import com.example.graduationthesis.user.dto.request.EmailRequest;
import com.example.graduationthesis.user.dto.request.UserLoginRequest;
import com.example.graduationthesis.user.dto.request.UserRegisterRequest;
import com.example.graduationthesis.user.dto.request.UserUpdateRequest;
import com.example.graduationthesis.user.dto.response.RefreshTokenResponse;
import com.example.graduationthesis.user.dto.response.UserDTOAuthenticatedResp;
import com.example.graduationthesis.user.dto.response.UserResponse;
import com.example.graduationthesis.user.dto.response.UserResponses;
import com.example.graduationthesis.user.dto.response.UsersDTOResp;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.mapper.UserDTOMapper;
import com.example.graduationthesis.user.repo.UserRepo;
import com.example.graduationthesis.user.service.UserService;
import com.example.graduationthesis.userrole.entity.UserRole;
import com.example.graduationthesis.userrole.repo.UserRoleRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.BaseUrlServiceUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final BaseUrlServiceUtil baseUrlService;

    private final UserRepo userRepo;

    private final UserDTOMapper userDTOMapper;

    private final RoleRepo roleRepo;

    private final UserRoleRepo userRoleRepo;

    private final TokenRepo tokenRepo;

    private final JwtService jwtService;

    private final EmailService emailService;

    private final BaseAmenityUtil baseAmenityUtil;

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    private final ConfirmationTokenService confirmationTokenService;

    private final HttpServletResponse resp;

    private final PermissionRepo permissionRepo;

    private final ImageService imageService;

    @Value("${application.url-resp-login-social}")
    private String urlResp;

    @Value("${application.url-confirm}")
    private String urlConfirmUser;

    @Value("${application.url-confirm-hotel-owner}")
    private String urlConfirmHotelOwner;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public UserResponse registerUser(Boolean key, UserRegisterRequest request) {
        checkEmail(request.getEmail());
        Role role = roleRepo.findRoleByIdAndStatus(key ? SystemConstant.HOTEL_OWNER_ID : SystemConstant.GUEST_USER_ID, SystemConstant.STATUS_ACTIVE)
                .orElseThrow(() -> new ApiRequestException("RL_001", "Find role by id not found!.."));

        User user = getUserRegister(request);
        userRoleRepo.save(
                UserRole.builder()
                        .user(user)
                        .role(role)
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        String tokenConfirm = jwtService.generateToken(user);
        confirmationTokenService.saveConfirmationToken(
                new ConfirmationToken(
                        tokenConfirm,
                        LocalDateTime.now(),
                        LocalDateTime.now().plusMinutes(15),
                        user
                )
        );

        String link = key ? baseUrlService.getBaseUrl() + UserConstant.URL_CONFIRM + tokenConfirm + "&owner=true"
                : baseUrlService.getBaseUrl() + UserConstant.URL_CONFIRM + tokenConfirm + "&owner=false";
        saveUserToken(user, tokenConfirm);
        emailService.send(
                user.getEmail(),
                emailService.buildEmail(user.getUsername(), link, "", false),
                "Confirm your email",
                null
        );

        return UserResponse.builder()
                .code(ResourceConstant.USR_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userDTOMapper.apply(user))
                .message(getMessageBundle(ResourceConstant.USR_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserResponse authenticate(UserLoginRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        User user = getUser(request.getEmail());
        var jwtToken = jwtService.generateToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);
        return UserResponse.builder()
                .code(ResourceConstant.USR_011)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new UserDTOAuthenticatedResp(jwtToken, jwtService.generateRefreshToken(user),
                        TimeUnit.MICROSECONDS.toSeconds(jwtService.extractExpired(jwtToken).getTime())))
                .message(baseAmenityUtil.getMessageBundle(ResourceConstant.USR_011))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private User getUser(String email) {
        return userRepo.findUserByEmailAndStatus(email, SystemConstant.STATUS_ACTIVE)
                .orElseThrow(() -> new UsernameNotFoundException("find user by " + email + " not found!."));
    }

    private User getUserByEmailAndIsDeleted(String token) {
        final String jwt = token.substring(7);
        final String email = jwtService.extractUsername(jwt);
        return getUser(email);
    }

    @Override
    public UserResponse findUserByToken(String token) {
        User user = getUserByEmailAndIsDeleted(token);

        return UserResponse.builder()
                .code(ResourceConstant.USR_012)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userDTOMapper.apply(user))
                .message(getMessageBundle(ResourceConstant.USR_012))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserResponse findPermissionsByToken(String token) {
        User user = getUserByEmailAndIsDeleted(token);

        List<Permission> permissions = permissionRepo.findPermissionsByUserId(user.getId());
        List<PermissionCodeResp> permissionCodeResps = permissions.stream()
                .map(permission -> new PermissionCodeResp(permission.getPermissionCode()))
                .toList();

        return UserResponse.builder()
                .code(ResourceConstant.USR_013)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(permissionCodeResps)
                .message(getMessageBundle(ResourceConstant.USR_013))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserResponse authenticate(OAuth2User request) {
        User user = null;
        try {
            if (!userRepo.existsUserByEmail(request.getAttribute("email"))) {
                user = userRepo.save(
                        User.builder()
                                .email(request.getAttribute("email"))
                                .avatar(request.getAttribute("picture"))
                                .password(passwordEncoder.encode(generateRandomPassword()))
                                .isDeleted(SystemConstant.ACTIVE)
                                .status(SystemConstant.STATUS_ACTIVE)
                                .build()
                );
            }

            if (userRepo.existsUserByEmail(request.getAttribute("email"))) {
                user = userRepo.findUserByEmailAndStatus(request.getAttribute("email"), SystemConstant.STATUS_ACTIVE)
                        .orElseThrow(() -> new ApiRequestException("find user not found!", ""));
            }

            assert user != null;
            String encodedUsername = URLEncoder.encode(Objects.requireNonNull(request.getAttribute("name")), StandardCharsets.UTF_8);
            String encodedEmail = URLEncoder.encode(user.getEmail(), StandardCharsets.UTF_8);
            String encodedAvatar = URLEncoder.encode(user.getAvatar(), StandardCharsets.UTF_8);

            var jwtToken = jwtService.generateToken(user);
            String encodedToken = URLEncoder.encode(jwtToken, StandardCharsets.UTF_8);
            revokeAllUserTokens(user);
            saveUserToken(user, jwtToken);
            resp.sendRedirect(urlResp + "?name=" + encodedUsername + "&email=" + encodedEmail + "&avatar=" + encodedAvatar + "&token=" + encodedToken + "&role=USER");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        assert user != null;
        return UserResponse.builder()
                .code(ResourceConstant.USR_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userDTOMapper.apply(user))
                .message(getMessageBundle(ResourceConstant.USR_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public String responseLoginOAuth2Fail() {
        return "<body style=\"font-family: Arial, sans-serif; background-color: #f4f4f4; text-align: center; margin: 0; padding: 0; height: 100vh; display: flex; align-items: center; justify-content: center;\">\n" +
                "\n" +
                "    <div style=\"background-color: #fff; padding: 20px; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);\">\n" +
                "        <h1 style=\"color: #ff6347;\">Login Failed</h1>\n" +
                "        <p>Sorry, your login attempt was unsuccessful. Please check your credentials and try again.</p>\n" +
                "        <a style=\"margin-top: 20px; display: block; text-decoration: none; color: #3498db; font-weight: bold;\" href=\"/\">Go to Home</a>\n" +
                "    </div>\n" +
                "</body>";
    }

    @Override
    public UserResponse confirmationToken(String token, Boolean isOwner) {
        ConfirmationTokenDTO confirmation = confirmationTokenService.findByToken(token);
        if (confirmation.getConfirmedAt() != null) {
            throw new ApiRequestException("CFMT_002", "user already confirm!.");
        }

        LocalDateTime expiredAt = confirmation.getExpiresAt();
        if (expiredAt.isBefore(LocalDateTime.now())) {
            throw new ApiRequestException("CFMT_001", "token  expiredAt!.");
        }

        if (isOwner) {
            try {
                resp.sendRedirect(urlConfirmHotelOwner);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            try {
                resp.sendRedirect(urlConfirmUser);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        confirmationTokenService.setConfirmAt(token);
        updateStatusByEmail(confirmation.getEmail());

        emailService.send(
                confirmation.getEmail(),
                emailService.buildEmailWelcome(confirmation.getEmail()),
                "Welcome to Staying",
                null
        );

        return UserResponse.builder()
                .code("USR_C_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new RefreshTokenResponse(token, TimeUnit.MICROSECONDS.toSeconds(jwtService.extractExpired(token).getTime())))
                .message("Confirm success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public void updateStatusByEmail(String email) {
        userRepo.updateStatusByEmail(email);
    }

    @Override
    public void forgotPassword(String email) {
        User user = userRepo.findUserByEmailAndStatus(email, SystemConstant.STATUS_ACTIVE)
                .orElseThrow(() -> new UsernameNotFoundException("find user by email " + email + " not found!."));

        String password = generateRandomPassword();
        emailService.send(
                user.getEmail(),
                emailService.buildEmail("", "", password, true),
                "Envoyer un nouveau mot de passe",
                null
        );
        user.setPassword(passwordEncoder.encode(password));
        userRepo.save(user);
    }

    @Override
    public UserResponses findAll(Boolean isHotelOwner, String status, Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<User> all = userRepo.findUsersNotAdminMaster(isHotelOwner, status, pageable);

        List<UsersDTOResp> users = all.stream()
                .map(userDTOMapper)
                .toList();

        return UserResponses.builder()
                .code(ResourceConstant.USR_001)
                .status(200)
                .data(users)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("Get users success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public boolean existsUserByEmail(String email) {
        return userRepo.existsUserByEmail(email);
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepo.findAllValidTokensByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(t -> {
            t.setExpired(true);
            t.setRevoked(true);
        });
        tokenRepo.saveAll(validUserTokens);
    }

    private void saveUserToken(User user, String jwtToken) {
        var token = Token.builder()
                .user(user)
                .tokenType(TokenType.BEARER)
                .token(jwtToken)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepo.save(token);
    }

    private String generateRandomPassword() {
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCase = "abcdefghijklmnopqrstuvwxyz";
        String digits = "0123456789";
        String specialChars = "@#$%^&+=";

        String allChars = upperCase + lowerCase + digits + specialChars;

        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder();

        // Add at least one digit
        int randomDigitIndex = random.nextInt(digits.length());
        password.append(digits.charAt(randomDigitIndex));

        // Add at least one lowercase letter
        int randomLowerCaseIndex = random.nextInt(lowerCase.length());
        password.append(lowerCase.charAt(randomLowerCaseIndex));

        // Add at least one uppercase letter
        int randomUpperCaseIndex = random.nextInt(upperCase.length());
        password.append(upperCase.charAt(randomUpperCaseIndex));

        // Add at least one special character
        int randomSpecialCharIndex = random.nextInt(specialChars.length());
        password.append(specialChars.charAt(randomSpecialCharIndex));

        // Fill the rest of the password with random characters
        for (int i = 0; i < 4; i++) {
            int randomIndex = random.nextInt(allChars.length());
            password.append(allChars.charAt(randomIndex));
        }

        // Shuffle the password characters

        return shuffleString(password.toString());
    }

    private static String shuffleString(String string) {
        char[] characters = string.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            int randomIndex = (int) (Math.random() * characters.length);
            char temp = characters[i];
            characters[i] = characters[randomIndex];
            characters[randomIndex] = temp;
        }
        return new String(characters);
    }

    private User getUserRegister(UserRegisterRequest request) {
        return userRepo.save(
                User.builder()
                        .email(request.getEmail())
                        .password(passwordEncoder.encode(request.getPassword()))
                        .firstName(request.getFirstName().trim())
                        .lastName(request.getLastName().trim())
                        .phoneNumber(request.getPhoneNumber())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_NO_ACTIVE)
                        .build()
        );
    }

    private void checkEmail(String email) {
        if (existsUserByEmail(email))
            throw new ApiRequestException("EM_001", "email : " + email + " already exist!.");
    }

    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;
        if (authHeader == null || !authHeader.startsWith(SystemConstant.BEARER)) {
            return;
        }
        refreshToken = authHeader.substring(7);
        userEmail = jwtService.extractUsername(refreshToken);
        if (userEmail != null) {
            var user = this.userRepo.findUserByEmail(userEmail)
                    .orElseThrow();
            if (jwtService.isValidToken(refreshToken, user)) {
                var accessToken = jwtService.generateToken(user);
                revokeAllUserTokens(user);
                saveUserToken(user, accessToken);
                var authResponse = UserResponse.builder()
                        .code("RFT_001")
                        .status(SystemConstant.STATUS_SUCCESS)
                        .data(new UserDTOAuthenticatedResp(accessToken, refreshToken,
                                TimeUnit.MICROSECONDS.toSeconds(jwtService.extractExpired(accessToken).getTime())))
                        .message("Refresh token success!..")
                        .responseTime(baseAmenityUtil.responseTime())
                        .build();
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
            }
        }
    }

    @Override
    public UserResponse checkEmail(EmailRequest request) {
        if (existsUserByEmail(request.getEmail()))
            throw new ApiRequestException("EM_001", "email : " + request.getEmail() + " already exist!.");

        return UserResponse.builder()
                .code("USR_013")
                .status(SystemConstant.STATUS_SUCCESS)
                .data("Email have register!")
                .message("Email not exists!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public UserResponse updateUser(String jwtToken, UserUpdateRequest request) {
        User user = getUserByEmailAndIsDeleted(jwtToken);
        String avatar = null;
        if (request.getAvatar() != null) {
            avatar = baseUrlService.getBaseUrl() + SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + "/image/" + imageService.saveImage(request.getAvatar());
        }

        if (!user.getFirstName().equals(request.getFirstName())) {
            user.setFirstName(request.getFirstName());
        }

        if (!user.getLastName().equals(request.getLastName())) {
            user.setLastName(request.getLastName());
        }

        if (user.getAddress() == null || !user.getAddress().equals(request.getAddress())) {
            user.setAddress(request.getAddress());
        }

        if (user.getGender() == null || !user.getGender().equals(request.getGender())) {
            user.setGender(request.getGender());
        }

        if (user.getDob() == null || !Objects.equals(user.getDob(), request.getDob())) {
            user.setDob(request.getDob());
        }

        if (user.getPhoneNumber() == null || !user.getPhoneNumber().equals(request.getPhoneNumber())) {
            user.setPhoneNumber(request.getPhoneNumber());
        }

        if (avatar != null) {
            user.setAvatar(avatar);
        }
        User save = userRepo.save(user);

        return UserResponse.builder()
                .code("USR_011")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(userDTOMapper.apply(save))
                .message("Update user success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

}
