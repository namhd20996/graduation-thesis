package com.example.graduationthesis.user.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateRequest {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private LocalDate dob;

    private String gender;

    private String address;

    private MultipartFile avatar;
}
