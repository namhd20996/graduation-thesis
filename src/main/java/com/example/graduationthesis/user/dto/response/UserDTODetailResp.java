package com.example.graduationthesis.user.dto.response;

import com.example.graduationthesis.permission.dto.PermissionDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class UserDTODetailResp extends AbsUserDTO {

    private List<PermissionDto> permissions;
}
