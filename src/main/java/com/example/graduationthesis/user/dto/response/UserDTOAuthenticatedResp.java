package com.example.graduationthesis.user.dto.response;

public record UserDTOAuthenticatedResp(
        String jwtToken,
        String refreshToken,
        Long expiration
) {
}
