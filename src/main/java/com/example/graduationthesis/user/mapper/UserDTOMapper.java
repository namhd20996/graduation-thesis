package com.example.graduationthesis.user.mapper;

import com.example.graduationthesis.user.dto.response.UserDTODetailResp;
import com.example.graduationthesis.user.dto.response.UsersDTOResp;
import com.example.graduationthesis.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class UserDTOMapper implements Function<User, UsersDTOResp> {

    public UserDTODetailResp applyUserDetail(User user) {

        return UserDTODetailResp.builder()
                .id(user.getId())
                .createdDate(user.getCreatedDate())
                .createdBy(user.getCreatedBy())
                .lastModifiedDate(user.getLastModifiedDate())
                .lastModifiedBy(user.getLastModifiedBy())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .phoneNumber(user.getPhoneNumber())
                .address(user.getAddress())
                .dob(user.getDob())
                .gender(user.getGender())
                .status(user.getStatus())
                .build();
    }


    @Override
    public UsersDTOResp apply(User user) {
        return getUsersDTOResp(user);
    }

    private UsersDTOResp getUsersDTOResp(User user) {
        return UsersDTOResp.builder()
                .id(user.getId())
                .createdDate(user.getCreatedDate())
                .createdBy(user.getCreatedBy())
                .lastModifiedDate(user.getLastModifiedDate())
                .lastModifiedBy(user.getLastModifiedBy())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .phoneNumber(user.getPhoneNumber())
                .address(user.getAddress())
                .dob(user.getDob())
                .gender(user.getGender())
                .status(user.getStatus())
                .isDeleted(user.getIsDeleted())
                .avatar(user.getAvatar())
                .build();
    }
}
