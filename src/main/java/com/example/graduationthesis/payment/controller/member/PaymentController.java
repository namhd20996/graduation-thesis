package com.example.graduationthesis.payment.controller.member;

import com.example.graduationthesis.constant.PaymentConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.payment.dto.request.PaymentDirectRequest;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueDay;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueMonth;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueYear;
import com.example.graduationthesis.payment.service.PaymentService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + PaymentConstant.API_PAYMENT)
@RequiredArgsConstructor
public class PaymentController {

    private final PaymentService paymentService;

    @GetMapping(PaymentConstant.API_CREATE)
    public ResponseEntity<?> createVnPay(HttpServletRequest req, HttpServletResponse resp) {
        paymentService.createPaymentVnPay(req, resp);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(PaymentConstant.API_CHECK_VN_PAY)
    public ResponseEntity<?> checkVnPay(
            @RequestParam("vnp_Amount") BigDecimal paymentAmount,
            @RequestParam("vnp_CardType") String paymentMethod,
            @RequestParam("vnp_ResponseCode") String vnp_ResponseCode
    ) {
        paymentService.checkVnPay(paymentAmount, paymentMethod, vnp_ResponseCode);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(PaymentConstant.API_ADD_DIRECT_PAYMENT)
    public ResponseEntity<?> createDirectPayment(
            @Valid @RequestBody PaymentDirectRequest request
    ) {
        paymentService.createDirectPayment(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(PaymentConstant.API_STATISTIC_DAY)
    public ResponseEntity<List<PaymentRevenueDay>> revenueReportByDay(
            @RequestParam("month") Integer month,
            @RequestParam("year") Integer year,
            @RequestParam("hid") UUID hid
    ) {
        return new ResponseEntity<>(paymentService.revenueReportByDay(month, year, hid), HttpStatus.OK);
    }

    @GetMapping(PaymentConstant.API_STATISTIC_YEAR)
    public ResponseEntity<List<PaymentRevenueMonth>> revenueReportByMonth(
            @RequestParam("year") Integer year,
            @RequestParam("hid") UUID hid
    ) {
        return new ResponseEntity<>(paymentService.revenueReportByMonth(year, hid), HttpStatus.OK);
    }

    @GetMapping(PaymentConstant.API_STATISTIC)
    public ResponseEntity<List<PaymentRevenueYear>> yearlySummaryStatistics(@RequestParam("hid") UUID hid) {
        return new ResponseEntity<>(paymentService.yearlySummaryStatistics(hid), HttpStatus.OK);
    }

}
