package com.example.graduationthesis.payment.controller.admin;

import com.example.graduationthesis.constant.PaymentConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.payment.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + PaymentConstant.API_PAYMENT)
public class PaymentAdminController {

    private final PaymentService paymentService;

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @PutMapping("/{bookingId}/{status}")
    public ResponseEntity<?> changeStatusPayment(
            @PathVariable("bookingId") UUID bookingId,
            @PathVariable("status") Optional<String> status,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(paymentService.changeStatusPaymentByBookingId(
                bookingId,
                status.orElse(PaymentConstant.STATUS_PAYMENT_PENDING),
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK
        );
    }
}
