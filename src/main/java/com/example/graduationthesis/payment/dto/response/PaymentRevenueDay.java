package com.example.graduationthesis.payment.dto.response;

public record PaymentRevenueDay(
        Integer day,
        Integer month,
        Integer year,
        Double totalAmount
) {
}
