package com.example.graduationthesis.payment.dto.response;

public record PaymentRevenueYear(
        Integer year,
        Double totalAmount
) {
}
