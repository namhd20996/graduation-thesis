package com.example.graduationthesis.payment.dto.response;

public record PaymentRevenueMonth(
        Integer month,
        Integer year,
        Double totalAmount
) {
}
