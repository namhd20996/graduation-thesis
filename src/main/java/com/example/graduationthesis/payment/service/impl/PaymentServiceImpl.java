package com.example.graduationthesis.payment.service.impl;

import com.example.graduationthesis.billingaddress.BillingAddress;
import com.example.graduationthesis.billingaddress.BillingAddressRepo;
import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.booking.repo.BookingRepo;
import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.config.PaymentConfig;
import com.example.graduationthesis.constant.PaymentConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.hotel.repo.HotelRepo;
import com.example.graduationthesis.payment.dto.PaymentDTO;
import com.example.graduationthesis.payment.dto.request.PaymentDirectRequest;
import com.example.graduationthesis.payment.dto.response.PaymentResponses;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueDay;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueMonth;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueYear;
import com.example.graduationthesis.payment.entity.Payment;
import com.example.graduationthesis.payment.mapper.PaymentDtoMapper;
import com.example.graduationthesis.payment.repo.PaymentRepo;
import com.example.graduationthesis.payment.service.PaymentService;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.room.repo.RoomRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.AbsUserServiceUtil;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.SessionServiceUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.*;
import java.util.stream.IntStream;

@Service
@Transactional
@RequiredArgsConstructor
public class PaymentServiceImpl extends AbsUserServiceUtil implements PaymentService {

    private final PaymentRepo paymentRepo;

    private final BillingAddressRepo billingAddressRepo;

    private final HotelRepo hotelRepo;

    private final BookingRepo bookingRepo;

    private final RoomRepo roomRepo;

    private final SessionServiceUtil sessionServiceUtil;


    private final BaseAmenityUtil baseAmenityUtil;

    private final PaymentDtoMapper paymentDtoMapper;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public void createPaymentVnPay(HttpServletRequest req, HttpServletResponse resp) {
        try {
            Map<String, Object> value = new HashMap<>();

            Hotel hotel = getHotel(UUID.fromString(req.getParameter("hid")));

            Booking booking = getBooking(UUID.fromString(req.getParameter("bid")));

            value.put(SystemConstant.HOTEL_SESSION_VNPAY, hotel);
            value.put(SystemConstant.BOOKING_SESSION_VNPAY, booking);

            sessionServiceUtil.putValue(SystemConstant.BOOKING_SESSIONS_VNPAY, value);

            String vnp_Version = "2.1.0";
            String vnp_Command = "pay";
            String vnp_OrderInfo = "Thanh toan cho nghi"; // Thông tin mô tả nội dung thanh toán (Tiếng Việt, không dấu). Ví dụ: **Nap tien cho thue bao 0123456789. So tien 100,000 VND**
            String orderType = "170000";//Mã danh mục hàng hóa.
            String vnp_TxnRef = PaymentConfig.getRandomNumber(8);
            String vnp_IpAddr = PaymentConfig.getIpAddress(req); // Lấy IP khách hàng giao dịch
            String vnp_TmnCode = PaymentConfig.vnp_TmnCode;

            int amount = Integer.parseInt(req.getParameter("amount")) * 100;
            Map vnp_Params = new HashMap<>();
            vnp_Params.put("vnp_Version", vnp_Version);
            vnp_Params.put("vnp_Command", vnp_Command);
            vnp_Params.put("vnp_TmnCode", vnp_TmnCode); // Mã website
            vnp_Params.put("vnp_Amount", String.valueOf(amount));
            vnp_Params.put("vnp_CurrCode", "VND");
            String bank_code = req.getParameter("bankcode"); // Mã ngân hàng VD: NCB
            if (bank_code != null && !bank_code.isEmpty()) {
                vnp_Params.put("vnp_BankCode", bank_code);
            }
            vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
            vnp_Params.put("vnp_OrderInfo", vnp_OrderInfo);
            vnp_Params.put("vnp_OrderType", orderType);

            String locate = req.getParameter("language");
            if (locate != null && !locate.isEmpty()) {
                vnp_Params.put("vnp_Locale", locate);
            } else {
                vnp_Params.put("vnp_Locale", "vn");
            }
            vnp_Params.put("vnp_ReturnUrl", PaymentConfig.vnp_ReturnUrl);
            vnp_Params.put("vnp_IpAddr", vnp_IpAddr);
            Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));

            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
            String vnp_CreateDate = formatter.format(cld.getTime());

            vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
            cld.add(Calendar.MINUTE, 15);
            String vnp_ExpireDate = formatter.format(cld.getTime());
            //Add Params of 2.1.0 Version
            vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);
            //Billing
            vnp_Params.put("vnp_Bill_Mobile", req.getParameter("txt_billing_mobile"));
            vnp_Params.put("vnp_Bill_Email", req.getParameter("txt_billing_email"));
            String fullName = (req.getParameter("txt_billing_fullname")).trim();
            if (fullName != null && !fullName.isEmpty()) {
                int idx = fullName.indexOf(' ');
                String firstName = fullName.substring(0, idx);
                String lastName = fullName.substring(fullName.lastIndexOf(' ') + 1);
                vnp_Params.put("vnp_Bill_FirstName", firstName);
                vnp_Params.put("vnp_Bill_LastName", lastName);

            }
            vnp_Params.put("vnp_Bill_Address", hotel.getStreetAddress());
            vnp_Params.put("vnp_Bill_City", hotel.getCity());
            vnp_Params.put("vnp_Bill_Country", hotel.getCountry());
            if (req.getParameter("txt_bill_state") != null && !req.getParameter("txt_bill_state").isEmpty()) {
                vnp_Params.put("vnp_Bill_State", req.getParameter("txt_bill_state"));
            }
            // Invoice
//        vnp_Params.put("vnp_Inv_Phone", req.getParameter("txt_inv_mobile"));
//        vnp_Params.put("vnp_Inv_Email", req.getParameter("txt_inv_email"));
//        vnp_Params.put("vnp_Inv_Customer", req.getParameter("txt_inv_customer"));
//        vnp_Params.put("vnp_Inv_Address", req.getParameter("txt_inv_addr1"));
//        vnp_Params.put("vnp_Inv_Company", req.getParameter("txt_inv_company"));
//        vnp_Params.put("vnp_Inv_Taxcode", req.getParameter("txt_inv_taxcode"));
//        vnp_Params.put("vnp_Inv_Type", req.getParameter("cbo_inv_type"));
            //Build data to hash and querystring
            List fieldNames = new ArrayList(vnp_Params.keySet());
            Collections.sort(fieldNames);
            StringBuilder hashData = new StringBuilder();
            StringBuilder query = new StringBuilder();
            Iterator itr = fieldNames.iterator();
            while (itr.hasNext()) {
                String fieldName = (String) itr.next();
                String fieldValue = (String) vnp_Params.get(fieldName);
                if ((fieldValue != null) && (fieldValue.length() > 0)) {
                    //Build hash data
                    hashData.append(fieldName);
                    hashData.append('=');
                    hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                    //Build query
                    query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                    query.append('=');
                    query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                    if (itr.hasNext()) {
                        query.append('&');
                        hashData.append('&');
                    }
                }
            }
            String queryUrl = query.toString();
            String vnp_SecureHash = PaymentConfig.hmacSHA512(PaymentConfig.secretKey, hashData.toString());
            queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
            String paymentUrl = PaymentConfig.vnp_PayUrl + "?" + queryUrl;
            com.google.gson.JsonObject job = new JsonObject();
            job.addProperty("code", "00");
            job.addProperty("message", "success");
            job.addProperty("data", paymentUrl);
            Gson gson = new Gson();
            resp.getWriter().write(gson.toJson(job));

        } catch (Exception e) {
            sessionServiceUtil.removeValue(SystemConstant.BOOKING_SESSIONS_VNPAY);
            throw new ApiRequestException("VNP_001", e.getMessage());
        }
    }

    @Override
    public void checkVnPay(
            BigDecimal paymentAmount,
            String paymentMethod,
            String vnp_ResponseCode
    ) {
        Map<String, Object> value = sessionServiceUtil.getValueMap(SystemConstant.BOOKING_SESSIONS_VNPAY);

        Hotel hotel = (Hotel) value.get(SystemConstant.HOTEL_SESSION_VNPAY);
        Booking booking = (Booking) value.get(SystemConstant.BOOKING_SESSION_VNPAY);

//        if (booking.getTotalPrice().compareTo(paymentAmount) < 0)
//            throw new ApiRequestException("payment amount is incorrect.", ");

        if (!vnp_ResponseCode.equals("00"))
            throw new ApiRequestException("billing fail!. notification vnp_ResponseCode = " + vnp_ResponseCode, "");

        User user = getUser();

        if (user == null)
            throw new ApiRequestException("user not login!.", "");

        BillingAddress billingAddress = billingAddressRepo.save(
                BillingAddress.builder()
                        .user(user)
                        .hotel(hotel)
                        .address(getAddressHotel(hotel))
                        .city(hotel.getCity())
                        .state(vnp_ResponseCode)
                        .postalCode("82000")
                        .country(hotel.getCountry())
                        .build()
        );

        paymentRepo.save(
                Payment.builder()
                        .booking(booking)
                        .paymentDate(LocalDateTime.now())
                        .paymentAmount(paymentAmount)
                        .paymentMethod(paymentMethod)
                        .billingAddress(billingAddress)
                        .build()
        );

        booking.getBookingDetails()
                .forEach(this::updateQuantityRoom);

        bookingRepo.updateStatusPaidByBookingId(booking.getId());
    }

    @Override
    public void createDirectPayment(PaymentDirectRequest request) {
        Hotel hotel = getHotel(request.getHid());

        Booking booking = getBooking(request.getBid());

        User user = getUser();

        BillingAddress billingAddress = billingAddressRepo.save(
                BillingAddress.builder()
                        .user(user)
                        .hotel(hotel)
                        .address(getAddressHotel(hotel))
                        .city(hotel.getCity())
                        .state("payment")
                        .postalCode("82000")
                        .country(hotel.getCountry())
                        .build()
        );

        paymentRepo.save(
                Payment.builder()
                        .booking(booking)
                        .paymentDate(LocalDateTime.now())
                        .paymentAmount(request.getAmount())
                        .paymentMethod("Direct payment")
                        .billingAddress(billingAddress)
                        .build()
        );

        booking.getBookingDetails()
                .forEach(this::updateQuantityRoom);

        bookingRepo.updateStatusPaidByBookingId(booking.getId());

    }

    @Override
    public List<PaymentRevenueDay> revenueReportByDay(Integer month, Integer year, UUID hid) {
        if (month > 12 || month < 1 || year < 0)
            throw new ApiRequestException("invalid condition year >0 and 0<month<=12", "");

        List<PaymentRevenueDay> paymentRevenueDays = paymentRepo.revenueReportByDay(month, year, hid)
                .orElseThrow(() -> new ApiRequestException("find payment by day not found!", ""));

        List<PaymentRevenueDay> paymentRevenueDaysNew = new ArrayList<>();

        int dayMonth = YearMonth.of(year, month).lengthOfMonth();
        IntStream.rangeClosed(1, dayMonth)
                .forEach(day -> paymentRevenueDays
                        .forEach(item -> {
                            if (item.day() != day)
                                paymentRevenueDaysNew.add(
                                        new PaymentRevenueDay(
                                                day,
                                                month,
                                                year,
                                                0.0
                                        )
                                );

                            if (item.day() == day)
                                paymentRevenueDaysNew.add(item);
                        }));

        return paymentRevenueDaysNew;
    }

    @Override
    public List<PaymentRevenueMonth> revenueReportByMonth(Integer year, UUID hid) {
        List<PaymentRevenueMonth> paymentRevenueMonths = paymentRepo.revenueReportByMonth(year, hid)
                .orElseThrow(() -> new ApiRequestException("find payment by year: " + year + " not found!.", ""));

        List<PaymentRevenueMonth> paymentRevenueMonthsNew = new ArrayList<>();
        IntStream.rangeClosed(1, 12)
                .forEach(month -> {
                    paymentRevenueMonths
                            .forEach(item -> {
                                if (item.month() != month)
                                    paymentRevenueMonthsNew.add(
                                            new PaymentRevenueMonth(
                                                    month,
                                                    year,
                                                    0.0
                                            )
                                    );

                                if (item.month() == month)
                                    paymentRevenueMonthsNew.add(item);
                            });
                });

        return paymentRevenueMonthsNew;
    }

    @Override
    public List<PaymentRevenueYear> yearlySummaryStatistics(UUID hid) {
        return paymentRepo.yearlySummaryStatistics(hid)
                .orElseThrow(() -> new ApiRequestException("yearly summary statistics not found", ""));
    }

    @Override
    public PaymentResponses changeStatusPaymentByBookingId(UUID bookingId, String status, int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Payment> all = paymentRepo.findPaymentsByBookingId(bookingId, pageable);

        switch (status) {
            case PaymentConstant.STATUS_PAYMENT_PENDING:
                all.forEach(item -> item.setStatus(PaymentConstant.STATUS_PAYMENT_PENDING));
                break;

            case PaymentConstant.STATUS_PAYMENT_PAID:
                all.forEach(item -> item.setStatus(PaymentConstant.STATUS_PAYMENT_PAID));
                break;

            case PaymentConstant.STATUS_PAYMENT_UN_PAID:
                all.forEach(item -> item.setStatus(PaymentConstant.STATUS_PAYMENT_UN_PAID));
                break;
            default:
                throw new ApiRequestException("PM_002", "Status payment not map!..");
        }

        List<Payment> payments = paymentRepo.saveAll(all);
        List<PaymentDTO> paymentDTOS = payments.stream()
                .map(paymentDtoMapper)
                .toList();

        return PaymentResponses.builder()
                .code("PM_002")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(paymentDTOS)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("Change status payment success!.")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private String getAddressHotel(Hotel hotel) {
        return hotel.getStreetAddress() + " " + hotel.getDistrictAddress() + " " + hotel.getCity() + " " + hotel.getCountry();
    }

    private void updateQuantityRoom(BookingDetail item) {
        Room room = item.getRoom();
        Integer quantityRoom = item.getQuantityRoomBooking() + room.getQuantityRoom();
        roomRepo.updateQuantityRoom(quantityRoom, room.getId());
    }

    private Booking getBooking(UUID bid) {
        return bookingRepo.findById(bid)
                .orElseThrow(() -> new ApiRequestException("find booking by id " + bid + " not found!.", ""));
    }

    private Hotel getHotel(UUID hid) {
        return hotelRepo.findById(hid)
                .orElseThrow(() -> new ApiRequestException("find hotel by id " + hid + " not found!.", ""));
    }
}
