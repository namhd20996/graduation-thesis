package com.example.graduationthesis.payment.service;

import com.example.graduationthesis.payment.dto.request.PaymentDirectRequest;
import com.example.graduationthesis.payment.dto.response.PaymentResponses;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueDay;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueMonth;
import com.example.graduationthesis.payment.dto.response.PaymentRevenueYear;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface PaymentService {

    void createPaymentVnPay(HttpServletRequest req, HttpServletResponse resp);

    void checkVnPay(BigDecimal paymentAmount, String paymentMethod, String vnp_ResponseCode);

    void createDirectPayment(PaymentDirectRequest request);

    List<PaymentRevenueDay> revenueReportByDay(Integer month, Integer year, UUID hid);

    List<PaymentRevenueMonth> revenueReportByMonth(Integer year, UUID hid);

    List<PaymentRevenueYear> yearlySummaryStatistics(UUID hid);

    PaymentResponses changeStatusPaymentByBookingId(UUID bookingId, String status, int currentPage, int limitPage);
}
