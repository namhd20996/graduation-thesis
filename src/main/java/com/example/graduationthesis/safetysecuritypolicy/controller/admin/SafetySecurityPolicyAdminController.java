package com.example.graduationthesis.safetysecuritypolicy.controller.admin;

import com.example.graduationthesis.constant.SafetySecurityPolicyConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.safetysecuritypolicy.dto.request.SafetySecurityPolicyRequest;
import com.example.graduationthesis.safetysecuritypolicy.dto.response.SafetySecurityPolicyResponse;
import com.example.graduationthesis.safetysecuritypolicy.dto.response.SafetySecurityPolicyResponses;
import com.example.graduationthesis.safetysecuritypolicy.service.SafetySecurityPolicyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + SafetySecurityPolicyConstant.API_SAFETY_SECURITY_POLICY)
@RequiredArgsConstructor
public class SafetySecurityPolicyAdminController {

    private final SafetySecurityPolicyService safetySecurityPolicyService;

    @PreAuthorize("hasAnyAuthority('admin:master', 'hotel:owner')")
    @GetMapping
    public ResponseEntity<SafetySecurityPolicyResponses> findAllSafetySecurityPolicy(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(safetySecurityPolicyService.findAll(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping
    public ResponseEntity<SafetySecurityPolicyResponse> addSafetySecurityPolicy(
            @Valid @RequestBody SafetySecurityPolicyRequest request
    ) {
        safetySecurityPolicyService.addSafetySecurityPolicy(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping("/{sspid}")
    public ResponseEntity<SafetySecurityPolicyResponse> updateSafetySecurityPolicy(
            @Valid @RequestBody SafetySecurityPolicyRequest request,
            @PathVariable("sspid") UUID sspid
    ) {
        safetySecurityPolicyService.updateSafetySecurityPolicy(request, sspid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping("/{sspid}")
    public ResponseEntity<SafetySecurityPolicyResponse> deleteSafetySecurityPolicy(@PathVariable("sspid") UUID sspid) {
        safetySecurityPolicyService.deleteSafetySecurityPolicy(sspid);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
