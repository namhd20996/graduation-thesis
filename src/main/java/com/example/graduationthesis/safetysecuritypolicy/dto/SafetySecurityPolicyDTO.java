package com.example.graduationthesis.safetysecuritypolicy.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class SafetySecurityPolicyDTO extends BaseDTOUtil {

    private String name;

    private String content;
}
