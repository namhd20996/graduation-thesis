package com.example.graduationthesis.safetysecuritypolicy.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SafetySecurityPolicyRequest {

    private String name;

    private String content;
}
