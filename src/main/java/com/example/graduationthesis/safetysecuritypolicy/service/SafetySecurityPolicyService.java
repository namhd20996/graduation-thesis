package com.example.graduationthesis.safetysecuritypolicy.service;

import com.example.graduationthesis.safetysecuritypolicy.dto.request.SafetySecurityPolicyRequest;
import com.example.graduationthesis.safetysecuritypolicy.dto.response.SafetySecurityPolicyResponse;
import com.example.graduationthesis.safetysecuritypolicy.dto.response.SafetySecurityPolicyResponses;

import java.util.UUID;

public interface SafetySecurityPolicyService {

    SafetySecurityPolicyResponse addSafetySecurityPolicy(SafetySecurityPolicyRequest request);

    SafetySecurityPolicyResponse updateSafetySecurityPolicy(SafetySecurityPolicyRequest request, UUID sspid);

    SafetySecurityPolicyResponse deleteSafetySecurityPolicy(UUID sspid);

    SafetySecurityPolicyResponses findAll(int currentPage, int limitPage);
}
