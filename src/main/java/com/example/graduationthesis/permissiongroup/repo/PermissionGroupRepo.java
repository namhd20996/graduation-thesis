package com.example.graduationthesis.permissiongroup.repo;

import com.example.graduationthesis.permissiongroup.entity.PermissionGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface PermissionGroupRepo extends JpaRepository<PermissionGroup, UUID> {

    @Modifying(clearAutomatically = true)
    @Query("""
            DELETE FROM PermissionGroup o WHERE o.group.id = ?1
                """)
    void deleteAllByGroupId(UUID groupId);

    @Query("""
            SELECT
                o
            FROM
                PermissionGroup o
            WHERE
                o.group.id = ?1
            """)
    List<PermissionGroup> findAllByGroupId(UUID groupId);
}
