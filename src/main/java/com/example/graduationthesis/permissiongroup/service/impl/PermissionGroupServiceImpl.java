package com.example.graduationthesis.permissiongroup.service.impl;

import com.example.graduationthesis.permissiongroup.service.PermissionGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class PermissionGroupServiceImpl implements PermissionGroupService {
}
