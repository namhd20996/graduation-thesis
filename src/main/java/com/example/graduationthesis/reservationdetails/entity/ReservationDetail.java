package com.example.graduationthesis.reservationdetails.entity;

import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_reservation_detail")
public class ReservationDetail extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column
    private LocalDate bookingDate;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "country")
    private String country;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "booking_for")
    private Boolean bookingFor;
    @Column(name = "business_travel")
    private Boolean businessTravel;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "floor_option")
    private Integer floorOption;
    @Column(name = "electronic_confirmation")
    private Boolean electronicConfirmation;
    @Column(name = "pickup_service")
    private Boolean pickupService;
    @Column(name = "car_rental")
    private Boolean carRental;
    @Column(name = "taxi_booking")
    private Boolean taxiBooking;
    @Column(name = "special_requirements")
    private String specialRequirements;
    @Column(name = "estimated_checkin_time")
    private String estimatedCheckinTime;
    @Column
    private Boolean receivePromotionalEmails;
    @Column
    private Boolean receivePromotionalEmailsTransport;

}
