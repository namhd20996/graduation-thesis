package com.example.graduationthesis.reservationdetails.repo;

import com.example.graduationthesis.reservationdetails.entity.ReservationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ReservationDetailRepo extends JpaRepository<ReservationDetail, UUID> {
}
