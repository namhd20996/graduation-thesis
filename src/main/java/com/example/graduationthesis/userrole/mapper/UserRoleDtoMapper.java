package com.example.graduationthesis.userrole.mapper;

import com.example.graduationthesis.userrole.dto.response.UserRoleDto;
import com.example.graduationthesis.userrole.entity.UserRole;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class UserRoleDtoMapper implements Function<UserRole, UserRoleDto> {
    @Override
    public UserRoleDto apply(UserRole userRole) {
        return UserRoleDto.builder()
                .userRoleId(userRole.getId())
                .roleName(userRole.getRole().getRoleName())
                .email(userRole.getUser().getEmail())
                .isDeleted(userRole.getIsDeleted())
                .status(userRole.getStatus())
                .build();
    }
}
