package com.example.graduationthesis.userrole.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserRoleUpdateRequest {

    private UUID userRoleId;

    private UUID roleId;

    private UUID userId;
}
