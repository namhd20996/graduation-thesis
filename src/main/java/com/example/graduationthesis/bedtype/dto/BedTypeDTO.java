package com.example.graduationthesis.bedtype.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class BedTypeDTO extends BaseDTOUtil {

    private String name;

    private String description;
}
