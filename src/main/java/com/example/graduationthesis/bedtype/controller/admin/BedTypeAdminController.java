package com.example.graduationthesis.bedtype.controller.admin;

import com.example.graduationthesis.bedtype.dto.request.BedTypeAddRequest;
import com.example.graduationthesis.bedtype.dto.response.BedTypeResponses;
import com.example.graduationthesis.bedtype.service.BedTypeService;
import com.example.graduationthesis.constant.BedTypeConstant;
import com.example.graduationthesis.constant.SystemConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + BedTypeConstant.API_BED_TYPE)
@RequiredArgsConstructor
public class BedTypeAdminController {

    private final BedTypeService bedTypeService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping
    public ResponseEntity<?> addBedType(
            @Validated @RequestBody BedTypeAddRequest request
    ) {
        return new ResponseEntity<>(bedTypeService.addBedType(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping
    public ResponseEntity<?> updateBedType(
            @Validated @RequestBody BedTypeAddRequest request,
            @RequestParam("btid") UUID btid
    ) {
        return new ResponseEntity<>(bedTypeService.updateBedType(request, btid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping
    public ResponseEntity<?> deleteBedType(@RequestParam("btid") UUID btid) {
        return new ResponseEntity<>(bedTypeService.deleteBedType(btid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @GetMapping
    public ResponseEntity<BedTypeResponses> selectAllBedType(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage,
            @RequestParam(SystemConstant.PARAM_STATUS) Optional<String> status
    ) {
        return new ResponseEntity<>(bedTypeService.findAll(
                currentPage.orElse(1),
                limitPage.orElse(8),
                status.orElse("ACTIVE")),
                HttpStatus.OK
        );
    }
}
