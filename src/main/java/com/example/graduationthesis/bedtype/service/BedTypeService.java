package com.example.graduationthesis.bedtype.service;

import com.example.graduationthesis.bedtype.dto.request.BedTypeAddRequest;
import com.example.graduationthesis.bedtype.dto.response.BedTypeResponse;
import com.example.graduationthesis.bedtype.dto.response.BedTypeResponses;

import java.util.UUID;

public interface BedTypeService {

    BedTypeResponse addBedType(BedTypeAddRequest request);

    BedTypeResponse updateBedType(BedTypeAddRequest request, UUID btid);

    BedTypeResponse deleteBedType(UUID btid);

    BedTypeResponses findAll(int currentPage, int limitPage, String status);

}
