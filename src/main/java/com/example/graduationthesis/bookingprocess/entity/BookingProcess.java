package com.example.graduationthesis.bookingprocess.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_booking_process")
@Entity
public class BookingProcess {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    private UUID hotelId;

    private String status;

    private String jwtToken;

    private LocalDate checkInDate;

    private LocalDate checkOutDate;

    private Integer quantityRoom;

    private Integer quantityAdult;

    private Integer quantityChildren;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private Boolean bookingForMe;

    private Boolean businessTravel;

    private String fullName;

    private String email;

    private String location;

    private String country;

    private Boolean electronicConfirm;

    private Boolean orderCar;

    private Boolean orderTaxi;

    private Boolean pickUpService;

    private String specialRequirements;

    private String estimatedCheckInTime;

    private Boolean receiveMarketingEmail;

}
