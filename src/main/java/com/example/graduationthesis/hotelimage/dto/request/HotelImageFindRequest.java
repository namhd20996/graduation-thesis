package com.example.graduationthesis.hotelimage.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelImageFindRequest {

    private UUID hotelId;

    private Integer page;

    private Integer limit;
}
