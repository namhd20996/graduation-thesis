package com.example.graduationthesis.hotelimage.dto;

import com.example.graduationthesis.utils.BaseDTOIDUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class HotelImageDTO extends BaseDTOIDUtil {

    private String imageName;
    private String type;
    private Date uploadDate;
    private byte[] picByte;
}
