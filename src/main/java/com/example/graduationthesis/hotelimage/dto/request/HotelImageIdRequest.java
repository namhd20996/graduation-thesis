package com.example.graduationthesis.hotelimage.dto.request;

import java.util.UUID;

public record HotelImageIdRequest(
        UUID hotelId
) {
}
