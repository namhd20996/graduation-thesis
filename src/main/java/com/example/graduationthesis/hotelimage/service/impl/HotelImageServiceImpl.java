package com.example.graduationthesis.hotelimage.service.impl;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.hotelimage.dto.request.HotelImageRequest;
import com.example.graduationthesis.hotelimage.dto.response.HotelImageResponse;
import com.example.graduationthesis.hotelimage.dto.response.HotelImageResponses;
import com.example.graduationthesis.hotelimage.entity.HotelImage;
import com.example.graduationthesis.hotelimage.mapper.HotelImageDTOMapper;
import com.example.graduationthesis.hotelimage.repo.HotelImageRepo;
import com.example.graduationthesis.hotelimage.service.HotelImageService;
import com.example.graduationthesis.image.ImageService;
import com.example.graduationthesis.session.common.SessionResponseUtil;
import com.example.graduationthesis.session.hotelimage.HotelImageSession;
import com.example.graduationthesis.session.hotelimage.HotelImageSessionRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.BaseUrlServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class HotelImageServiceImpl implements HotelImageService {

    private final HotelImageRepo hotelImageRepo;

    private final HotelImageDTOMapper hotelImageDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final ImageService imageService;

    private final BaseUrlServiceUtil baseUrlServiceUtil;

    private final HotelImageSessionRepo hotelImageSessionRepo;

    @Override
    public HotelImageResponse addHotelImage(String jwtToken, MultipartFile[] multipartFiles) {
        baseAmenityUtil.checkJwtToken(jwtToken);
        hotelImageSessionRepo.findHotelImageSessionByJwtToken(jwtToken).ifPresent(hotelImageSessionRepo::deleteAll);
        List<String> urlImages = new ArrayList<>();
        for (MultipartFile file : multipartFiles) {
            String filename = imageService.saveImage(file);
            urlImages.add(baseUrlServiceUtil.getBaseUrl() + SystemConstant.API_PUBLIC + SystemConstant.API_VERSION_ONE + "/image/" + filename);
        }

        List<HotelImageSession> hotelImageSessions = urlImages.stream()
                .map(item -> new HotelImageSession(item, jwtToken))
                .toList();
        List<HotelImageSession> imageSessions = hotelImageSessionRepo.saveAll(hotelImageSessions);
        return HotelImageResponse.builder()
                .code("HTIM_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(new SessionResponseUtil("HOTEL_IMAGE", imageSessions))
                .message("Add hotel images session success!.")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public void addHotelImageMore(UUID hid, MultipartFile[] multipartFiles) {
    }

    @Override
    public void updateHotelImage(HotelImageRequest request) {
    }

    @Override
    public void deleteHotelImage(UUID hiid) {
        HotelImage hotelImage = hotelImageRepo.findById(hiid)
                .orElseThrow(() -> new ApiRequestException("find image by id " + hiid + " not found!.", ""));
        hotelImageRepo.delete(hotelImage);
    }

    @Override
    public HotelImageResponses findHotelImagesByHotelId(UUID hotelId, Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);

        List<HotelImage> hotelImages = hotelImageRepo.findHotelImagesByHotelId(hotelId, pageable)
                .orElseThrow(() ->
                        new ApiRequestException("find images by hotel id" + hotelId + " not found!.", ""));

        return HotelImageResponses.builder()
                .data(
                        hotelImages.stream()
                                .map(hotelImageDTOMapper)
                                .toList()
                )
                .build();
    }
}