package com.example.graduationthesis.hotelimage.controller.admin;


import com.example.graduationthesis.constant.HotelImageConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotelimage.dto.response.HotelImageResponses;
import com.example.graduationthesis.hotelimage.service.HotelImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + HotelImageConstant.API_HOTEL_IMAGE)
@RequiredArgsConstructor
public class HotelImageAdminController {

    public final HotelImageService hotelImageService;

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> addHotelImage(
            @RequestHeader("jwtToken") String jwtToken,
            @RequestParam("images") MultipartFile[] multipartFiles
    ) {
        return new ResponseEntity<>(hotelImageService.addHotelImage(jwtToken, multipartFiles), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @PostMapping(value = HotelImageConstant.API_ADD_MORE + "/{hid}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> addHotelImageMore(
            @PathVariable("hid") UUID hid,
            @RequestPart("images") MultipartFile[] multipartFiles
    ) {
        hotelImageService.addHotelImageMore(hid, multipartFiles);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @DeleteMapping("/{hiid}")
    public ResponseEntity<?> deleteHotelImage(@PathVariable("hiid") UUID hiid) {
        hotelImageService.deleteHotelImage(hiid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster,@hotelOwner)")
    @GetMapping("/{hid}")
    public ResponseEntity<HotelImageResponses> findHotelImagesByHotelId(
            @PathVariable("hid") UUID hid,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(hotelImageService.findHotelImagesByHotelId(hid, currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }
}
