package com.example.graduationthesis.hotelpolicy.repo;

import com.example.graduationthesis.hotelpolicy.entity.HotelPolicy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface HotelPolicyRepo extends JpaRepository<HotelPolicy, UUID> {

    Optional<HotelPolicy> findByBookingPolicyAndCancellationPolicyAndPetPolicyAndSmokingPolicyAndAdditionalPolices(
            String bookingPolicy,
            String cancellationPolicy,
            String petPolicy,
            String smokingPolicy,
            String additionalPolices
    );
}
