package com.example.graduationthesis.hotelpolicy.service;

import com.example.graduationthesis.hotelpolicy.dto.request.HotelPolicyAddRequest;
import com.example.graduationthesis.hotelpolicy.dto.request.HotelPolicyUpdateRequest;
import com.example.graduationthesis.hotelpolicy.dto.response.HotelPolicyResponse;
import com.example.graduationthesis.hotelpolicy.dto.response.HotelPolicyResponses;

import java.util.UUID;

public interface HotelPolicyService {

    HotelPolicyResponse addHotelPolicy(String jwtToken, HotelPolicyAddRequest request);

    HotelPolicyResponse addHotelPolicyNew(HotelPolicyAddRequest request);

    HotelPolicyResponse updateHotelPolicy(HotelPolicyUpdateRequest request, UUID hpid);

    HotelPolicyResponse deleteHotelPolicy(UUID hpid);

    HotelPolicyResponses findAll(int currentPage, int limitPage);
}
