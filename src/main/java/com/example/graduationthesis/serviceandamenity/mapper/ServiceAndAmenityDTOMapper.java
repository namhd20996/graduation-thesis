package com.example.graduationthesis.serviceandamenity.mapper;

import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityDTOResp;
import com.example.graduationthesis.serviceandamenity.entity.ServiceAndAmenity;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class ServiceAndAmenityDTOMapper
        implements Function<ServiceAndAmenity, ServiceAndAmenityDTOResp> {
    @Override
    public ServiceAndAmenityDTOResp apply(ServiceAndAmenity serviceAndAmenity) {
        return ServiceAndAmenityDTOResp.builder()
                .id(serviceAndAmenity.getId())
                .createdBy(serviceAndAmenity.getCreatedBy())
                .createdDate(serviceAndAmenity.getCreatedDate())
                .lastModifiedBy(serviceAndAmenity.getLastModifiedBy())
                .lastModifiedDate(serviceAndAmenity.getLastModifiedDate())
                .name(serviceAndAmenity.getName())
                .type(serviceAndAmenity.getType())
                .status(serviceAndAmenity.getStatus())
                .isDeleted(serviceAndAmenity.getIsDeleted())
                .description(serviceAndAmenity.getDescription())
                .build();
    }
}
