package com.example.graduationthesis.serviceandamenity.dto.response;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class ServiceAndAmenityDTOResp extends BaseDTOUtil {

    private String name;

    private String type;

    private String status;

    private String description;
}
