package com.example.graduationthesis.serviceandamenity.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.serviceandamenity.dto.request.ServiceAndAmenityRequest;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityDTOResp;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityResponse;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityResponses;
import com.example.graduationthesis.serviceandamenity.entity.ServiceAndAmenity;
import com.example.graduationthesis.serviceandamenity.mapper.ServiceAndAmenityDTOMapper;
import com.example.graduationthesis.serviceandamenity.repo.ServiceAndAmenityRepo;
import com.example.graduationthesis.serviceandamenity.service.ServiceAndAmenityService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class ServiceAndAmenityServiceImpl implements ServiceAndAmenityService {

    private final ServiceAndAmenityRepo serviceAndAmenityRepo;

    private final ServiceAndAmenityDTOMapper serviceAndAmenityDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public ServiceAndAmenityResponse addServiceAndAmenity(ServiceAndAmenityRequest request) {
        if (serviceAndAmenityRepo.existsByName(request.getName()))
            throw new ApiRequestException("", "add service and amenity fail " + request.getName() + " already exist");

        ServiceAndAmenity serviceAndAmenity = serviceAndAmenityRepo.save(
                ServiceAndAmenity.builder()
                        .name(request.getName().trim())
                        .type(request.getType().trim())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .description(request.getDescription())
                        .build()
        );

        return ServiceAndAmenityResponse.builder()
                .code(ResourceConstant.SAM_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(serviceAndAmenityDTOMapper.apply(serviceAndAmenity))
                .message(getMessageBundle(ResourceConstant.SAM_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ServiceAndAmenityResponse updateServiceAndAmenity(ServiceAndAmenityRequest request, UUID said) {
        ServiceAndAmenity serviceAndAmenity = getServiceAndAmenity(said);

        if (!serviceAndAmenity.getName().equals(request.getName()))
            serviceAndAmenity.setName(request.getName());

        if (!serviceAndAmenity.getType().equals(request.getType()))
            serviceAndAmenity.setType(request.getType());

        if (serviceAndAmenity.getDescription() == null || !serviceAndAmenity.getDescription().equals(request.getDescription()))
            serviceAndAmenity.setDescription(request.getDescription());

        serviceAndAmenityRepo.save(serviceAndAmenity);

        return ServiceAndAmenityResponse.builder()
                .code(ResourceConstant.SAM_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(serviceAndAmenityDTOMapper.apply(serviceAndAmenity))
                .message(getMessageBundle(ResourceConstant.SAM_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ServiceAndAmenityResponse deleteServiceAndAmenity(UUID said) {
        ServiceAndAmenity serviceAndAmenity = getServiceAndAmenity(said);
        serviceAndAmenity.setIsDeleted(SystemConstant.NO_ACTIVE);
        serviceAndAmenity.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        serviceAndAmenityRepo.save(serviceAndAmenity);

        return ServiceAndAmenityResponse.builder()
                .code(ResourceConstant.SAM_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(serviceAndAmenityDTOMapper.apply(serviceAndAmenity))
                .message(getMessageBundle(ResourceConstant.SAM_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ServiceAndAmenityResponse findById(UUID said) {
        ServiceAndAmenity serviceAndAmenity = getServiceAndAmenity(said);

        return ServiceAndAmenityResponse.builder()
                .code(ResourceConstant.SAM_002)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(serviceAndAmenityDTOMapper.apply(serviceAndAmenity))
                .message(getMessageBundle(ResourceConstant.SAM_002))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public ServiceAndAmenityResponses findAll(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<ServiceAndAmenity> all = serviceAndAmenityRepo.findAll(pageable);

        List<ServiceAndAmenityDTOResp> resps = all.stream()
                .map(serviceAndAmenityDTOMapper)
                .toList();

        return ServiceAndAmenityResponses.builder()
                .code("SAAR_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(resps)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("Get service and amenity success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private ServiceAndAmenity getServiceAndAmenity(UUID said) {
        return serviceAndAmenityRepo.findById(said)
                .orElseThrow(() -> new ApiRequestException("", "find service and amenity by id " + said + " not found!."));
    }
}
