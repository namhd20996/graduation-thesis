package com.example.graduationthesis.serviceandamenity.entity;

import com.example.graduationthesis.serviceandamenityroom.entity.ServiceAndAmenityRoom;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;


@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "_service_and_amenity")
public class ServiceAndAmenity extends BaseEntityUtil {

    private String name;

    private String type;

    private String description;

    @OneToMany(mappedBy = "serviceAndAmenity")
    private List<ServiceAndAmenityRoom> serviceAndAmenityRooms;
}
