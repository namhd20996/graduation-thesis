package com.example.graduationthesis.serviceandamenity.controller.admin;

import com.example.graduationthesis.constant.ServiceAndAmenityConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.serviceandamenity.dto.request.ServiceAndAmenityRequest;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityResponse;
import com.example.graduationthesis.serviceandamenity.dto.response.ServiceAndAmenityResponses;
import com.example.graduationthesis.serviceandamenity.service.ServiceAndAmenityService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + ServiceAndAmenityConstant.API_SERVICE_AND_AMENITY)
@RequiredArgsConstructor
public class ServiceAndAmenityAdminController {

    private final ServiceAndAmenityService serviceAndAmenityService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping
    public ResponseEntity<ServiceAndAmenityResponse> addServiceAndAmenity(
            @Valid @RequestBody ServiceAndAmenityRequest request
    ) {
        return new ResponseEntity<>(serviceAndAmenityService.addServiceAndAmenity(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping
    public ResponseEntity<ServiceAndAmenityResponse> updateServiceAndAmenity(
            @Valid @RequestBody ServiceAndAmenityRequest request,
            @RequestParam("said") UUID said
    ) {
        return new ResponseEntity<>(serviceAndAmenityService.updateServiceAndAmenity(request, said), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping
    public ResponseEntity<ServiceAndAmenityResponse> deleteServiceAndAmenity(@RequestParam("said") UUID said) {
        return new ResponseEntity<>(serviceAndAmenityService.deleteServiceAndAmenity(said), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<ServiceAndAmenityResponses> selectAllServiceAndAmenity(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(serviceAndAmenityService.findAll(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }
}
