package com.example.graduationthesis.session.common;

public record SessionResponseUtil(
        String status,

        Object data
) {


}
