package com.example.graduationthesis.session.hotel;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface HotelSessionRepo extends JpaRepository<HotelSession, UUID> {

    Optional<HotelSession> findHotelSessionByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
