package com.example.graduationthesis.session.room;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_room_session")
@Entity
public class RoomSession extends AbsBaseIdSession {

    private UUID roomTypeId;

    private BigDecimal pricePerNight;

    private String roomName;

    private String roomNameCustom;

    private Integer quantityRoom;

    private Double roomArea;

    private Integer maxOccupancy;

    private String bedName;

}
