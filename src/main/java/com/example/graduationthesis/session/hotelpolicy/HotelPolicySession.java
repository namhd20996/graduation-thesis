package com.example.graduationthesis.session.hotelpolicy;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_hotel_policy_session")
@Entity
public class HotelPolicySession extends AbsBaseIdSession {

    private String bookingPolicy;

    private String cancellationPolicy;

    private String petPolicy;

    private String smokingPolicy;

    private String additionalPolices;
}
