package com.example.graduationthesis.session.serviceamenityroom;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
public class ServiceAmenityRoomSession extends AbsBaseIdSession {

    private Integer extraBed;

    private Boolean childrenSleepInCribs;

    private Boolean typeOfGuestChildren;

    private String childrenOld;

    private BigDecimal priceGuestChildren;

    private Boolean typeOfGuestAdults;

    private BigDecimal priceGuestAdults;

}
