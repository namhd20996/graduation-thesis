package com.example.graduationthesis.session.hotelimage;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_hotel_image_session")
@Entity
public class HotelImageSession extends AbsBaseIdSession {

    private String urlImage;

    public HotelImageSession(String urlImage, String jwtToken) {
        this.urlImage = urlImage;
        super.setJwtToken(jwtToken);
    }
}
