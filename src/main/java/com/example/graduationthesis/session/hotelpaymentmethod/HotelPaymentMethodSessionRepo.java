package com.example.graduationthesis.session.hotelpaymentmethod;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface HotelPaymentMethodSessionRepo extends JpaRepository<HotelPaymentMethodSession, UUID> {

    Optional<HotelPaymentMethodSession> findHotelPaymentMethodSessionByJwtToken(String jwtToken);

    void deleteByJwtToken(String jwtToken);
}
