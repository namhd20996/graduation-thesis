package com.example.graduationthesis.session.serviceamenityroomid;

import com.example.graduationthesis.session.AbsBaseIdSession;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "_service_amenity_room_id")
@Entity
public class ServiceAmenityRoomId extends AbsBaseIdSession {

    private UUID serviceAmenityRoomId;
}
