package com.example.graduationthesis.booking.controller.member;

import com.example.graduationthesis.booking.service.BookingService;
import com.example.graduationthesis.constant.BookingConstant;
import com.example.graduationthesis.constant.SystemConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + BookingConstant.API_BOOKING)
public class BookingMemberController {

    private final BookingService bookingService;

    @GetMapping
    public ResponseEntity<?> getBookingsByUser() {
        return new ResponseEntity<>(bookingService.getBookingsByUser(), HttpStatus.OK);
    }

    @PutMapping("/{bookingId}")
    public ResponseEntity<?> cancelBookingByUser(@PathVariable("bookingId") UUID bookingId) {
        return new ResponseEntity<>(bookingService.cancelBookingByUser(bookingId), HttpStatus.OK);
    }
}
