package com.example.graduationthesis.booking.dto;

import com.example.graduationthesis.hotel.dto.response.HotelBookingResponse;
import com.example.graduationthesis.payment.dto.PaymentDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

public record BookingUserDTO(
        UUID bookingId,
        String fullName,
        String phoneNumber,
        String email,
        String bookingCode,
        String pinCode,
        String address,
        String status,
        LocalDate bookingDate,
        BigDecimal totalPrice,
        PaymentDTO payment,
        HotelBookingResponse hotel
) {
}
