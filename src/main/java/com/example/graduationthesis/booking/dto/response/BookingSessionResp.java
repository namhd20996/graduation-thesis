package com.example.graduationthesis.booking.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class BookingSessionResp extends AbsBookingSessionResp {
}
