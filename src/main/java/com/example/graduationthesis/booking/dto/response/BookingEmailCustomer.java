package com.example.graduationthesis.booking.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class BookingEmailCustomer {

    private String nameCustomer;

    private String email;

    private String phoneNumber;

    private BigDecimal totalPrice;

    private List<BookingEmailDetail> bookingEmailDetails;
}
