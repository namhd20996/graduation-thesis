package com.example.graduationthesis.booking.dto.response;

import java.math.BigDecimal;
import java.time.LocalDate;

public record BookingEmailDetail(
        String nameRoom,

        LocalDate checkInDate,

        LocalDate checkOutDate,

        BigDecimal priceRoom,
        String bookingCode,
        String pinCode
) {


}
