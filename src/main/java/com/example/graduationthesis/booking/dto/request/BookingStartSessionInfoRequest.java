package com.example.graduationthesis.booking.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BookingStartSessionInfoRequest {

    private String firstName;

    private String lastName;

    private String email;

    private String country;

    private String phoneNumber;

    private Boolean bookingForMe;

    private Boolean businessTravel;

    private Boolean electronicConfirm;

    private Boolean orderCar;

    private Boolean orderTaxi;

    private Boolean pickUpService;

    private String specialRequirements;

    private String estimatedCheckInTime;

}
