package com.example.graduationthesis.role.entity;


import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import com.example.graduationthesis.userrole.entity.UserRole;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_role")
public class Role extends BaseEntityUtil {
    @Column
    private String roleName;
    @Column
    private String roleCode;
    @OneToMany(mappedBy = "role", fetch = FetchType.EAGER)
    private List<PermissionRole> permissionRoles;
    @OneToMany(mappedBy = "role", fetch = FetchType.EAGER)
    private List<UserRole> userRoles;
}
