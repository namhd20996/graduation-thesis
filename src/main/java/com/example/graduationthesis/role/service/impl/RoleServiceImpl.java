package com.example.graduationthesis.role.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.permission.dto.request.PermissionIdRequest;
import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.permission.repo.PermissionRepo;
import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import com.example.graduationthesis.permissionrole.repo.PermissionRoleRepo;
import com.example.graduationthesis.role.dto.request.RoleAddRequest;
import com.example.graduationthesis.role.dto.request.RoleUpdateRequest;
import com.example.graduationthesis.role.dto.response.RoleDTO;
import com.example.graduationthesis.role.dto.response.RoleResponse;
import com.example.graduationthesis.role.dto.response.RoleResponses;
import com.example.graduationthesis.role.entity.Role;
import com.example.graduationthesis.role.mapper.RoleDTOMapper;
import com.example.graduationthesis.role.repo.RoleRepo;
import com.example.graduationthesis.role.service.RoleService;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.repo.UserRepo;
import com.example.graduationthesis.userrole.entity.UserRole;
import com.example.graduationthesis.userrole.repo.UserRoleRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepo roleRepo;

    private final RoleDTOMapper roleDtoMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final PermissionRepo permissionRepo;

    private final UserRepo userRepo;

    private final UserRoleRepo userRoleRepo;

    private final PermissionRoleRepo permissionRoleRepo;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    private Role getRoleById(UUID roleId) {
        return roleRepo.findById(roleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.RL_002, getMessageBundle(ResourceConstant.RL_002)));
    }

    private Permission getPermission(UUID permissionId) {
        return permissionRepo.findPermissionByIdAndIsDeleted(permissionId, SystemConstant.ACTIVE)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.PMS_002, getMessageBundle(ResourceConstant.PMS_002)));
    }

    private List<Permission> getPermissions(List<PermissionIdRequest> permissionIds) {
        return permissionIds.stream()
                .map(item -> getPermission(item.permissionId()))
                .toList();
    }

    private void saveAllPermissionRole(List<Permission> permissions, Role role) {
        List<PermissionRole> permissionRoles = permissions.stream()
                .map(permission -> new PermissionRole(
                        permission,
                        role,
                        SystemConstant.ACTIVE,
                        SystemConstant.STATUS_ACTIVE
                ))
                .toList();
        permissionRoleRepo.saveAll(permissionRoles);
    }

    @Override
    public RoleResponse addRole(RoleAddRequest request) {
        if (roleRepo.existsByRoleName(request.getRoleName())) {
            throw new ApiRequestException(ResourceConstant.RL_001, getMessageBundle(ResourceConstant.RL_001));
        }
        List<Permission> permissions = getPermissions(request.getPermissionIdsRequest());

        Role role = roleRepo.save(
                Role.builder()
                        .roleName(request.getRoleName())
                        .roleCode(request.getRoleCode())
                        .isDeleted(SystemConstant.ACTIVE)
                        .build()
        );
        saveAllPermissionRole(permissions, role);

        return RoleResponse.builder()
                .code(ResourceConstant.RL_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roleDtoMapper.apply(role))
                .message(getMessageBundle(ResourceConstant.RL_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }


    @Override
    public RoleResponse updateRole(UUID userId, RoleUpdateRequest request) {
        User user = userRepo.findById(userId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.USR_001, getMessageBundle(ResourceConstant.USR_001)));
        userRoleRepo.deleteAllByUserId(user.getId());

        List<Permission> permissions = request.getPermissionIdsRequest().stream()
                .map(item -> getPermission(item.permissionId()))
                .toList();

        if (roleRepo.existsByRoleName(request.getRoleName())) {
            throw new ApiRequestException(ResourceConstant.RL_004, getMessageBundle(ResourceConstant.RL_004));
        }

        Role role = roleRepo.save(
                Role.builder()
                        .roleName(request.getRoleName())
                        .roleCode(request.getRoleCode())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        List<PermissionRole> permissionRoles = permissions.stream()
                .map(item -> new PermissionRole(item, role)).toList();
        permissionRoleRepo.saveAll(permissionRoles);

        userRoleRepo.save(
                UserRole.builder()
                        .user(user)
                        .role(role)
                        .build()
        );

        return RoleResponse.builder()
                .code(ResourceConstant.RL_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roleDtoMapper.apply(role))
                .message(getMessageBundle(ResourceConstant.RL_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoleResponse deleteRole(UUID roleId) {
        Role role = getRoleById(roleId);
        role.setIsDeleted(SystemConstant.NO_ACTIVE);

        Role roleSave = roleRepo.save(role);
        return RoleResponse.builder()
                .code(ResourceConstant.RL_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roleDtoMapper.apply(roleSave))
                .message(getMessageBundle(ResourceConstant.RL_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoleResponse findById(UUID roleId) {
        Role role = roleRepo.findById(roleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.RL_002, getMessageBundle(ResourceConstant.RL_002)));

        return RoleResponse.builder()
                .code(ResourceConstant.RL_001)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roleDtoMapper.applyRoleDetail(role))
                .message(getMessageBundle(ResourceConstant.RL_001))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoleResponses findAllRole(Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Role> all = roleRepo.findAllRoleNotRoleNameAdminMaster(pageable);
        List<RoleDTO> roleDtos = all.stream()
                .map(roleDtoMapper)
                .toList();

        return RoleResponses.builder()
                .code(ResourceConstant.RL_011)
                .status(SystemConstant.STATUS_SUCCESS)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .data(roleDtos)
                .message(getMessageBundle(ResourceConstant.RL_011))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }
}





