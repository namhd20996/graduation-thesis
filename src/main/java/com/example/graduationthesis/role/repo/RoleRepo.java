package com.example.graduationthesis.role.repo;

import com.example.graduationthesis.role.entity.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RoleRepo extends JpaRepository<Role, UUID> {

    Optional<Role> findRoleByIdAndStatus(UUID roleId, String status);

    boolean existsByRoleName(String roleName);

    Optional<Role> findRoleByRoleCode(String roleCode);

    @Query("""
            SELECT
                o
            FROM
                Role o
            WHERE NOT
                o.roleName = 'ADMIN_MASTER'
            """)
    Page<Role> findAllRoleNotRoleNameAdminMaster(Pageable pageable);

    @Query("""
            SELECT
                o
            FROM
                Role o
            INNER JOIN
                UserRole  ur ON ur.role.id = o.id
            INNER JOIN
                User u ON u.id = ur.user.id
            WHERE u.id = ?1
            AND o.id NOT IN (
                    SELECT
                        r.id
                    FROM
                        Role r
                    INNER JOIN
                        UserRole  ur ON ur.role.id = r.id
                    INNER JOIN
                        User u ON u.id = ur.user.id
                    WHERE
                        r.roleName = 'ADMIN_MASTER' OR r.roleName = 'MANAGER' OR r.roleName = 'GUEST'
                )
            """)
    List<Role> findAllRoleNotRoleNameAdminMasterAndGuestAndManagerByUserId(UUID userId);

}
