package com.example.graduationthesis.role.controller.admin;

import com.example.graduationthesis.constant.RoleConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.constant.UserConstant;
import com.example.graduationthesis.role.dto.request.RoleAddRequest;
import com.example.graduationthesis.role.dto.request.RoleUpdateRequest;
import com.example.graduationthesis.role.dto.response.RoleResponse;
import com.example.graduationthesis.role.dto.response.RoleResponses;
import com.example.graduationthesis.role.service.RoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + RoleConstant.API_ROLE)
@RequiredArgsConstructor
public class RoleAdminController {

    private final RoleService roleService;

    @PostMapping
    public ResponseEntity<RoleResponse> addRole(@Valid @RequestBody RoleAddRequest request) {
        return new ResponseEntity<>(roleService.addRole(request), HttpStatus.OK);
    }

    @GetMapping(RoleConstant.API_ROLE_ID)
    public ResponseEntity<RoleResponse> findRoleById(@PathVariable(RoleConstant.PATH_ROLE_ID) UUID id) {
        return new ResponseEntity<>(roleService.findById(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<RoleResponses> findAllRole(
            @RequestParam(value = SystemConstant.CURRENT_PAGE, required = false) Optional<Integer> currentPage,
            @RequestParam(value = SystemConstant.LIMIT_PAGE, required = false) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(roleService.findAllRole(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }

    @PutMapping(UserConstant.API_USER_ID)
    public ResponseEntity<RoleResponse> updateRole(
            @PathVariable(UserConstant.PATH_USER_ID) UUID userId,
            @Valid @RequestBody RoleUpdateRequest request
    ) {
        return new ResponseEntity<>(roleService.updateRole(userId, request), HttpStatus.OK);
    }

    @DeleteMapping(RoleConstant.API_ROLE_ID)
    public ResponseEntity<RoleResponse> deleteRole(@PathVariable(RoleConstant.PATH_ROLE_ID) UUID id) {
        return new ResponseEntity<>(roleService.deleteRole(id), HttpStatus.OK);
    }
}
