package com.example.graduationthesis.group.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.group.dto.request.GroupAddPermissionRequest;
import com.example.graduationthesis.group.dto.request.GroupAddRequest;
import com.example.graduationthesis.group.dto.request.GroupAddUserForGroup;
import com.example.graduationthesis.group.dto.request.GroupRequest;
import com.example.graduationthesis.group.dto.response.GroupDto;
import com.example.graduationthesis.group.dto.response.GroupResponse;
import com.example.graduationthesis.group.dto.response.GroupResponses;
import com.example.graduationthesis.group.entity.Group;
import com.example.graduationthesis.group.mapper.GroupDtoMapper;
import com.example.graduationthesis.group.repo.GroupRepo;
import com.example.graduationthesis.group.service.GroupService;
import com.example.graduationthesis.permission.entity.Permission;
import com.example.graduationthesis.permission.repo.PermissionRepo;
import com.example.graduationthesis.permissiongroup.entity.PermissionGroup;
import com.example.graduationthesis.permissiongroup.repo.PermissionGroupRepo;
import com.example.graduationthesis.permissionrole.entity.PermissionRole;
import com.example.graduationthesis.permissionrole.repo.PermissionRoleRepo;
import com.example.graduationthesis.role.entity.Role;
import com.example.graduationthesis.role.repo.RoleRepo;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.user.repo.UserRepo;
import com.example.graduationthesis.userrole.entity.UserRole;
import com.example.graduationthesis.userrole.repo.UserRoleRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import com.example.graduationthesis.utils.PageableResponseUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepo groupRepo;

    private final RedisTemplate<String, String> redisTemplate;

    private final ObjectMapper objectMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private final RoleRepo roleRepo;

    private final UserRepo userRepo;

    private final UserRoleRepo userRoleRepo;

    private final PermissionRepo permissionRepo;

    private final PermissionRoleRepo permissionRoleRepo;

    private final PermissionGroupRepo permissionGroupRepo;

    private final GroupDtoMapper groupDtoMapper;

    private final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    private User getUser(UUID userId) {
        return userRepo.findUsersByIdAndIsDeleted(userId, SystemConstant.ACTIVE)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.USR_002, getMessageBundle(ResourceConstant.USR_002)));
    }

    private Permission getPermission(UUID permissionId) {
        return permissionRepo.findById(permissionId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.PMS_001, getMessageBundle(ResourceConstant.PMS_001)));
    }

    private Role getRole(UUID roleId) {
        return roleRepo.findById(roleId)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.RL_001, getMessageBundle(ResourceConstant.RL_001)));
    }

    @Override
    public GroupResponse addUserForGroup(GroupAddUserForGroup request) {
        User user = getUser(request.getUserId());
        List<Role> roles = roleRepo.findAllRoleNotRoleNameAdminMasterAndGuestAndManagerByUserId(user.getId());

        List<PermissionRole> permissionRolesFind = permissionRoleRepo.findPermissionRolesByUserId(user.getId());
        if (!permissionRolesFind.isEmpty()) {
            permissionRoleRepo.deleteAll(permissionRolesFind);
        }

        if (!user.getUserRoles().isEmpty()) {
            userRoleRepo.deleteAll(user.getUserRoles());
        }

        if (!roles.isEmpty()) {
            roleRepo.deleteAll(roles);
        }

        Role role = roleRepo.save(
                Role.builder()
                        .roleName(SystemConstant.ROLE_NAME_CUSTOM + System.currentTimeMillis())
                        .roleCode(SystemConstant.ROLE_CODE_CUSTOM + System.currentTimeMillis())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        userRoleRepo.save(
                UserRole.builder()
                        .user(user)
                        .role(role)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .isDeleted(SystemConstant.ACTIVE)
                        .build()
        );

        List<PermissionRole> permissionRoles = request.getGroupIds().stream()
                .map(item -> getGroup(item.groupId()))
                .map(group -> permissionGroupRepo.findAllByGroupId(group.getId()))
                .flatMap(Collection::stream)
                .map(PermissionGroup::getPermission)
                .map(permission -> new PermissionRole(permission, role, SystemConstant.ACTIVE, SystemConstant.STATUS_ACTIVE))
                .toList();
        permissionRoleRepo.saveAll(permissionRoles);

        return GroupResponse.builder()
                .code(ResourceConstant.ADD_001)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(getMessageBundle(ResourceConstant.ADD_001))
                .message(getMessageBundle(ResourceConstant.ADD_001))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GroupResponse updatePermissionsForGroup(GroupAddPermissionRequest request) {
        Group group = getGroup(request.getGroupId());
        List<PermissionRole> permissionRolesFind = permissionRoleRepo.findPermissionRolesByGroupId(group.getId());

        Set<Role> roles = permissionRolesFind.stream()
                .map(PermissionRole::getRole)
                .collect(Collectors.toSet());

        List<Permission> permissions = request.getPermissionIds().stream()
                .map(item -> getPermission(item.permissionId()))
                .toList();

        List<PermissionRole> permissionRoles = new ArrayList<>();
        roles.forEach(role -> {
            permissionRoles.addAll(permissions.stream()
                    .map(permission -> new PermissionRole(permission, role, SystemConstant.ACTIVE, SystemConstant.STATUS_ACTIVE))
                    .toList());
        });

        permissionRoleRepo.deleteAllInBatch(permissionRolesFind);
        permissionRoleRepo.saveAllAndFlush(permissionRoles);

        permissionGroupRepo.deleteAllByGroupId(group.getId());
        List<PermissionGroup> permissionGroups = permissions.stream()
                .map(permission -> new PermissionGroup(group, permission))
                .toList();

        permissionGroupRepo.saveAll(permissionGroups);

        return GroupResponse.builder()
                .code(ResourceConstant.GR_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(groupDtoMapper.apply(group))
                .message(getMessageBundle(ResourceConstant.GR_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GroupResponse addGroup(GroupAddRequest request) {
        if (groupRepo.existsByGroupName(request.getGroupName())) {
            throw new ApiRequestException(ResourceConstant.GR_011, getMessageBundle(ResourceConstant.GR_011));
        }

        Group group = groupRepo.save(
                Group.builder()
                        .groupName(request.getGroupName())
                        .description(request.getDescription())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );

        return GroupResponse.builder()
                .code(ResourceConstant.GR_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(groupDtoMapper.apply(group))
                .message(getMessageBundle(ResourceConstant.GR_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GroupResponse addPermissionForGroup(GroupRequest request) {
        Group group = getGroup(request.getGroupId());
        return null;
    }

    @Override
    public GroupResponse updateGroup(GroupRequest request) {
        Group group = getGroup(request.getGroupId());

        if (groupRepo.existsByGroupNameNot(group.getGroupName())) {
            throw new ApiRequestException(ResourceConstant.GR_011, getMessageBundle(ResourceConstant.GR_011));
        }

        if (!group.getGroupName().equals(request.getGroupName())) {
            group.setGroupName(request.getGroupName());
        }

        if (!group.getDescription().equals(request.getDescription())) {
            group.setDescription(request.getDescription());
        }

        Group groupSave = groupRepo.save(group);

        return GroupResponse.builder()
                .code(ResourceConstant.GR_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(groupDtoMapper.apply(groupSave))
                .message(getMessageBundle(ResourceConstant.GR_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private Group getGroup(UUID groupId) {
        return groupRepo.findGroupByIdAndIsDeleted(groupId, SystemConstant.ACTIVE)
                .orElseThrow(() -> new ApiRequestException(ResourceConstant.GR_001, getMessageBundle(ResourceConstant.GR_001)));
    }

    @Override
    public GroupResponse deleteGroup(UUID groupId) {
        if (groupRepo == null) {
            throw new ApiRequestException(ResourceConstant.PMS_G_001, getMessageBundle(ResourceConstant.PMS_G_001));
        }
        Group group = getGroup(groupId);
        group.setIsDeleted(false);
        Group groupSave = groupRepo.save(group);

        return GroupResponse.builder()
                .code(ResourceConstant.PMS_G_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(groupDtoMapper.apply(groupSave))
                .message(getMessageBundle(ResourceConstant.PMS_G_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GroupResponse findGroupById(UUID groupId) {
        Group group = getGroup(groupId);

        return GroupResponse.builder()
                .code(ResourceConstant.GR_002)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(groupDtoMapper.applyGroupDtoDetail(group))
                .message(getMessageBundle(ResourceConstant.GR_002))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public GroupResponses findAllGroup(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<Group> all = groupRepo.findAllNotAdminMaster(pageable);
//        try {
//            redisTemplate.opsForValue().getOperations().delete("ca");
//            if (redisTemplate.opsForValue().get("ca") != null) {
//                String jsonData = redisTemplate.opsForValue().get("ca");
//                System.out.println("Cache ne!");
//
//                List<Group> groups = objectMapper.readValue(
//                        jsonData,
//                        new TypeReference<>() {
//                        }
//                );
//
//                return groups.stream()
//                        .map(groupDtoMapper)
//                        .toList();
//            } else {
//                System.out.println("No Cache!");
//                all = groupRepo.findAll();
//
//                String valueAsString = objectMapper.writeValueAsString(all);
//                redisTemplate.opsForValue().set("ca", valueAsString);
//            }
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
        List<GroupDto> groupDtos = all.stream()
                .map(groupDtoMapper)
                .toList();

        PageableResponseUtil pageableResponse = baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages());

        return GroupResponses.builder()
                .code(ResourceConstant.GR_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(groupDtos)
                .meta(pageableResponse)
                .message(getMessageBundle(ResourceConstant.GR_004))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }
}
