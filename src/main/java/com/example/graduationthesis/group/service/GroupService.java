package com.example.graduationthesis.group.service;

import com.example.graduationthesis.group.dto.request.GroupAddPermissionRequest;
import com.example.graduationthesis.group.dto.request.GroupAddRequest;
import com.example.graduationthesis.group.dto.request.GroupAddUserForGroup;
import com.example.graduationthesis.group.dto.request.GroupRequest;
import com.example.graduationthesis.group.dto.response.GroupResponse;
import com.example.graduationthesis.group.dto.response.GroupResponses;

import java.util.UUID;

public interface GroupService {

    GroupResponse addUserForGroup(GroupAddUserForGroup request);

    GroupResponse updatePermissionsForGroup(GroupAddPermissionRequest request);

    GroupResponse addGroup(GroupAddRequest request);

    GroupResponse addPermissionForGroup(GroupRequest request);

    GroupResponse updateGroup(GroupRequest request);

    GroupResponse deleteGroup(UUID groupId);

    GroupResponse findGroupById(UUID groupId);

    GroupResponses findAllGroup(int currentPage, int limitPage);

}
