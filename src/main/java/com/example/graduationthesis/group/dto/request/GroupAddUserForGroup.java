package com.example.graduationthesis.group.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class GroupAddUserForGroup {
    private UUID userId;

    private List<GroupIdRequest> groupIds;

    public record GroupIdRequest(
            UUID groupId
    ) {
    }
}
