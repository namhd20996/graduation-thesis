package com.example.graduationthesis.group.dto.response;

import com.example.graduationthesis.permission.dto.PermissionDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;


@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class GroupDtoDetailResp extends AbsGroupDto {
    private List<PermissionDto> permissions;
}
