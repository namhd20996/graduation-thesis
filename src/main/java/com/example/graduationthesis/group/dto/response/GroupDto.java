package com.example.graduationthesis.group.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder(toBuilder = true)
public class GroupDto extends AbsGroupDto {
}
