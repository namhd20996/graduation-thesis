package com.example.graduationthesis.group.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class GroupRequest {
    private UUID groupId;
    private String groupName;

    private String description;

}
