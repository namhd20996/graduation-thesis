package com.example.graduationthesis.group.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GroupAddRequest {
    private String groupName;

    private String description;

}
