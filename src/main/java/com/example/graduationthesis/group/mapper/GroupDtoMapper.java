package com.example.graduationthesis.group.mapper;


import com.example.graduationthesis.group.dto.response.GroupDto;
import com.example.graduationthesis.group.dto.response.GroupDtoDetailResp;
import com.example.graduationthesis.group.entity.Group;
import com.example.graduationthesis.permission.dto.PermissionDto;
import com.example.graduationthesis.permission.mapper.PermissionDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class GroupDtoMapper implements Function<Group, GroupDto> {

    private final PermissionDtoMapper permissionDtoMapper;

    public GroupDtoDetailResp applyGroupDtoDetail(Group group) {
        List<PermissionDto> permissions = group.getPermissionGroups().stream()
                .map(item -> permissionDtoMapper.apply(item.getPermission()))
                .toList();
        return GroupDtoDetailResp.builder()
                .groupId(group.getId())
                .groupName(group.getGroupName())
                .isDeleted(group.getIsDeleted())
                .status(group.getStatus())
                .description(group.getDescription())
                .permissions(permissions)
                .build();
    }

    @Override
    public GroupDto apply(Group group) {

        return GroupDto.builder()
                .groupId(group.getId())
                .groupName(group.getGroupName())
                .isDeleted(group.getIsDeleted())
                .status(group.getStatus())
                .description(group.getDescription())
                .build();
    }
}
