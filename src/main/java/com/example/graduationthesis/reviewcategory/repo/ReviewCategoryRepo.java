package com.example.graduationthesis.reviewcategory.repo;

import com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName;
import com.example.graduationthesis.reviewcategory.entity.ReviewCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ReviewCategoryRepo extends JpaRepository<ReviewCategory, UUID> {

    @Query("""
                SELECT
                        new com.example.graduationthesis.reviewcategory.dto.response.StatisticalRatingGroupByName(
                            c.categoryName,
                            SUM(r.rating),
                            COUNT(c.categoryName),
                            CAST(CASE WHEN COUNT(c.categoryName) > 0 THEN SUM(r.rating) / COUNT(c.categoryName) ELSE 0 END AS DOUBLE)
                        )
                FROM
                    ReviewCategory rc
                INNER JOIN
                    Review r ON r.id = rc.review.id
                INNER JOIN
                    Category c ON c.id = rc.category.id
                WHERE
                    r.hotel.id = ?1
                GROUP BY
                    c.categoryName
            """)
    Optional<List<StatisticalRatingGroupByName>> statisticalRatingGroupNameByHotelId(UUID hotelId);
}
