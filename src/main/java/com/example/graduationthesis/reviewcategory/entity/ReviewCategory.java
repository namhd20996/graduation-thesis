package com.example.graduationthesis.reviewcategory.entity;

import com.example.graduationthesis.category.entity.Category;
import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_review_category")
public class ReviewCategory extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "review_id")
    private Review review;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
