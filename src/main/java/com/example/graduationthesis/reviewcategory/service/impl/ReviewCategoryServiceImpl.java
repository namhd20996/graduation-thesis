package com.example.graduationthesis.reviewcategory.service.impl;

import com.example.graduationthesis.reviewcategory.repo.ReviewCategoryRepo;
import com.example.graduationthesis.reviewcategory.service.ReviewCategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ReviewCategoryServiceImpl implements ReviewCategoryService {

    private final ReviewCategoryRepo reviewCategoryRepo;

}
