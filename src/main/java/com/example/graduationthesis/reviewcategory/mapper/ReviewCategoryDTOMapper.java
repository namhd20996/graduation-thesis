package com.example.graduationthesis.reviewcategory.mapper;

import com.example.graduationthesis.reviewcategory.dto.ReviewCategoryDTO;
import com.example.graduationthesis.reviewcategory.entity.ReviewCategory;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class ReviewCategoryDTOMapper implements
        Function<ReviewCategory, ReviewCategoryDTO> {

    @Override
    public ReviewCategoryDTO apply(ReviewCategory reviewCategory) {
        return new ReviewCategoryDTO(
                reviewCategory.getId(),
                reviewCategory.getReview().getId(),
                reviewCategory.getCategory().getId()
        );
    }
}
