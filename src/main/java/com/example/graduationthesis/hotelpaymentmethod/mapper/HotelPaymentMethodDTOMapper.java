package com.example.graduationthesis.hotelpaymentmethod.mapper;

import com.example.graduationthesis.hotelpaymentmethod.dto.HotelPaymentMethodDTO;
import com.example.graduationthesis.hotelpaymentmethod.entity.HotelPaymentMethod;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class HotelPaymentMethodDTOMapper implements
        Function<HotelPaymentMethod, HotelPaymentMethodDTO> {

    private final ModelMapper mapper;

    @Override
    public HotelPaymentMethodDTO apply(HotelPaymentMethod hotelPaymentMethod) {
        return mapper.map(hotelPaymentMethod, HotelPaymentMethodDTO.class);
    }
}
