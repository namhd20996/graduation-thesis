package com.example.graduationthesis.hotelpaymentmethod.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class HotelPaymentMethodDTO extends BaseDTOUtil {

    private Boolean creditOrDebitCard;

    private Double commissionPercentage;

    private String nameOnInvoice;

    private String invoiceAddress;
}
