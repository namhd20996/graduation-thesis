package com.example.graduationthesis.hotelpaymentmethod.dto.request;

import com.example.graduationthesis.hotelpaymentmethod.dto.request.HotelPaymentMethodAddressRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HotelPaymentMethodRequest {

    private Boolean creditOrDebitCard;

    private String nameOnInvoice;

    private Boolean flagReceivingAddress;

    private String invoiceAddress;

    private HotelPaymentMethodAddressRequest hotelPaymentMethodAddressRequest;
}
