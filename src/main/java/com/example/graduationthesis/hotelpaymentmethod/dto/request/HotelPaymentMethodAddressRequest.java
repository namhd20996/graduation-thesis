package com.example.graduationthesis.hotelpaymentmethod.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HotelPaymentMethodAddressRequest {

    private String streetAddress;

    private String districtAddress;

    private String country;

    private String city;
}
