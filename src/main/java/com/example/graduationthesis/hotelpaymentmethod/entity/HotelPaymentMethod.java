package com.example.graduationthesis.hotelpaymentmethod.entity;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.utils.BaseEntityUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_hotel_payment_method")
public class HotelPaymentMethod extends BaseEntityUtil {

    @Column
    private Boolean creditOrDebitCard;
    @Column
    private Double commissionPercentage;
    @Column
    private String nameOnInvoice;
    @Column
    private String invoiceAddress;
    @OneToMany(mappedBy = "hotelPaymentMethod", fetch = FetchType.LAZY)
    private List<Hotel> hotels;
}
