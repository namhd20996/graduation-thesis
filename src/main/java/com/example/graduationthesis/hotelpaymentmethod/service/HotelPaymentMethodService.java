package com.example.graduationthesis.hotelpaymentmethod.service;

import com.example.graduationthesis.hotelpaymentmethod.dto.request.HotelPaymentMethodRequest;
import com.example.graduationthesis.hotelpaymentmethod.dto.response.HotelPaymentMethodResponse;
import com.example.graduationthesis.hotelpaymentmethod.dto.response.HotelPaymentMethodResponses;

public interface HotelPaymentMethodService {

    HotelPaymentMethodResponse addHotelPaymentMethod(String jwtToken, HotelPaymentMethodRequest request);

    HotelPaymentMethodResponses findAllHotelPaymentMethod(int currentPage, int limitPage);
}
