package com.example.graduationthesis.serviceandamenityroom.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class ServiceAndAmenityRoomDTOResp extends AbsServiceAndAmenityRoomDTO {
}
