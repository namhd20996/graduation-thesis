package com.example.graduationthesis.serviceandamenityroom.dto.response;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceAmenityRoomResp {

    private String serviceAmenityRoomName;

}
