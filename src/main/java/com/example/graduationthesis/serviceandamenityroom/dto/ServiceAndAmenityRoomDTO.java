package com.example.graduationthesis.serviceandamenityroom.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class ServiceAndAmenityRoomDTO extends BaseDTOUtil {

    private Integer extraBed;

    private Boolean childrenSleepInCribs;

    private Boolean typeOfGuestChildren;

    private String childrenOld;

    private BigDecimal priceGuestChildren;

    private Boolean typeOfGuestAdults;

    private BigDecimal priceGuestAdults;
}
