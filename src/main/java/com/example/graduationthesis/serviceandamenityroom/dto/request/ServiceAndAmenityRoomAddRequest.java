package com.example.graduationthesis.serviceandamenityroom.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ServiceAndAmenityRoomAddRequest {

    private UUID roomId;

    private Integer extraBed;

    private Boolean childrenSleepInCribs;

    private Boolean typeOfGuestChildren;

    private String childrenOld;

    private BigDecimal priceGuestChildren;

    private Boolean typeOfGuestAdults;

    private BigDecimal priceGuestAdults;

    private List<ServiceAndAmenityRoomUUIDRequest> serviceAndAmenityId;
}
