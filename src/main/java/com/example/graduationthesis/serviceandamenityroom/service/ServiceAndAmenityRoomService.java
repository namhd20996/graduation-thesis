package com.example.graduationthesis.serviceandamenityroom.service;

import com.example.graduationthesis.serviceandamenityroom.dto.request.ServiceAndAmenityRoomAddRequest;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAndAmenityRoomResp;

import java.util.UUID;

public interface ServiceAndAmenityRoomService {

    ServiceAndAmenityRoomResp addServiceAndAmenityRoom(String jwtToken, ServiceAndAmenityRoomAddRequest request);

    ServiceAndAmenityRoomResp updateServiceAndAmenityRoom(ServiceAndAmenityRoomAddRequest request);

    ServiceAndAmenityRoomResp deleteServiceAndAmenityRoom(UUID uuid);

}
