package com.example.graduationthesis.category.dto.response;

import com.example.graduationthesis.utils.BaseResponseUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class CategoryResponse extends BaseResponseUtil {
}
