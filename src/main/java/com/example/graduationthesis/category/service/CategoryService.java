package com.example.graduationthesis.category.service;

import com.example.graduationthesis.category.dto.request.CategoryUpdateRequest;
import com.example.graduationthesis.category.dto.request.CategoryAddRequest;
import com.example.graduationthesis.category.dto.response.CategoryResponse;
import com.example.graduationthesis.category.dto.response.CategoryResponses;

import java.util.UUID;

public interface CategoryService {

    CategoryResponse addCategory(CategoryAddRequest request);

    CategoryResponse updateCategory(CategoryUpdateRequest request, UUID cid);

    CategoryResponse deleteCategory(UUID cid);

    CategoryResponses findAllCategoryByStatus(Integer page, Integer limit, String status);
}
