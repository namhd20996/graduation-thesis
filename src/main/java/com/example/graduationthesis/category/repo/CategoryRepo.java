package com.example.graduationthesis.category.repo;

import com.example.graduationthesis.category.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CategoryRepo extends JpaRepository<Category, UUID> {

    long countByStatus(String status);

    boolean existsByCategoryName(String categoryName);

    boolean existsByCategoryNameAndIdNot(String categoryName, UUID id);

    Optional<Page<Category>> findCategoriesByStatus(String status, Pageable pageable);

    Optional<List<Category>> findCategoriesByStatus(String status);
}
