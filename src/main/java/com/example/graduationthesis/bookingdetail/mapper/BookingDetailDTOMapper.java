package com.example.graduationthesis.bookingdetail.mapper;

import com.example.graduationthesis.booking.entity.Booking;
import com.example.graduationthesis.bookingdetail.dto.BookingDetailDTO;
import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.room.entity.Room;
import com.example.graduationthesis.serviceandamenityroom.dto.response.ServiceAmenityRoomResp;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
public class BookingDetailDTOMapper implements Function<BookingDetail, BookingDetailDTO> {

    @Override
    public BookingDetailDTO apply(BookingDetail bookingDetail) {
        Hotel hotel = bookingDetail.getRoom().getHotel();
        Room room = bookingDetail.getRoom();
        Booking booking = bookingDetail.getBooking();
        List<ServiceAmenityRoomResp> serviceAmenityRooms = room.getServiceAndAmenityRooms().stream()
                .map(item -> new ServiceAmenityRoomResp(item.getServiceAndAmenity().getName())
                ).toList();

        return new BookingDetailDTO(
                bookingDetail.getRoom().getRoomName(),
                bookingDetail.getQuantityRoomBooking(),
                bookingDetail.getCheckInDate(),
                bookingDetail.getCheckOutDate(),
                bookingDetail.getTotalPriceRoom(),
                bookingDetail.getTotalPriceRoomOrigin(),
                bookingDetail.getTotalDay(),
                bookingDetail.getBookingCode(),
                bookingDetail.getPinCode(),
                bookingDetail.getDiscount(),
                room.getBedName(),
                hotel.getPhoneNumber(),
                hotel.getUser().getEmail(),
                booking.getQuantityAdult(),
                booking.getQuantityChildren(),
                serviceAmenityRooms
        );
    }
}
