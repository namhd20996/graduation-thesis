package com.example.graduationthesis.bookingdetail.repo;

import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BookingDetailRepo extends JpaRepository<BookingDetail, UUID> {

  Page<BookingDetail> findAllByBookingId(UUID bookingId, Pageable pageable);
}
