package com.example.graduationthesis.bookingdetail.service.impl;

import com.example.graduationthesis.bookingdetail.dto.BookingDetailDTO;
import com.example.graduationthesis.bookingdetail.dto.response.BookingDetailResponses;
import com.example.graduationthesis.bookingdetail.entity.BookingDetail;
import com.example.graduationthesis.bookingdetail.mapper.BookingDetailDTOMapper;
import com.example.graduationthesis.bookingdetail.repo.BookingDetailRepo;
import com.example.graduationthesis.bookingdetail.service.BookingDetailService;
import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class BookingDetailServiceImpl implements BookingDetailService {

    private final BookingDetailRepo bookingDetailRepo;

    private final BookingDetailDTOMapper bookingDetailDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public BookingDetailResponses findAllByBookingId(UUID bid, Integer currentPage, Integer limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<BookingDetail> bookingDetails = bookingDetailRepo.findAllByBookingId(bid, pageable);
        List<BookingDetailDTO> list = bookingDetails
                .stream()
                .map(bookingDetailDTOMapper)
                .toList();
        return BookingDetailResponses.builder()
                .code(ResourceConstant.BKD_004)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(list)
                .message(getMessageBundle(ResourceConstant.BKD_004))
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, bookingDetails.getTotalPages()))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }
}
