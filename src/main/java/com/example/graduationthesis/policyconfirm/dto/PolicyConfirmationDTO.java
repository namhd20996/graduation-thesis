package com.example.graduationthesis.policyconfirm.dto;

import com.example.graduationthesis.utils.BaseDTOUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class PolicyConfirmationDTO extends BaseDTOUtil {

    private Integer acceptGeneralTermsPrivacy;

    private Integer certificationOfLegalBusinessOperations;
}
