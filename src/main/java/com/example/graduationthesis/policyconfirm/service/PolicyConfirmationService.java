package com.example.graduationthesis.policyconfirm.service;

import com.example.graduationthesis.policyconfirm.dto.request.PolicyConfirmationRequest;
import com.example.graduationthesis.policyconfirm.dto.response.PolicyConfirmResponse;

public interface PolicyConfirmationService {

    PolicyConfirmResponse addPolicyConfirmation(String jwtToken, PolicyConfirmationRequest request);
}
