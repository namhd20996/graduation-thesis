package com.example.graduationthesis.exception;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@RequiredArgsConstructor
public class ApiExceptionHandle {

    private final BaseAmenityUtil baseAmenity;

    private String extractErrorCode(String fullErrorMessage) {
        int startBracketIndex = fullErrorMessage.indexOf("Detail:");
        int endBracketIndex = fullErrorMessage.indexOf(']');

        if (startBracketIndex != -1 && endBracketIndex != -1 && startBracketIndex < endBracketIndex) {
            return fullErrorMessage.substring(startBracketIndex + 1, endBracketIndex).trim();
        }

        return fullErrorMessage;
    }

    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleException(ApiRequestException e) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ApiException exception = new ApiException(
                e.getCode(),
                SystemConstant.STATUS_BAD_REQUEST,
                e.getMessage(),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, badRequest);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<Object> handleBindException(BindException e) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        List<String> listError = new ArrayList<>();

        if (e.getBindingResult().hasErrors()) {
            listError = e.getBindingResult().getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .map(baseAmenity::getMessageBundle)
                    .toList();
        }
        ApiException exception = new ApiException(
                ResourceConstant.ERROR_VALIDATION_FIELD,
                SystemConstant.STATUS_BAD_REQUEST,
                listError.toString(),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, badRequest);
    }

    @ExceptionHandler({ConstraintViolationException.class, TransactionSystemException.class})
    public ResponseEntity<Object> handleException(ConstraintViolationException e) {
        HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
        String code = e.getConstraintViolations().iterator().next().getMessage();
        ApiException exception = new ApiException(
                baseAmenity.getMessageBundle(ResourceConstant.ERROR_SERVER),
                SystemConstant.STATUS_INTERNAL,
                extractErrorCode(e.getMessage()),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, internalServerError);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleException(AccessDeniedException e) {
        HttpStatus forbidden = HttpStatus.FORBIDDEN;
        ApiException exception = new ApiException(
                baseAmenity.getMessageBundle(ResourceConstant.ERROR_FORBIDDEN),
                SystemConstant.STATUS_FORBIDDEN,
                extractErrorCode(e.getMessage()),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, forbidden);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleException(DataIntegrityViolationException e) {

        HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiException exception = new ApiException(
                baseAmenity.getMessageBundle(ResourceConstant.ERROR_SERVER),
                SystemConstant.STATUS_INTERNAL,
                extractErrorCode(e.getMessage()),
                baseAmenity.responseTime()
        );
        return new ResponseEntity<>(exception, internalServerError);
    }

//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<Object> handleException(Exception e) {
//        HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
//        ApiException exception = new ApiException(
//                baseAmenity.getMessageBundle(ResourceConstant.ERROR_SERVER),
//                SystemConstant.STATUS_INTERNAL,
//                e.getMessage(),
//                baseAmenity.responseTime()
//        );
//        return new ResponseEntity<>(exception, internalServerError);
//    }

}