package com.example.graduationthesis.hoteltype.mapper;

import com.example.graduationthesis.hoteltype.dto.HotelTypeDTO;
import com.example.graduationthesis.hoteltype.entity.HotelType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class HotelTypeDTOMapper implements Function<HotelType, HotelTypeDTO> {

    @Override
    public HotelTypeDTO apply(HotelType hotelType) {
        return HotelTypeDTO.builder()
                .id(hotelType.getId())
                .name(hotelType.getName())
                .status(hotelType.getStatus())
                .description(hotelType.getDescription())
                .createdBy(hotelType.getCreatedBy())
                .createdDate(hotelType.getCreatedDate())
                .lastModifiedBy(hotelType.getLastModifiedBy())
                .lastModifiedDate(hotelType.getLastModifiedDate())
                .urlImage(hotelType.getUrlImage())
                .isDeleted(hotelType.getIsDeleted())
                .build();
    }
}
