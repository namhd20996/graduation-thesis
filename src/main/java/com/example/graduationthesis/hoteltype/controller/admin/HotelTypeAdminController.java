package com.example.graduationthesis.hoteltype.controller.admin;

import com.example.graduationthesis.constant.HotelTypeConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeResponse;
import com.example.graduationthesis.hoteltype.dto.response.HotelTypeResponses;
import com.example.graduationthesis.hoteltype.service.HotelTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + HotelTypeConstant.API_HOTEL_TYPE)
@RequiredArgsConstructor
public class HotelTypeAdminController {

    private final HotelTypeService hotelTypeService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<HotelTypeResponse> addHotelType(
            @RequestParam("name") String name,
            @RequestParam("description") String description,
            @RequestParam("image") MultipartFile multipartFile
    ) {
        return new ResponseEntity<>(hotelTypeService.addHotelType(name, description, multipartFile), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping(value = "/{httid}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<HotelTypeResponse> updateHotelType(
            @RequestParam("name") String name,
            @RequestParam("description") String description,
            @PathVariable("httid") UUID httid,
            @RequestParam("image") MultipartFile multipartFile
    ) {
        return new ResponseEntity<>(hotelTypeService.updateHotelType(name, description, multipartFile, httid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping("/{httid}")
    public ResponseEntity<HotelTypeResponse> deleteHotelType(@PathVariable("httid") UUID httid) {
        return new ResponseEntity<>(hotelTypeService.deleteHotelType(httid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<HotelTypeResponses> selectAllHotelType(
            @RequestParam(SystemConstant.PARAM_STATUS) Optional<String> status,
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(hotelTypeService.findAll(
                status.orElse(SystemConstant.STATUS_ACTIVE),
                currentPage.orElse(1),
                limitPage.orElse(8)),
                HttpStatus.OK
        );
    }
}
