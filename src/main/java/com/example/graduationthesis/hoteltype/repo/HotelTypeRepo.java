package com.example.graduationthesis.hoteltype.repo;

import com.example.graduationthesis.hoteltype.dto.response.HotelTypeIndexResp;
import com.example.graduationthesis.hoteltype.entity.HotelType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HotelTypeRepo extends JpaRepository<HotelType, UUID> {

    @Query("""
                SELECT
                    new com.example.graduationthesis.hoteltype.dto.response.HotelTypeIndexResp(
                        ht.name,
                        SUM(CASE WHEN r.quantityRoom > 0 THEN r.quantityRoom ELSE 0 END),
                        ht.urlImage
                    )
                FROM
                    HotelType ht
                LEFT JOIN
                    ht.hotels h ON h.hotelType.id = ht.id AND h.status = 'ACTIVE'
                LEFT JOIN
                    Room r ON r.hotel.id = h.id AND r.status = 'ACTIVE' AND
                    ht.status = 'ACTIVE'
                GROUP BY
                    ht.name, ht.urlImage
            """)
    Optional<List<HotelTypeIndexResp>> selectHotelTypeIndex();

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, UUID hotelTypeId);

    @Query("""
            SELECT
                o
            FROM
                HotelType o
            WHERE
                o.status = ?1
            ORDER BY
                o.createdDate DESC
            """)
    Page<HotelType> findAllByStatus(String status, Pageable pageable);
}
