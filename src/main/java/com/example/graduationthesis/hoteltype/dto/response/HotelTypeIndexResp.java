package com.example.graduationthesis.hoteltype.dto.response;

public record HotelTypeIndexResp(
        String name,
        Long quantityRoom,
        String url
) {
}
