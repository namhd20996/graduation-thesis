package com.example.graduationthesis.feedback.repo;

import com.example.graduationthesis.feedback.entity.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FeedbackRepo extends JpaRepository<Feedback, UUID> {
}
