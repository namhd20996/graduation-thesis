package com.example.graduationthesis.feedback.service.impl;

import com.example.graduationthesis.constant.ResourceConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.feedback.dto.request.FeedbackAddRequest;
import com.example.graduationthesis.feedback.dto.request.FeedbackUpdateRequest;
import com.example.graduationthesis.feedback.dto.response.FeedbackResponse;
import com.example.graduationthesis.feedback.entity.Feedback;
import com.example.graduationthesis.feedback.mapper.FeedbackDTOMapper;
import com.example.graduationthesis.feedback.repo.FeedbackRepo;
import com.example.graduationthesis.feedback.service.FeedbackService;
import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.review.repo.ReviewRepo;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepo feedbackRepo;

    private final FeedbackDTOMapper feedbackDTOMapper;

    private final ReviewRepo reviewRepo;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }


    @Override
    public FeedbackResponse addFeedbackByReviewId(FeedbackAddRequest request) {
        Review review = reviewRepo.findById(request.getReviewId())
                .orElseThrow(() -> new ApiRequestException("find feedback by id " + request.getReviewId() + " not found!.", ""));

        Feedback feedback = feedbackRepo.save(
                Feedback.builder()
                        .review(review)
                        .feedbackContent(request.getFeedbackContent())
                        .isDeleted(SystemConstant.ACTIVE)
                        .status(SystemConstant.STATUS_ACTIVE)
                        .build()
        );
        return FeedbackResponse.builder()
                .code(ResourceConstant.FB_009)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(feedbackDTOMapper.apply(feedback))
                .message(getMessageBundle(ResourceConstant.FB_009))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public FeedbackResponse updateFeedback(FeedbackUpdateRequest request) {
        Feedback feedback = getFeedback(request.getFeedbackId());

        if (!feedback.getFeedbackContent().equals(request.getFeedbackContent()))
            feedback.setFeedbackContent(request.getFeedbackContent());

        Feedback save = feedbackRepo.save(feedback);


        return FeedbackResponse.builder()
                .code(ResourceConstant.FB_007)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(feedbackDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.FB_007))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public FeedbackResponse deleteFeedback(UUID fid) {
        Feedback feedback = getFeedback(fid);
        feedback.setIsDeleted(SystemConstant.NO_ACTIVE);
        feedback.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        Feedback save = feedbackRepo.save(feedback);
        return FeedbackResponse.builder()
                .code(ResourceConstant.FB_005)
                .status(SystemConstant.STATUS_SUCCESS)
                .data(feedbackDTOMapper.apply(save))
                .message(getMessageBundle(ResourceConstant.FB_005))
                .responseTime(baseAmenityUtil.responseTime())
                .build();

    }

    private Feedback getFeedback(UUID fid) {
        return feedbackRepo.findById(fid)
                .orElseThrow(() -> new ApiRequestException("find feedback by id " + fid + " not found!.", ""));
    }
}
