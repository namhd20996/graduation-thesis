package com.example.graduationthesis.feedback.service;

import com.example.graduationthesis.feedback.dto.request.FeedbackAddRequest;
import com.example.graduationthesis.feedback.dto.request.FeedbackUpdateRequest;
import com.example.graduationthesis.feedback.dto.FeedbackDTO;
import com.example.graduationthesis.feedback.dto.response.FeedbackResponse;

import java.util.UUID;

public interface FeedbackService {

    FeedbackResponse addFeedbackByReviewId(FeedbackAddRequest request);

    FeedbackResponse updateFeedback(FeedbackUpdateRequest request);

    FeedbackResponse deleteFeedback(UUID fid);
}
