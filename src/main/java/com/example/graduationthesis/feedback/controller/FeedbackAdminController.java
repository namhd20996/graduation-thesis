package com.example.graduationthesis.feedback.controller;

import com.example.graduationthesis.constant.FeedBackConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.feedback.dto.request.FeedbackAddRequest;
import com.example.graduationthesis.feedback.dto.response.FeedbackResponse;
import com.example.graduationthesis.feedback.service.FeedbackService;
import com.example.graduationthesis.feedback.dto.request.FeedbackUpdateRequest;
import com.example.graduationthesis.feedback.dto.FeedbackDTO;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + FeedBackConstant.API_FEED_BACK)
@RequiredArgsConstructor
public class FeedbackAdminController {

    private final FeedbackService feedbackService;

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PostMapping
    public ResponseEntity<FeedbackResponse> addFeedback(
            @Valid @RequestBody FeedbackAddRequest request
    ) {
        return new ResponseEntity<>(feedbackService.addFeedbackByReviewId(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @PutMapping
    public ResponseEntity<FeedbackResponse> updateFeedback(
            @Valid @RequestBody FeedbackUpdateRequest request
    ) {
        return new ResponseEntity<>(feedbackService.updateFeedback(request), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @DeleteMapping
    public ResponseEntity<FeedbackResponse> updateFeedback(@RequestParam("fid") UUID fid) {
        feedbackService.deleteFeedback(fid);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
