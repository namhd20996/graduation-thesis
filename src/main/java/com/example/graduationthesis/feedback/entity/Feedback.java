package com.example.graduationthesis.feedback.entity;

import com.example.graduationthesis.review.entity.Review;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "_feedback")
public class Feedback extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "review_id")
    private Review review;
    @Column
    @CreatedDate
    private Date feedbackDate;
    @Column(length = 512)
    private String feedbackContent;

}
