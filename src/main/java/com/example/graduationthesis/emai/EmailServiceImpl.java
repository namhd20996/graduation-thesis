package com.example.graduationthesis.emai;

import com.example.graduationthesis.booking.dto.response.BookingEmailCustomer;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final static Logger LOGGER = LoggerFactory
            .getLogger(EmailService.class);

    private final JavaMailSender mailSender;

    @Override
    public void send(String to, String email, String subject, BufferedImage qrCodeImage) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper =
                    new MimeMessageHelper(mimeMessage, true,"utf-8");
            helper.setText(email, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom("staying.booking.online@gmail.com", "Staying.com");

            if (qrCodeImage != null) {
                // Chuyển đổi hình ảnh QR code thành mảng byte
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ImageIO.write(qrCodeImage, "png", byteArrayOutputStream);
                byteArrayOutputStream.flush();
                byte[] qrCodeBytes = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                // Convert byte array to InputStreamSource
                InputStreamSource qrCodeSource = new ByteArrayResource(qrCodeBytes);
                // Thêm hình ảnh mã QR code vào email
                helper.addAttachment("qr_code.png", qrCodeSource, "image/png");
            }
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String buildEmailWelcome(String email) {
        return "<div style=\"font-family: Arial, sans-serif; background-color: #f4f4f4; margin: 0; padding: 0;\">\n" +
                "    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#f4f4f4\">\n" +
                "        <tr>\n" +
                "            <td align=\"center\">\n" +
                "                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color: #ffffff; border-radius: 5px; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1); margin: 20px auto;\">\n" +
                "                    <tr>\n" +
                "                        <td align=\"center\" style=\"padding: 20px;\">\n" +
                "                            <h1 style=\"color: #333;\">Chào mừng bạn đến với Staying.com!</h1>\n" +
                "                            <p style=\"color: #666; line-height: 1.6;\">Cảm ơn bạn đã đăng ký để nhận các cập nhật và thông tin mới nhất từ chúng tôi.</p>\n" +
                "                            <p style=\"color: #666; line-height: 1.6;\">Chúng tôi cam kết sẽ cung cấp cho bạn những thông tin thú vị và giá trị về việc đăng chỗ nghỉ online ở trang web của chúng tôi. Đừng bỏ lỡ cơ hội nhận các ưu đãi đặc biệt và thông tin quan trọng.</p>\n" +
                "                            <p style=\"color: #666; line-height: 1.6;\"> Bây giờ bạn có thể đăng nhập bằng email: <a href=\"mailto:youremail@example.com\" style=\"color: #007BFF; text-decoration: none;\">" + email + "</a> và mật khẩu đã tạo.</p>\n" +
                "                            <p style=\"color: #666; line-height: 1.6;\">Nếu bạn có bất kỳ câu hỏi hoặc góp ý nào, đừng ngần ngại liên hệ với chúng tôi qua email <a href=\"mailto:staying.booking.online@gmail.com\" style=\"color: #007BFF; text-decoration: none;\">staying.booking.online@gmail.com</a>.</p>\n" +
                "                            <p style=\"color: #666; line-height: 1.6;\">Cảm ơn bạn một lần nữa và chúng tôi rất mong được kết nối với bạn!</p>\n" +
                "                            <a href=\"[URL Của Trang Chính]\" style=\"display: inline-block; background-color: #007BFF; color: #fff; padding: 10px 20px; text-decoration: none; border-radius: 4px;\">Trang Chính</a>\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</div>";
    }

    @Override
    public String buildEmail(String name, String link, String password, boolean isValidEmail) {
        String send = "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote> Link will expire in 15 minutes. <p>See you soon</p>";
        String title = "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>";
        if (isValidEmail) {
            send = "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Password: " + password + "</p>";
            title = "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your password</span>";
        }
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">" +
                "" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>" +
                "" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">" +
                "    <tbody><tr>" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">" +
                "        " +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">" +
                "          <tbody><tr>" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">" +
                "                  <tbody><tr>" +
                "                    <td style=\"padding-left:10px\">" +
                "                  " +
                "                    </td>" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">" +
                title +
                "                    </td>" +
                "                  </tr>" +
                "                </tbody></table>" +
                "              </a>" +
                "            </td>" +
                "          </tr>" +
                "        </tbody></table>" +
                "        " +
                "      </td>" +
                "    </tr>" +
                "  </tbody></table>" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">" +
                "    <tbody><tr>" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>" +
                "      <td>" +
                "        " +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">" +
                "                  <tbody><tr>" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>" +
                "                  </tr>" +
                "                </tbody></table>" +
                "        " +
                "      </td>" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>" +
                "    </tr>" +
                "  </tbody></table>" +
                "" +
                "" +
                "" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">" +
                "    <tbody><tr>" +
                "      <td height=\"30\"><br></td>" +
                "    </tr>" +
                "    <tr>" +
                "      <td width=\"10\" valign=\"middle\"><br></td>" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">" +
                "        " +
                send +
                "        " +
                "      </td>" +
                "      <td width=\"10\" valign=\"middle\"><br></td>" +
                "    </tr>" +
                "    <tr>" +
                "      <td height=\"30\"><br></td>" +
                "    </tr>" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">" +
                "" +
                "</div></div>";
    }

    @Override
    public String buildEmailCongratulation(String email) {
        return "<body style=\"font-family: 'Arial', sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; text-align: center;\">\n" +
                "    <div style=\"background-color: #ffffff; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); padding: 30px; margin: 20px auto; max-width: 500px; text-align: left;\">\n" +
                "        <h1 style=\"color: #333333;\">Chúc mừng!</h1>\n" +
                "        <p style=\"color: #666666; text-indent: 20px;\">Xin chào [Tên người đăng ký],</p>\n" +
                "        <p style=\"color: #666666; text-indent: 20px;\">Chúc mừng bạn đã hoàn tất quá trình đăng ký thành công!</p>\n" +
                "        <p style=\"color: #666666; text-indent: 20px;\">Hãy kiểm tra email của bạn để cập nhật thông tin và nhận các thông báo quan trọng từ chúng tôi.</p>\n" +
                "        <p style=\"color: #666666; text-indent: 20px;\">Nếu bạn có bất kỳ câu hỏi hoặc cần hỗ trợ, vui lòng liên hệ với chúng tôi.</p>\n" +
                "        \n" +
                "        <!-- Thay đổi đường dẫn của href bên dưới bằng liên kết thực tế của bạn -->\n" +
                "        <a href=\"[Đường dẫn tới trang chủ]\" style=\"display: inline-block; padding: 10px 20px; margin-top: 20px; text-decoration: none; background-color: #4caf50; color: #ffffff; border-radius: 5px;\">Trang chủ</a>\n" +
                "    </div>\n" +
                "</body>";
    }

    @Override
    public String buildEmailVerify(String email) {
        return "<div style=\"font-family: 'Arial', sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; text-align: center; color: #333333;\">\n" +
                "    <div style=\"background-color: #ffffff; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); padding: 30px; margin: 20px auto; max-width: 600px;\">\n" +
                "        <h1 style=\"color: #4caf50;\">Staying.com</h1>\n" +
                "        \n" +
                "        <p style=\"margin-bottom: 15px;\">Xin chào [Tên đối tác],</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi rất vui mừng thông báo rằng quá trình xác nhận các điều khoản đã hoàn tất thành công. Đây là bước quan trọng để chúng ta có thể tiếp tục hợp tác một cách hiệu quả.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi đã cập nhật thông tin về các điều khoản trong hợp đồng và bạn có thể xem chi tiết bằng cách truy cập vào <a href=\"[Liên kết đến trang chi tiết điều khoản]\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">trang chi tiết điều khoản</a>.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Nếu bạn có bất kỳ thắc mắc hoặc cần thêm thông tin, đừng ngần ngại liên hệ với chúng tôi qua địa chỉ email: <a href=\"mailto:lienhe@example.com\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">lienhe@example.com</a>.</p>\n" +
                "\n" +
                "        <!-- Thay đổi tên đối tác và địa chỉ email bên dưới bằng thông tin thực tế của đối tác -->\n" +
                "        <p style=\"text-align: center; margin-bottom: 0; color: #333333;\">Trân trọng,<br>[Tên Công Ty]</p>\n" +
                "    </div>\n" +
                "</div>";
    }

    @Override
    public String buildEmailContract(String mail) {
        return "<div style=\"font-family: 'Arial', sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; text-align: center; color: #333333;\">\n" +
                "    <div style=\"background-color: #ffffff; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); padding: 30px; margin: 20px auto; max-width: 600px;\">\n" +
                "        <h1 style=\"color: #4caf50;\">Staying.com</h1>\n" +
                "        \n" +
                "        <p style=\"margin-bottom: 15px;\">Xin chào [Tên đối tác],</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi rất vui mừng thông báo rằng quá trình xác nhận các điều khoản đã hoàn tất thành công. Đây là bước quan trọng để chúng ta có thể tiếp tục hợp tác một cách hiệu quả.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi đã cập nhật thông tin về các điều khoản trong hợp đồng và bạn có thể xem chi tiết bằng cách truy cập vào <a href=\"[Liên kết đến trang chi tiết điều khoản]\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">trang chi tiết điều khoản</a>.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Nếu bạn có bất kỳ thắc mắc hoặc cần thêm thông tin, đừng ngần ngại liên hệ với chúng tôi qua địa chỉ email: <a href=\"mailto:lienhe@example.com\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">lienhe@example.com</a>.</p>\n" +
                "\n" +
                "        <!-- Thay đổi tên đối tác và địa chỉ email bên dưới bằng thông tin thực tế của đối tác -->\n" +
                "        <p style=\"text-align: center; margin-bottom: 0; color: #333333;\">Trân trọng,<br>[Tên Công Ty]</p>\n" +
                "    </div>\n" +
                "</div>";
    }

    @Override
    public String buildEmailWaitForConfirmation(String mail) {
        return "<div style=\"font-family: 'Arial', sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; text-align: center; color: #333333;\">\n" +
                "    <div style=\"background-color: #ffffff; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); padding: 30px; margin: 20px auto; max-width: 600px;\">\n" +
                "        <h1 style=\"color: #4caf50;\">Staying.com</h1>\n" +
                "        \n" +
                "        <p style=\"margin-bottom: 15px;\">Xin chào [Tên đối tác],</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi rất vui mừng thông báo rằng quá trình xác nhận các điều khoản đã hoàn tất thành công. Đây là bước quan trọng để chúng ta có thể tiếp tục hợp tác một cách hiệu quả.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Chúng tôi đã cập nhật thông tin về các điều khoản trong hợp đồng và bạn có thể xem chi tiết bằng cách truy cập vào <a href=\"[Liên kết đến trang chi tiết điều khoản]\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">trang chi tiết điều khoản</a>.</p>\n" +
                "\n" +
                "        <p style=\"margin-bottom: 15px;\">Nếu bạn có bất kỳ thắc mắc hoặc cần thêm thông tin, đừng ngần ngại liên hệ với chúng tôi qua địa chỉ email: <a href=\"mailto:lienhe@example.com\" style=\"color: #0066cc; text-decoration: none; font-weight: bold;\">lienhe@example.com</a>.</p>\n" +
                "\n" +
                "        <!-- Thay đổi tên đối tác và địa chỉ email bên dưới bằng thông tin thực tế của đối tác -->\n" +
                "        <p style=\"text-align: center; margin-bottom: 0; color: #333333;\">Trân trọng,<br>[Tên Công Ty]</p>\n" +
                "    </div>\n" +
                "</div>";
    }

    @Override
    public String buildEmailBookingCustomer(BookingEmailCustomer bookingEmailCustomer) {
        StringBuilder sb = new StringBuilder();
        bookingEmailCustomer.getBookingEmailDetails()
                .forEach(item -> sb.append("<tr>\n" + "<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.nameRoom())
                        .append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.checkInDate()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.checkOutDate()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.priceRoom()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.bookingCode()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.pinCode()).append("</td>\n")
                        .append("</tr>\n"));
        return " <div class=\"container\" style=\"width: 100%; max-width: 600px; margin: 0 auto; background-color: #fff; padding: 20px; box-sizing: border-box; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);\">\n" +
                "        <div class=\"confirmation\" style=\"background-color: #4caf50; color: #fff; padding: 10px; text-align: center; margin-bottom: 20px;\">\n" +
                "            <h2>Xác Nhận Đặt Phòng</h2>\n" +
                "            <p>Cảm ơn bạn đã đặt phòng khách sạn!</p>\n" +
                "        </div>\n" +
                "        <div class=\"details\" style=\"margin-top: 20px;\">\n" +
                "            <h3>Thông Tin Đặt Phòng</h3>\n" +
                "            <table style=\"width: 100%; border-collapse: collapse; margin-top: 10px;\">\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Tên Khách Hàng</th>\n" +
                "                    <td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">" + bookingEmailCustomer.getNameCustomer() + "</td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Email</th>\n" +
                "                    <td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">" + bookingEmailCustomer.getEmail() + "</td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Số Điện Thoại</th>\n" +
                "                    <td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">" + bookingEmailCustomer.getPhoneNumber() + "</td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <h3>Chi Tiết Đặt Phòng</h3>\n" +
                "            <table style=\"width: 100%; border-collapse: collapse; margin-top: 10px;\">\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Ngày Nhận Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Ngày Trả Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Giá Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Booking code</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Pin code</th>\n" +
                "                </tr>\n" +
                sb +
                "            </table>\n" +
                "            <div class=\"total\" style=\"margin-top: 20px; font-weight: bold;\">\n" +
                "                <p>Tổng Tiền: " + bookingEmailCustomer.getTotalPrice() + " VND</p>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"footer\" style=\"margin-top: 20px; text-align: center; color: #777;\">\n" +
                "            <p>Xin vui lòng liên hệ chúng tôi nếu có bất kỳ thắc mắc nào. Chúng tôi rất mong đợi sự đến của bạn!</p>\n" +
                "        </div>\n" +
                "    </div>";
    }

    @Override
    public String buildEmailBookingCancel(BookingEmailCustomer bookingEmailCustomer) {
        return null;
    }

    public String buildEmailBookingOwner(BookingEmailCustomer bookingEmailCustomer) {
        StringBuilder sb = new StringBuilder();
        bookingEmailCustomer.getBookingEmailDetails()
                .forEach(item -> sb.append("<tr>\n" + "<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.nameRoom())
                        .append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.checkInDate()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.checkOutDate()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.priceRoom()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.bookingCode()).append("</td>\n")
                        .append("<td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">")
                        .append(item.pinCode()).append("</td>\n")
                        .append("</tr>\n"));
        return " <div class=\"container\" style=\"width: 100%; max-width: 600px; margin: 0 auto; background-color: #fff; padding: 20px; box-sizing: border-box; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);\">\n" +
                "        <div class=\"confirmation\" style=\"background-color: #4caf50; color: #fff; padding: 10px; text-align: center; margin-bottom: 20px;\">\n" +
                "            <h2>Xác Nhận Đặt Phòng</h2>\n" +
                "            <p>Cảm ơn bạn đã đặt phòng khách sạn!</p>\n" +
                "        </div>\n" +
                "        <div class=\"details\" style=\"margin-top: 20px;\">\n" +
                "            <h3>Thông Tin Đặt Phòng</h3>\n" +
                "            <table style=\"width: 100%; border-collapse: collapse; margin-top: 10px;\">\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Tên Khách Hàng</th>\n" +
                "                    <td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">" + bookingEmailCustomer.getNameCustomer() + "</td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Email</th>\n" +
                "                    <td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">" + bookingEmailCustomer.getEmail() + "</td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Số Điện Thoại</th>\n" +
                "                    <td style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">" + bookingEmailCustomer.getPhoneNumber() + "</td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <h3>Chi Tiết Đặt Phòng</h3>\n" +
                "            <table style=\"width: 100%; border-collapse: collapse; margin-top: 10px;\">\n" +
                "                <tr>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Ngày Nhận Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Ngày Trả Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Giá Phòng</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Booking code</th>\n" +
                "                    <th style=\"border: 1px solid #ddd; padding: 10px; text-align: left;\">Pin code</th>\n" +
                "                </tr>\n" +
                sb +
                "            </table>\n" +
                "            <div class=\"total\" style=\"margin-top: 20px; font-weight: bold;\">\n" +
                "                <p>Tổng Tiền: " + bookingEmailCustomer.getTotalPrice() + " VND</p>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"footer\" style=\"margin-top: 20px; text-align: center; color: #777;\">\n" +
                "            <p>Khách sạn có đơn đặt hàng mới!.</p>\n" +
                "        </div>\n" +
                "    </div>";
    }

}
