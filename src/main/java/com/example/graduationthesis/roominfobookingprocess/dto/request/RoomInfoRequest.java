package com.example.graduationthesis.roominfobookingprocess.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class RoomInfoRequest {

    private UUID roomId;

    private Integer quantityRoom;
}
