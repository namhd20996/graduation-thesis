package com.example.graduationthesis.roominfobookingprocess.mapper;

import com.example.graduationthesis.roominfobookingprocess.dto.RoomInfoDTO;
import com.example.graduationthesis.roominfobookingprocess.dto.request.RoomInfoRequest;
import com.example.graduationthesis.roominfobookingprocess.entity.RoomInfo;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RoomInfoDtoMapper implements Function<RoomInfo, RoomInfoDTO> {

    @Override
    public RoomInfoDTO apply(RoomInfo roomInfo) {
        return RoomInfoDTO.builder()
                .roomId(roomInfo.getRoomId())
                .quantityRoom(roomInfo.getQuantityRoom())
                .jwtToken(roomInfo.getJwtToken())
                .build();
    }

    public RoomInfoRequest applyRoomInfoRequest(RoomInfo roomInfo) {
        return RoomInfoRequest.builder()
                .roomId(roomInfo.getRoomId())
                .quantityRoom(roomInfo.getQuantityRoom())
                .build();
    }
}
