package com.example.graduationthesis.roominfobookingprocess.repo;

import com.example.graduationthesis.roominfobookingprocess.entity.RoomInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface RoomInfoRepo extends JpaRepository<RoomInfo, UUID> {

    List<RoomInfo> findRoomInfosByJwtToken(String jwtToken);
}
