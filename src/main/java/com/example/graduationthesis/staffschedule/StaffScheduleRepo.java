package com.example.graduationthesis.staffschedule;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface StaffScheduleRepo extends JpaRepository<StaffSchedule, UUID> {
}
