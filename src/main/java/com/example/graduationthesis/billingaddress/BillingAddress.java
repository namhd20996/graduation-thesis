package com.example.graduationthesis.billingaddress;

import com.example.graduationthesis.hotel.entity.Hotel;
import com.example.graduationthesis.payment.entity.Payment;
import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_billing_address")
public class BillingAddress extends BaseIDUtil {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @Column
    private String address;
    @Column
    private String city;
    @Column
    private String state;
    @Column
    private String postalCode;
    @Column
    private String country;
    @OneToMany(mappedBy = "billingAddress", fetch = FetchType.LAZY)
    private List<Payment> payments;

}
