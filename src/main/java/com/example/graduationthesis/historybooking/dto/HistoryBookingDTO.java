package com.example.graduationthesis.historybooking.dto;

import java.time.LocalDate;

public record HistoryBookingDTO(
        Long totalDay,

        String fullName,

        LocalDate bookingDate,
        String roomName,
        String bedName,
        String roomTypeName
) {
}
