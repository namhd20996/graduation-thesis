package com.example.graduationthesis.historybooking.service;

import com.example.graduationthesis.historybooking.dto.HistoryBookingDTO;
import com.example.graduationthesis.historybooking.dto.request.HistoryBookingRequest;

public interface HistoryBookingService {

    HistoryBookingDTO findHistory(HistoryBookingRequest request);
}
