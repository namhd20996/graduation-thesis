package com.example.graduationthesis.historybooking.entity;

import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_history_booking")
public class HistoryBooking extends BaseIDUtil {

    private String bookingCode;

    private String pinCode;

    private Long totalDay;

    private String fullName;

    private LocalDate bookingDate;

    private String bedName;

    private String roomName;

    private String roomTypeName;

    private UUID hotelId;
}
