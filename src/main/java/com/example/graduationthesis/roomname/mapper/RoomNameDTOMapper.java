package com.example.graduationthesis.roomname.mapper;

import com.example.graduationthesis.roomname.dto.RoomNameDTO;
import com.example.graduationthesis.roomname.enity.RoomName;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class RoomNameDTOMapper implements Function<RoomName, RoomNameDTO> {

    private final ModelMapper mapper;

    @Override
    public RoomNameDTO apply(RoomName roomName) {
        return mapper.map(roomName, RoomNameDTO.class);
    }
}
