package com.example.graduationthesis.roomname.controller.admin;

import com.example.graduationthesis.constant.RoomNameConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.roomname.dto.request.RoomNameAddRequest;
import com.example.graduationthesis.roomname.dto.response.RoomNameResponse;
import com.example.graduationthesis.roomname.dto.response.RoomNameResponses;
import com.example.graduationthesis.roomname.service.RoomNameService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_ADMIN + SystemConstant.API_VERSION_ONE + RoomNameConstant.API_ROOM_NAME)
@RequiredArgsConstructor
public class RoomNameAdminController {

    private final RoomNameService roomNameService;

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PostMapping
    public ResponseEntity<RoomNameResponse> addRoomName(
            @Valid @RequestBody RoomNameAddRequest request
    ) {
        return new ResponseEntity<>(roomNameService.addRoomName(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @PutMapping("/{ronid}")
    public ResponseEntity<RoomNameResponse> updateRoomName(
            @Valid @RequestBody RoomNameAddRequest request,
            @PathVariable("ronid") UUID ronid
    ) {
        return new ResponseEntity<>(roomNameService.updateRoomName(request, ronid), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster)")
    @DeleteMapping("/{ronid}")
    public ResponseEntity<RoomNameResponse> deleteRoomName(@PathVariable("ronid") UUID ronid) {
        return new ResponseEntity<>(roomNameService.deleteRoomName(ronid), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority(@adminMaster, @hotelOwner)")
    @GetMapping
    public ResponseEntity<RoomNameResponses> selectAllRoomName(
            @RequestParam(SystemConstant.CURRENT_PAGE) Optional<Integer> currentPage,
            @RequestParam(SystemConstant.LIMIT_PAGE) Optional<Integer> limitPage
    ) {
        return new ResponseEntity<>(roomNameService.findAll(currentPage.orElse(1), limitPage.orElse(8)), HttpStatus.OK);
    }
}
