package com.example.graduationthesis.roomname.repo;

import com.example.graduationthesis.roomname.enity.RoomName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoomNameRepo extends JpaRepository<RoomName, UUID> {

    boolean existsByName(String name);

}
