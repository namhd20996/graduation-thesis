package com.example.graduationthesis.roomname.service.impl;

import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.exception.ApiRequestException;
import com.example.graduationthesis.roomname.dto.RoomNameDTO;
import com.example.graduationthesis.roomname.dto.request.RoomNameAddRequest;
import com.example.graduationthesis.roomname.dto.response.RoomNameResponse;
import com.example.graduationthesis.roomname.dto.response.RoomNameResponses;
import com.example.graduationthesis.roomname.enity.RoomName;
import com.example.graduationthesis.roomname.mapper.RoomNameDTOMapper;
import com.example.graduationthesis.roomname.repo.RoomNameRepo;
import com.example.graduationthesis.roomname.service.RoomNameService;
import com.example.graduationthesis.utils.BaseAmenityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class RoomNameServiceImpl implements RoomNameService {

    private final RoomNameRepo roomNameRepo;

    private final RoomNameDTOMapper roomNameDTOMapper;

    private final BaseAmenityUtil baseAmenityUtil;

    private String getMessageBundle(String key) {
        return baseAmenityUtil.getMessageBundle(key);
    }

    @Override
    public RoomNameResponse addRoomName(RoomNameAddRequest request) {
        extracted(request.getName());

        RoomName save = roomNameRepo.save(
                RoomName.builder()
                        .name(request.getName().trim())
                        .description(request.getDescription().trim())
                        .status(SystemConstant.STATUS_ACTIVE)
                        .isDeleted(SystemConstant.ACTIVE)
                        .build()
        );

        return RoomNameResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomNameDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomNameResponse updateRoomName(RoomNameAddRequest request, UUID ronid) {
        RoomName roomName = extractedRoomName(ronid);

        if (!roomName.getName().equals(request.getName()))
            roomName.setName(request.getName());

        if (roomName.getDescription() == null || !roomName.getDescription().equals(request.getDescription()))
            roomName.setDescription(request.getDescription());

        RoomName save = roomNameRepo.save(roomName);

        return RoomNameResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomNameDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomNameResponse deleteRoomName(UUID ronid) {
        RoomName roomName = extractedRoomName(ronid);
        roomName.setStatus(SystemConstant.STATUS_NO_ACTIVE);
        roomName.setIsDeleted(SystemConstant.NO_ACTIVE);
        RoomName save = roomNameRepo.save(roomName);

        return RoomNameResponse.builder()
                .code("")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomNameDTOMapper.apply(save))
                .message(getMessageBundle(""))
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    @Override
    public RoomNameResponses findAll(int currentPage, int limitPage) {
        Pageable pageable = baseAmenityUtil.pageable(currentPage, limitPage);
        Page<RoomName> all = roomNameRepo.findAll(pageable);

        List<RoomNameDTO> roomNameDTOS = all.stream()
                .map(roomNameDTOMapper)
                .toList();

        return RoomNameResponses.builder()
                .code("RN_001")
                .status(SystemConstant.STATUS_SUCCESS)
                .data(roomNameDTOS)
                .meta(baseAmenityUtil.pageableResponseUtil(pageable, all.getTotalPages()))
                .message("Get rooms name success!..")
                .responseTime(baseAmenityUtil.responseTime())
                .build();
    }

    private void extracted(String name) {
        if (roomNameRepo.existsByName(name))
            throw new ApiRequestException("add fail room name " + name + " already exists!.", "");
    }

    private RoomName extractedRoomName(UUID ronid) {
        return roomNameRepo.findById(ronid)
                .orElseThrow(() -> new ApiRequestException("find room name by id " + ronid + " not found!.", ""));
    }
}
