package com.example.graduationthesis.roomname.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RoomNameAddRequest {

    private String name;

    private String description;
}
