package com.example.graduationthesis.hotelfavourite.mapper;

import com.example.graduationthesis.hotelfavourite.dto.HotelFavouriteDTO;
import com.example.graduationthesis.hotelfavourite.entity.HotelFavourite;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class HotelFavouriteDTOMapper implements
        Function<HotelFavourite, HotelFavouriteDTO> {

    @Override
    public HotelFavouriteDTO apply(HotelFavourite hotelFavourite) {
        return new HotelFavouriteDTO(
                hotelFavourite.getId(),
                hotelFavourite.getUser().getId(),
                hotelFavourite.getHotel().getId()
        );
    }
}
