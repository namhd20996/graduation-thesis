package com.example.graduationthesis.hotelfavourite.repo;

import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteDTOResp;
import com.example.graduationthesis.hotelfavourite.entity.HotelFavourite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HotelFavouriteRepo extends JpaRepository<HotelFavourite, UUID> {

    @Query("""
            SELECT
                new com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteDTOResp(
                    h.id,
                    h.name,
                    h.country,
                    h.streetAddress,
                    h.districtAddress,
                    h.city,
                    COUNT(hf.hotel)
                )
            FROM
                HotelFavourite hf
            INNER JOIN
                Hotel h ON h.id = hf.hotel.id
            WHERE
                h.status = 'ACTIVE'
            GROUP BY
                h.id,
                h.name,
                h.country,
                h.streetAddress,
                h.districtAddress,
                h.city
            ORDER BY
               COUNT(hf.hotel) DESC
            LIMIT
                18
            """)
    Optional<List<HotelFavouriteDTOResp>> findHotelByFavourite();

    boolean existsByUserIdAndHotelId(UUID userId, UUID hotelId);

    Optional<HotelFavourite> findHotelFavouriteByHotel_IdAndUser_Id(UUID hotelId, UUID userId);
}
