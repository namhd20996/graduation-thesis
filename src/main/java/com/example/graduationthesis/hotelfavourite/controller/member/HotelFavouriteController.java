package com.example.graduationthesis.hotelfavourite.controller.member;

import com.example.graduationthesis.constant.HotelFavouriteConstant;
import com.example.graduationthesis.constant.SystemConstant;
import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteResp;
import com.example.graduationthesis.hotelfavourite.dto.response.HotelFavouriteResponse;
import com.example.graduationthesis.hotelfavourite.service.HotelFavouriteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(SystemConstant.API_MEMBER + SystemConstant.API_VERSION_ONE + HotelFavouriteConstant.API_HOTEL_FAVOURITE)
@RequiredArgsConstructor
public class HotelFavouriteController {

    private final HotelFavouriteService hotelFavouriteService;

    @PostMapping("/{hid}")
    public ResponseEntity<HotelFavouriteResp> addHotelFavourite(@PathVariable("hid") UUID hid) {
        return new ResponseEntity<>(hotelFavouriteService.addHotelFavourite(hid), HttpStatus.CREATED);
    }

    @DeleteMapping("/{hotelId}")
    public ResponseEntity<HotelFavouriteResponse> deleteHotelFavourite(@PathVariable("hotelId") UUID hotelId) {
        return new ResponseEntity<>(hotelFavouriteService.deleteFavourite(hotelId), HttpStatus.OK);
    }

}
