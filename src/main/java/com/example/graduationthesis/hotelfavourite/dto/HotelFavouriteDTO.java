package com.example.graduationthesis.hotelfavourite.dto;

import java.util.UUID;

public record HotelFavouriteDTO(
        UUID hotelFavouriteId,
        UUID userId,
        UUID hotelId
) {
}
