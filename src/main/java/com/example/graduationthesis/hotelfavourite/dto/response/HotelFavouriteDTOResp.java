package com.example.graduationthesis.hotelfavourite.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class HotelFavouriteDTOResp {

    private UUID hotelId;
    private String nameHotel;
    private String country;
    private String streetAddress;
    private String districtAddress;
    private String city;
    private Long totalFavourite;
    private Double reviewRating;
    private Long countReview;
    private String urlImage;

    public HotelFavouriteDTOResp(
            UUID hotelId,
            String nameHotel,
            String country,
            String streetAddress,
            String districtAddress,
            String city,
            Long totalFavourite
    ) {
        this.hotelId = hotelId;
        this.nameHotel = nameHotel;
        this.country = country;
        this.streetAddress = streetAddress;
        this.districtAddress = districtAddress;
        this.city = city;
        this.totalFavourite = totalFavourite;
    }
}
