package com.example.graduationthesis.hotelfavourite.dto.response;

import com.example.graduationthesis.utils.BaseResponseUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class HotelFavouriteResponse extends BaseResponseUtil {
    private UUID hotelId;
    private String nameHotel;
    private String country;
    private String streetAddress;
    private String districtAddress;
    private String city;
    private Double reviewRating;
    private Long countReview;
    private Long totalFavourite;
    private String urlImage;

}
