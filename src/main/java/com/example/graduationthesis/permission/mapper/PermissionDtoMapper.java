package com.example.graduationthesis.permission.mapper;

import com.example.graduationthesis.permission.dto.PermissionDto;
import com.example.graduationthesis.permission.entity.Permission;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class PermissionDtoMapper implements Function<Permission, PermissionDto> {

    @Override
    public PermissionDto apply(Permission permission) {
        return PermissionDto.builder()
                .permissionId(permission.getId())
                .permissionCode(permission.getPermissionCode())
                .isDeleted(permission.getIsDeleted())
                .status(permission.getStatus())
                .description(permission.getDescription())
                .build();
    }
}
