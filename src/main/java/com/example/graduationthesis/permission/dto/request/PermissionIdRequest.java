package com.example.graduationthesis.permission.dto.request;

import java.util.UUID;

public record PermissionIdRequest(UUID permissionId) {
}
