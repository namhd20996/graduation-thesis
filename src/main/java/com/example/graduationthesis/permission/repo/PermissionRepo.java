package com.example.graduationthesis.permission.repo;

import com.example.graduationthesis.permission.entity.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PermissionRepo extends JpaRepository<Permission, UUID> {
    boolean existsByPermissionCode(String permissionCode);

    Optional<Permission> findPermissionByIdAndIsDeleted(UUID permissionId, Boolean isDeleted);

    @Query("""
            SELECT
                o
            FROM
                Permission o
            WHERE
                o.status= ?1
            AND NOT
                o.permissionCode = 'admin_master'
            """)
    Page<Permission> findAllByStatusAndNotPermissionCodeMaster(String status, Pageable pageable);

    @Query("""
            SELECT
                pm
            FROM
                Permission  pm
            INNER JOIN
                PermissionRole pmsr ON pmsr.permission.id = pm.id
            INNER JOIN
                Role r ON r.id = pmsr.role.id
            INNER  JOIN
                UserRole  usr ON usr.role.id = r.id
            INNER JOIN
                User u ON u.id = usr.user.id
            WHERE
               u.id = ?1
            """)
    List<Permission> findPermissionsByUserId(UUID userId);
}
