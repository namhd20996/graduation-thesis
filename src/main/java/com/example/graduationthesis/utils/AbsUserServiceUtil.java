package com.example.graduationthesis.utils;

import com.example.graduationthesis.user.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbsUserServiceUtil {

    public User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
