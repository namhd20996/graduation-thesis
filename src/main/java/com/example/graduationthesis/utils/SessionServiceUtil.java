package com.example.graduationthesis.utils;

import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
@SuppressWarnings("unchecked")
public class SessionServiceUtil {

    private final HttpSession httpSession;

    public void putValue(String key, Object value) {
        httpSession.setAttribute(key, value);
    }


    public Object getValue(String key) {
        return httpSession.getAttribute(key);
    }

    public Map<String, Object> getValueMap(String key) {
        return (Map<String, Object>) httpSession.getAttribute(key);
    }

    public void removeValue(String key) {
        httpSession.removeAttribute(key);
    }

    public void removeValueAndPutValue(String key, Object value) {
        removeValue(key);
        putValue(key, value);
    }
}
