package com.example.graduationthesis.utils;

import org.springframework.data.domain.Pageable;

public record PageableResponseUtil(
        Integer totalPage,
        Pageable pageable
) {
}
