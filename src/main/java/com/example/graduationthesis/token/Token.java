package com.example.graduationthesis.token;

import com.example.graduationthesis.user.entity.User;
import com.example.graduationthesis.utils.BaseIDUtil;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_token")
public class Token extends BaseIDUtil {
    @Column
    private String token;
    @Enumerated(EnumType.STRING)
    private TokenType tokenType;
    @Column
    private Boolean expired;
    @Column
    private Boolean revoked;
    @ManyToOne
    @JoinColumn(name = "_user_id")
    private User user;
}
